# This file is a xhcloud frontend dockerfile, and might need editing before it works on your project.

FROM 188.104.15.37/nginx/nginx:1.19.1-alpine-perl

COPY nginx-config.conf /etc/nginx/conf.d/default.conf
COPY . /usr/share/nginx/html

WORKDIR /usr/share/nginx/html/

EXPOSE 80
