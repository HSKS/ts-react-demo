import React, {Component, useState, FC} from "react"
import {observer} from "mobx-react"
import {
    LeftOutlined,
    RightOutlined
} from '@ant-design/icons';
import store from "./store"
import "./styles.less"
import {Link} from "react-router-dom";

interface LBProps {
    images: Array<string>
}
let timer: NodeJS.Timeout
const LB: FC<LBProps> = (props: LBProps) => {
    const [idx, setIdx] = useState(0)
    const {images} = props
    const updateIndex = (add: boolean): void => {
        if (add) {
            setIdx((idx+1)%images.length)
        } else {
            setIdx(images.length-1-idx%images.length)
        }
    }
    console.log(idx)
    setTimeout(()=>{
        updateIndex(true)
    }, 1000)

    return (
        <div className='lb' style={{backgroundImage: `url("${props.images[idx]}")`}}>
            <LeftOutlined  className='left-icon' onClick={()=>updateIndex(false)}/>
            <RightOutlined className='right-icon' onClick={()=>updateIndex(true)}/>
        </div>
    )
}

@observer
class Index extends Component {
    render() {
        return (
            <div className='alerter'>
                <LB images={[
                    'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=1853665517,4109325398&fm=26&gp=0.jpg',
                    'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=2711321822,3561690598&fm=26&gp=0.jpg',
                    'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=1326207195,2757755478&fm=26&gp=0.jpg'
                ]}/>
            </div>
        )
    }
}

export default Index