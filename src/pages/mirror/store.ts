import {action, observable} from 'mobx'
import request from '../../utils/request'
import apis from '../../config/apis'
import {MirrorList} from './interfaces'

class Store {
    @observable name: string | undefined = undefined
    @observable os: string | undefined = undefined
    @observable middle: string | undefined = undefined
    @observable framework: string | undefined = undefined
    @observable mirrorList: Array<MirrorList> = []
    @action init = () => {
        this.name = undefined
        this.os = undefined
        this.middle = undefined
        this.framework = undefined
        this.GetmirrorList()
    }
    @action GetmirrorList = async () => {
        const api = apis.GetmirrorList
        const params = {
            mirror_name: this.name,
            system: this.os,
            system_framework: this.framework,
            middle: this.middle
        }
        const resp = await request(api, params)
        if (!resp) return
        this.mirrorList = resp
    }
    @action MirrorCollect = async (id: number, status: number) => {
        const api = apis.MirrorCollect
        if (status === 0) {
            status = 2
        } else if (status === 1) {
            status = 1
        }
        const params = {
            id: id,
            status: status
        }
        const resp = await request(api, params)
        if (!resp) return
    }
}
export default new Store()

