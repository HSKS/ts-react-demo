export interface MirrorList {
    name: string
    is_collect: number
    collect_count: number
    download_count: number
    description: string
    icon: string
    id: number
}