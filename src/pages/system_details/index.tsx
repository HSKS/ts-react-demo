import React, {Component} from "react"
import {observer} from "mobx-react"
import {Steps, Input, Progress, Tabs, Divider, Table} from 'antd'
import copy from 'copy-to-clipboard';
import {Link, RouteComponentProps} from 'react-router-dom';
import "./styles.less"
import store from "./store";
import Container from '../../components/Container'
import formatDate from "../../utils/formatDate";
import routes from "../../config/routes";

const {Step} = Steps;

const {TabPane} = Tabs;

interface EditInput {
    isEdit?: boolean
    title: string
    value: string
    onChange?: (val: string) => void
}

const EditInput: React.FC<EditInput> = ({isEdit, title, value, onChange}) => {
    return isEdit ? (
        <div className='edit-input'>
            <label className='label'>{title}：</label>
            <Input className='input' value={value} onChange={(e) => {
                if (onChange) onChange(e.target.value)
            }}/>
        </div>
    ) : (
        <div className='edit-input'>
            <label className='label'>{title}：</label>
            <span className='input'>{value}</span>
        </div>
    )
}

interface Params {
    namespace: string
}
@observer
class Index extends Component<RouteComponentProps, {}> {
    componentDidMount(): void {
        const {namespace} = this.props.match.params as Params
        store.namespace = namespace
        store.GetNsInfo()
        store.GetOperationLog()
        store.GetNsAppList()
    }

    render() {
        const {name, groupName, oneArea, twoArea, createdAt, username, describe, mobile, total_cpu, total_mem, used_cpu, used_mem, operationLogs, appList, page_num, page_size, total_num} = store
        return (
            <Container title='业务系统详情' className='system-details'>
                {/* 内容开始 */}
                <section className="image-content">
                    <div className='top'>
                        <div className='row'>
                            <div className='col'>
                                <EditInput title='业务系统名称' value={name}/>
                                <EditInput title='租户' value={groupName}/>
                            </div>
                            <div className='col'>
                                <EditInput title='一级、二级域' value={`${oneArea} ${twoArea}`}/>
                                <EditInput title='创建时间' value={formatDate(new Date(createdAt))}/>
                            </div>
                            <div className='col'>
                                <EditInput title='负责人' value={username}/>
                                <EditInput title='描述' value={describe}/>
                            </div>
                            <div className='col'>
                                <EditInput title='联系电话' value={mobile}/>
                            </div>

                        </div>
                    </div>
                    <Tabs defaultActiveKey="1">
                        <TabPane tab="资源配额" key="1">
                            <div className='resource-quota-top'>
                                <div className='item'>
                                    <div className='l'>
                                        <h3>CPU</h3>
                                        <h5>资源分配</h5>
                                        <h3>{used_cpu.toFixed(2)}/{total_cpu}C</h3>
                                    </div>
                                    <Progress type="dashboard"
                                              percent={parseFloat(((used_cpu / total_cpu) * 100).toFixed(2))}
                                              width={80}/>
                                </div>
                                <div className='item'>
                                    <div className='l'>
                                        <h3>内存</h3>
                                        <h5>资源分配</h5>
                                        <h3>{used_mem.toFixed(2)}/{total_mem}G</h3>
                                    </div>
                                    <Progress type="dashboard"
                                              percent={parseFloat(((used_mem / total_mem) * 100).toFixed(2))}
                                              width={80}/>
                                </div>
                            </div>
                            <Table columns={[
                                {
                                    title: '应用名称',
                                    width: 120,
                                    dataIndex: 'name',
                                    // fixed: 'left',
                                    align: 'center',
                                },
                                {
                                    title: 'APPID',
                                    width: 120,
                                    dataIndex: 'app_id',
                                    // fixed: 'left',
                                    align: 'center',
                                },
                                {
                                    title: 'CPU',
                                    width: 120,
                                    dataIndex: 'cpu',
                                    align: 'center',
                                    render: (text) => `${text}Core`
                                },
                                {
                                    title: '内存',
                                    width: 120,
                                    dataIndex: 'mem',
                                    align: 'center',
                                    render: (text) => `${text}GB`
                                },
                                {
                                    title: 'GPU',
                                    width: 150,
                                    dataIndex: 'gpu_num',
                                    align: 'center',
                                    render: (text, {gpu_name, gpu_num}) => text ? `${gpu_name}/${gpu_num}` : '-'
                                },

                            ]} dataSource={[...appList]}
                                   scroll={{x: 630}}
                                   pagination={{
                                       pageSize: page_size,
                                       current: page_num,
                                       total: total_num,
                                       onChange: (val) => {
                                           store.page_num = val
                                           store.GetNsAppList()
                                       }
                                   }}
                                   rowKey={(record: any) => record.app_id}
                            />
                        </TabPane>
                        <TabPane tab="资源利用率" key="2">

                        </TabPane>
                        <TabPane tab="事件管理" key="3">
                            <Steps progressDot current={operationLogs.length || 0} direction="vertical">
                                {
                                    operationLogs && operationLogs.map((val) => {
                                        return (
                                            <Step key={val.id} title={`用户：${val.real_name} `}
                                                  description={`${val.describe} ${formatDate(new Date(val.created_at))}`}/>
                                        )
                                    })
                                }
                            </Steps>
                        </TabPane>
                    </Tabs>
                </section>
                {/* 内容结束 */}
            </Container>

        )

    }

}


export default Index;
