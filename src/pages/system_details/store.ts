import {action, observable} from "mobx"
import request from '../../utils/request'
import apis from '../../config/apis'
import {NsInfo, OperationLog, nsAppInfo} from './interfaces'
import formatDate from "../../utils/formatDate";
class Store {
    @observable namespace = ''
    @observable name = ''
    @observable groupName = ''
    @observable oneArea = ''
    @observable twoArea = ''
    @observable createdAt = formatDate(new Date())
    @observable username = ''
    @observable describe = ''
    @observable mobile = ''
    @observable total_cpu = 1
    @observable total_mem = 1
    @observable used_cpu = 0
    @observable used_mem = 0

    @action GetNsInfo = async () => {
        const api = apis.GetNsInfo
        const resp: NsInfo = await request(api, {name_en: this.namespace})
        if (!resp) return
        console.log(resp)
        this.name = resp.name_cn
        this.groupName  = resp.group_name
        this.oneArea = resp.top_level_domain
        this.twoArea = resp.second_level_domain
        this.createdAt = resp.created_at
        this.username = resp.real_name
        this.describe = resp.describe
        this.total_cpu = resp.total_cpu
        this.total_mem = resp.total_mem
        this.used_cpu = resp.used_cpu
        this.used_mem = resp.used_mem
    }

    @observable operationLogs = new Array<OperationLog>()
    @action GetOperationLog = async () => {
        const api = apis.GetOperationLog
        const resp: Array<OperationLog> = await request(api, {namespace: this.namespace})
        if (!resp) return
        console.log(resp)
        this.operationLogs = resp
    }
    @observable page_num = 1
    @observable page_size = 20
    @observable total_num = 0
    @observable appList = new Array<nsAppInfo>()
    @action GetNsAppList = async () => {
        const api = apis.GetNsAppList
        const params = {
            page_num: this.page_num,
            page_size: this.page_size,
            namespace: this.namespace
        }
        const resp = await request(api, params)
        if (!resp) return
        console.log(resp)
        this.page_num = resp.page_num
        this.page_size = resp.page_size
        this.appList = resp.list
        this.total_num = resp.count
    }
}

export default new Store()