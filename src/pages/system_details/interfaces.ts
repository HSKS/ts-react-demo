export interface NsInfo {
    app_number: number
    cpu: number
    created_at: string
    describe: string
    disk: number
    domain_id: number
    gpu_high: number
    gpu_low: number
    gpu_mid: number
    group_id: number
    group_name: string
    id: number
    is_deleted: number
    mem: number
    name_cn: string
    name_en: string
    pod_number: number
    real_name: string
    second_domain_id: number
    second_level_domain: string
    top_domain_id: number
    top_level_domain: string
    total_cpu: number
    total_mem: number
    used_cpu: number
    used_mem: number
    username: string
}

export interface OperationLog {
    id: number
    log_code: string
    method: string
    app_id: string
    namespace: string
    username: string
    real_name: string
    status: number
    describe: string
    created_at: string
}

export interface nsAppInfo {
    id: number
    name: string
    app_id: string
    namespace: string
    cpu: number
    mem: number
    gpu_id: number
    gpu_name: string
    gpu_num: number
    describe: string
    is_deleted: number
    created_at: string

}