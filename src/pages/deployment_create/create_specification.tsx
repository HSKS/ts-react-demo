import React, { Component } from 'react'
import { observer } from 'mobx-react'
import { Descriptions, Collapse, Table } from 'antd';
import store from './store'
import './styles.less'
import CreateFooter from './create_footer'
const { Panel } = Collapse;
@observer
class Index extends Component {
    render() {
        const {} = store
        return (
            <div className='create-specification'>
                {/* 应用配置开始 */}
                <section className='content'>
                    <Descriptions title="应用配置" column={4}>
                        <Descriptions.Item label="业务系统">示例</Descriptions.Item>
                        <Descriptions.Item label="应用名称">示例</Descriptions.Item>
                        <Descriptions.Item label="APPID">示例</Descriptions.Item>
                        <Descriptions.Item label="节点亲和">示例</Descriptions.Item>
                        <Descriptions.Item label="应用描述">这里是一段应用描述XXXXXXXXXXXXXXXXXXXXXXXXXXX。</Descriptions.Item>
                    </Descriptions>
                </section>
                {/* 应用配置结束 */}


                {/* 容器配置开始 */}
                <section className='content'>
                    <Descriptions title="容器配置" column={4}>
                        <Descriptions.Item label="部署区域">示例</Descriptions.Item>
                        <Descriptions.Item label="部署集群">示例</Descriptions.Item>
                        <Descriptions.Item label="实例数">示例</Descriptions.Item>
                        <Descriptions.Item label="镜像">示例</Descriptions.Item>
                        <Descriptions.Item label="容器规格">通用计算型</Descriptions.Item>
                    </Descriptions>
                    {/* 高级配置 */}
                    <div className='advanced'>
                        <strong>高级配置：</strong>
                        <Collapse defaultActiveKey={['1']} onChange={() => {

                        }}>
                            {/* 负载均衡 */}
                            <Panel header="负载均衡" key="1">
                                <Table
                                    bordered={true}
                                    pagination={false}
                                    columns={[
                                        {
                                            title: '服务端口',
                                            dataIndex: 'age',
                                            key: 'age',
                                        },
                                        {
                                            title: '协议',
                                            dataIndex: 'address',
                                            key: 'address',
                                        },
                                    ]} dataSource={[
                                        {
                                            key: '1',
                                            age: 32,
                                            address: '8080',
                                        },
                                        {
                                            key: '2',
                                            name: 'Jim Green',
                                            age: 42,
                                            address: 'London No. 1 Lake Park',
                                        },
                                        {
                                            key: '3',
                                            name: 'Joe Black',
                                            age: 32,
                                            address: 'Sidney No. 1 Lake Park',
                                        },
                                    ]} />
                            </Panel>

                            {/* 数据存储 */}
                            <Panel header="数据存储" key="2">
                                <p>{33}</p>
                            </Panel>

                            {/* 目录映射 */}
                            <Panel header="目录映射" key="3">
                                <p>{22}</p>
                            </Panel>

                            {/* 健康检查 */}
                            <Panel header="健康检查" key="4">
                                <p>{22}</p>
                            </Panel>

                            {/* 重启策略 */}
                            <Panel header="重启策略" key="5">
                                <p>{22}</p>
                            </Panel>

                            {/* 启动命令 */}
                            <Panel header="启动命令" key="6">
                                <p>{22}</p>
                            </Panel>
                        </Collapse>
                    </div>
                </section>
                {/* 容器配置结束 */}


                <CreateFooter evts={() => {
                    const { setCreateAppId, setCurrent, current } = store
                    // setCurrent(current + 1)

                    return true
                }} />

            </div>
        )
    }
}


export default Index
