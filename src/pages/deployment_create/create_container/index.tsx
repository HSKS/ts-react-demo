import React, { Component } from 'react'
import { observer } from 'mobx-react'
import {Input, Button} from 'antd'
import store from './store'
import './styles.less'
import { Link } from 'react-router-dom'
import {Container} from '../interfaces'
import ContainerInfo from './container_info'

interface EmitFunc {
    (arg0: Array<Container>): void
}

interface CreateCProps {
    onChange: EmitFunc
}

@observer
class Index extends Component<CreateCProps, {}> {
    generateParams = () => {

    }
    render() {
        const {containers} = store
        return (
            <div className='create-container'>
                {
                    containers.map((c, idx)=>{
                        return <ContainerInfo key={Math.random()*10000*idx} idx={idx} c={c} evts={(ctn, idx)=>{
                            store.setContainer(idx, ctn)
                        }
                        }/>
                    })
                }
                <Button onClick={this.generateParams}>下一步</Button>
            </div>
        )
    }
}


export default Index
