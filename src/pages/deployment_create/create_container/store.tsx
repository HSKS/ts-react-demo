import { observable, action } from 'mobx'

import {Container,Port} from '../interfaces'

class Store {
    @observable containers:Array<Container> = []
    @action setContainer = (idx: number, c: Container): void => {
        this.containers[idx] = c
        this.containers = [...this.containers]
    }
    @action setName = (idx: number, name: string): void => {
        this.checkContainerNull(idx)
        this.containers[idx].name = name
        this.containers = [...this.containers]
    }
    @action setImages = (idx: number, image: string): void => {
        this.checkContainerNull(idx)
        this.containers[idx].image = image
        this.containers = [...this.containers]
    }
    @action setCommand = (idx: number, commandStr: string): void => {
        this.checkContainerNull(idx)
        this.containers[idx].command = commandStr.split(' ')
        this.containers = [...this.containers]
    }
    @action setArgs =(idx: number, argStr: string): void => {
        this.checkContainerNull(idx)
        this.containers[idx].args = argStr.split(' ')
        this.containers = [...this.containers]
    }
    @action setWorkDir = (idx: number, dir: string): void => {
        this.checkContainerNull(idx)
        this.containers[idx].working_dir = dir
        this.containers = [...this.containers]
    }
    @action addEnvCmName = (idx: number, name: string): void => {
        this.checkContainerNull(idx)
        if (!this.containers[idx].env_cm_names) this.containers[idx].env_cm_names = []
        // @ts-ignore
        this.containers[idx].env_cm_names.push(name)
        this.containers = [...this.containers]
    }

    @action addKvEnv = (idx: number, key: string, val: string): void => {
        this.checkContainerNull(idx)
        if (!this.containers[idx].kv_envs) this.containers[idx].kv_envs = {}
        // @ts-ignore
        this.containers[idx].kv_envs[key] = val
        this.containers = [...this.containers]
    }

    checkContainerNull = (idx: number) =>{
        if (!this.containers[idx]) this.containers[idx] = {
            args: undefined,
            command: undefined,
            cpu: 0.1,
            env_cm_names: undefined,
            image: "",
            kv_envs: undefined,
            mem: 1024,
            name: "",
            ports: undefined,
            should_port: true,
            // volume_mounts: undefined,
            working_dir: ""
        }
    }
}


export default new Store()


