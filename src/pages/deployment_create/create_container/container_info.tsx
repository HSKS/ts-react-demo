import React, {Component} from "react"
import {observer} from "mobx-react"
import {Input} from 'antd'
import "./container_info_styles.less"
import {Container} from '../interfaces'

interface Evt {
    (arg: Container, idx: number): void
}

interface C {
    c: Container
    evts: Evt
    idx: number
}

@observer
class Index extends Component<C, {}> {
    render() {
        const {name, image, command, args, working_dir, env_cm_names, kv_envs, ports, cpu, mem, should_port} = this.props.c
        return (
            <div className='container-info'>
                <Input addonBefore="应用名称"  value={name} onChange={(e)=>{
                    const c = this.props.c
                    c.name = e.target.value
                    this.props.evts(c, this.props.idx)
                }} />
            </div>
        )
    }
}

export default Index