export interface data {
    editing: boolean
    key: number
    [prop: string]: any
}
export interface TableConfig {
    columns: Array<object>
    datas: Array<data>
    dataLength?: number
    add(): void
}

//没啥别的意思，这就是个对象
export interface IObject {
    [prop: string]: any
}

export interface Container {
    name?: string
    image: string
    command?: Array<string>
    args?: Array<string>
    working_dir?: string
    env_cm_names?: Array<string>
    kv_envs?: {
        [prop: string]: string
    },
    ports?: Array<Port>
    cpu: number,
    mem: number,
    should_port?: boolean
}

export interface Port {
    name?: string
    path?: string
    container_port?: string
    protocol?: string
}

export interface VolumeMount {

}

// 应用创建 - 提交
export interface CreateApp {
    app_id: string //应用ID
    addr: string //区域
    clusters: [{//集群列表
        cluster_label: string //集群标签
        replicas: number //实例数量
    }] | []
    namespace: string //业务系统 
    app_name: string //应用名称
    describe: string //描述
    node_affinity_labels?: { //节点亲和性，自定义的k，v
        [prop: string]: Array<string>
    }
    restart_policy?: string //Always总是重启  OnFailure失败的时候重启  Never从不重启
    command?: Array<string> //指令集合，用空格split
    image: string //镜像名称


    labels: {  // 标签
        [porp: string]: string
    }

    // ========================
    cpu: number //CPU核
    mem: number //内存G
    gpu_id:number
    gpu_num:number

    //==========================================

    is_proxy: boolean //是否启动负载均衡 
    container_port?: number //容器端口号
    protocol?: string //协议类型 TCP，UDP

    //==========================================
    host_path_volumes?: Array<{ //节点目录映射
        to: string //容器中的映射目录
        from: string //节点主机上的目录
        read_only?: boolean //是否只读
    }>
    //==========================================
    env?: { //环境变量，自定义k，v
        [prop: string]: string
    }
    //==========================================
    is_liveness_probe: boolean//是否更改状态
    liveness_probe?: { //健康检测
        type: number //健康检测类型：1exec，2http，3tcp
        command: Array<string> //指令集，用空格split
        port: number //端口号
        path: string //http的请求路径
        headers: { //http请求的请求头
            [prop: string]: string
        }
        initial_delay_seconds: number //容器启动后多少秒后进行探测
        timeout_seconds: number //超时时间
        period_seconds: number //每隔多少秒进行探测
    }
    is_check_resource_quotas: boolean //默认true
   
}
