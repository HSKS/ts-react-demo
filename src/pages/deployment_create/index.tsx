import React, { Component } from "react"
import { observer } from "mobx-react"
import { Steps } from 'antd';

import store from "./store"
import "./styles.less"

import CreateApplication from './create_application'
import CreateContainer from './create_container'
import CreateSpecification from './create_specification'
import CreateFinish from './create_finish'

import Container from '../../components/Container'
import Loading from '../../components/Loading'
const { Step } = Steps;

interface IStepsPorps {
    current: number
}
const HeaderSteps: React.FC<IStepsPorps> = ({ current }) => {
    let className = 'one';
    switch (current) {
        case 0:
            className = 'one'
            break;
        case 1:
            className = 'two'
            break;
        case 2:
            className = 'three'
            break;
        case 3:
            className = 'four'
            break;
    }

    return (
        <section className={'header-steps ' + className}>
            {/* 步骤 */}
            <div className='steps'>
                <span>应用配置</span>
                <span>容器配置</span>
                <span>规格确认</span>
                <span>完成</span>
            </div>
            {/* 线 */}
            <div className='steps-hr'>
                <i className='this' />
            </div>
        </section>)
}

@observer
class Index extends Component {
    componentDidMount(): void {
        store.initRequest()
        
        store.initListCreateApps(1000009)//测试用 - 镜像初始化
        store.init()//参数清空
    }

    render() {
        const { current, loading } = store;
        return (
            <Container title='应用创建' className="deployment-create cont">
                {/* 头部步骤开始 */}
                <HeaderSteps current={current} />
                {/* 头部步骤结束 */}

                {/* 内容开始 */}
                <div className="cont">
                    {current === 0 && <CreateApplication />}
                    {current === 1 && <CreateContainer />}
                    {current === 2 && <CreateSpecification />}
                    {current === 3 && <CreateFinish />}
                </div>
                {/* 内容结束 */}

                {/* 加载中 */}
                <Loading show={loading} />
            </Container>
        )
    }
}

export default Index