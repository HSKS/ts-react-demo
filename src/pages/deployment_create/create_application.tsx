import React, { Component, useState } from 'react'
import { observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import { Form, Input, Button, Select, InputNumber, Cascader, Popover } from 'antd';
import { QuestionCircleOutlined } from '@ant-design/icons';
import { FormInstance } from 'antd/lib/form';
import ContainerRow from '../../components/ContainerRow'
import Counter from '../../components/Counter'
import store from './store'
import './styles.less'
import CreateFooter from './create_footer'

const { Option } = Select;

@observer
class Index extends Component {
    state = {
        // 错误提示
        required: {
            appId: false,
            namespace: false,
            appName: false,
        },
        error: {
            appId: '',
        }
    }


    //更改 required 或者 error 内容
    empty = (key: string, value: boolean | string): void | object => {
        let { required, error } = this.state
        if (typeof value === 'boolean') {
            // @ts-ignore
            required[key] = value;
            this.setState({ required })
        } else {
            // @ts-ignore
            required[key] = true; error[key] = value;
            return { required, error }
        }
    }


    // 提交验证 - 非空验证
    required(data: object): Array<string> {
        let nullValue: string[] = [];
        console.log(data)
        for (const key in data) {
            // @ts-ignore
            const value = data[key];
            if (!value) nullValue.push(key)
        }
        return nullValue;
    }


    render() {
        const { required, error } = this.state
        const { namespaces, appId, namespace, appName, describe } = store

        return (
            <div className='create-application' >

                {/* 业务系统 */}
                <section>
                    <ContainerRow title='业务系统' required={required.namespace}>
                        <Select
                            placeholder="请选择"
                            value={namespace}
                            onFocus={() => this.empty('namespace', false)}
                            onChange={(e) => {
                                const [name,id] = e.split(',')
                                store.setNamespace(name)
                                store.initListCreateApps(Number(id))//初始化镜像
                            }}>
                            {namespaces.length && namespaces.map((item, i) =>
                                <Option key={i} value={item.name_en + ',' + item.id}>{item.name_cn}</Option>)}
                        </Select>
                    </ContainerRow>
                </section>


                {/* 应用名称 */}
                <ContainerRow title='应用名称' required={required.appName}>
                    <Input
                        placeholder="请输入应用名称"
                        className="form-input"
                        value={appName}
                        onFocus={() => this.empty('appName', false)}
                        onChange={(e) => {
                            store.setAppName(e.target.value as string)
                        }} />
                </ContainerRow>


                <section>
                    {/* appId */}
                    <ContainerRow title='AppId' required={required.appId} error={error.appId}>
                        <Input
                            placeholder="请输入AppId"
                            className="form-input"
                            value={appId}
                            onFocus={() => this.empty('appId', false)}
                            onChange={e => {
                                store.setAppId(e.target.value as string)
                            }} />
                    </ContainerRow>

                    {/* 实例数量  -  取消了，我好难过 */}
                    {/* <ContainerRow title='实例数量' className='replicas' required={required.replicas}>
                        <Counter onClick={num => {
                            console.log(num)
                        }} />
                    </ContainerRow> */}
                </section>


                {/* 节点亲和 */}
                <ContainerRow className="node paralleling" title='节点亲和'>
                    <QuestionCircleOutlined className='node-question' />
                    <Select
                        placeholder="tag1"
                        onChange={(e) => {
                            // store.setNodeAffinityLabels(e as {})
                        }}
                        allowClear>
                        {/* <Option value="male">male</Option>
                        <Option value="female">female</Option>
                        <Option value="other">other</Option> */}
                    </Select>
                </ContainerRow>


                {/* 应用描述 */}
                <ContainerRow title='应用描述' inputWidth='900px'>
                    <Input.TextArea
                        rows={4}
                        placeholder="选填，请输入应用描述"
                        value={describe}
                        onFocus={() => this.empty('describe', false)}
                        onChange={(e) => {
                            store.setDescribe(e.target.value as string)
                        }} />
                </ContainerRow>

                {/* 底部按钮开始 */}
                <CreateFooter evts={(): boolean => {
                    const { appId, appName, namespace } = store
                    const required: string[] = this.required({ appId, appName, namespace })

                    // 非空校验
                    if (required.length) {
                        let states: object = {}
                        // @ts-ignore
                        required.forEach(item => states[item] = true)
                        this.setState({ required: states })
                        return false
                    }

                    // appid校验
                    const isAppId = /^[a-z](\d|[a-z]|-)*$/.test(appId);
                    if (!isAppId) {
                        console.log(!(/^[a-z](\d|[a-z]|-)*$/.test(appId)), appId)
                        this.setState({ ...this.empty('appId', "请输入有效appId") })
                        return false
                    }
                    return true;
                }} />
                {/* 底部按钮结束 */}
            </div >

        );
    }
}


export default Index
