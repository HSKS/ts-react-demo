import { observable, action } from "mobx"
import { CreateApp, IObject } from './interfaces'
import request from '../../utils/request'
import apis from '../../config/apis'
import host from '../../config/host'
import { observer } from "mobx-react"

class Store {

    //创建应用生成appid
    @observable createAppId: string = '';
    @action setCreateAppId = (createAppId: string): void => {
        this.createAppId = createAppId
    }

    //加载中
    @observable loading = false
    @action setLoading = (): void => {
        this.loading = true
        setTimeout(() => {   //加载中
            this.loading = false
        }, 2000)
    }

    //步骤图
    @observable current: number = 0; //这里改页数没用，看下面的参数清空
    @action setCurrent = (current: number): void => {
        this.current = current
    }



    // 初始化接口 - 地区选择
    @observable areas: Array<IObject> = [];//地区
    @observable namespaces: Array<IObject> = [];//业务查询
    @observable listCreateApps: Array<IObject> = [];//
    @observable cluster: Array<IObject> = [];//集群名称
    @observable nodeList: Array<IObject> = [];//容器规格
    @observable images: IObject = {};//镜像列表

    @action initRequest = async () => {
        const { areas } = await request(apis.GetAreaCluster)
        const { list } = await request(apis.GetNamespace)
   
        this.namespaces = list
        this.areas = areas
    }

    @action initListCreateApps = async (id:number) => {
      const data = await request(apis.ListCreateApp,{id})
      console.log(data);
      this.images = data
    }

    @action initGetNodeType = async (cluster_labels:Array<string>) => {
      const data = await request(apis.GetNodeType,{cluster_labels})
         this.nodeList = data
    }


    // 资源组
    @action getNamespace = async (params: object) => {
        const data = await request(apis.InitResourceData, params)
        console.log(data)
        if (!data) return

        return data
    }


    // 应用提交
    @action setCreateApp = async (params: object) => {
        const data = await request(apis.CreateApp, params)
        if (!data) return;
        return data
    }

    // CreateApp参数实例

    @observable appId: CreateApp['app_id'] = ''
    @observable addr: CreateApp['addr'] = ''
    @observable clusters: CreateApp['clusters'] = []
    @observable namespace: CreateApp['namespace'] = ''
    @observable appName: CreateApp['app_name'] = ''
    @observable describe: CreateApp['describe'] = ''
    @observable nodeAffinityLabels: CreateApp['node_affinity_labels'] = undefined
    @observable restartPolicy: CreateApp['restart_policy'] = undefined
    @observable command: CreateApp['command'] = undefined
    @observable image: CreateApp['image'] = ''
    @observable cpu: CreateApp['cpu'] = 0.5
    @observable mem: CreateApp['mem'] = 3
    @observable isProxy: CreateApp['is_proxy'] = false
    @observable containerPort: CreateApp['container_port'] = undefined
    @observable protocol: CreateApp['protocol'] = undefined
    @observable hostPathVolumes: CreateApp['host_path_volumes'] = undefined
    @observable env: CreateApp['env'] = undefined
    @observable isLivenessProbe: CreateApp['is_liveness_probe'] = false
    @observable livenessProbe: CreateApp['liveness_probe'] = undefined
    @observable isCheckResourceQuotas: CreateApp['is_check_resource_quotas'] = true

    //参数清空   current决定当前所在页数
    @action init = (current = 1): void => {
        this.current = current
        this.appId = ''
        this.addr = ''
        this.clusters = []
        this.namespace = ''
        this.appName = ''
        this.nodeAffinityLabels = undefined
        this.describe = ''
        this.restartPolicy = undefined
        this.command = undefined
        this.image = ''
        this.cpu = 0.5
        this.mem = 3
        this.isProxy = false
        this.containerPort = undefined
        this.protocol = undefined
        this.hostPathVolumes = undefined
        this.env = undefined
        this.isLivenessProbe = false
        this.livenessProbe = undefined
    }


    @action setAppId = (val: CreateApp['app_id']): void => {
        this.appId = val
    }
    @action setAddr = (val: CreateApp['addr']): void => {
        this.addr = val
    }
    @action setClusters = (val: CreateApp['clusters']): void => {
        this.clusters = val
    }
    @action setNamespace = (val: CreateApp['namespace']): void => {
        this.namespace = val
    }
    @action setAppName = (val: CreateApp['app_name']): void => {
        this.appName = val
    }
    @action setNodeAffinityLabels = (val: CreateApp['node_affinity_labels']): void => {
        this.nodeAffinityLabels = val
    }

    @action setDescribe = (val: CreateApp['describe']): void => {
        this.describe = val
    }
    @action setRestartPolicy = (val: CreateApp['restart_policy']): void => {
        this.restartPolicy = val
    }
    @action setImage = (val: CreateApp['image']): void => {
        this.image = val
    }
    @action setCPU = (val: CreateApp['cpu']): void => {
        this.cpu = val
    }
    @action setMem = (val: CreateApp['mem']): void => {
        this.mem = val
    }

    @action setCommand = (val: CreateApp['command']): void => {
        this.command = val
    }
    @action setIsProxy = (val: CreateApp['is_proxy']): void => {
        this.isProxy = val
    }
    @action setProtocol = (val: CreateApp['protocol']): void => {
        this.protocol = val
    }
    @action setContainerPort = (val: CreateApp['container_port']): void => {
        this.containerPort = val
    }
    @action setHostPathVolumes = (val: CreateApp['host_path_volumes']): void => {
        this.hostPathVolumes = val
    }
    @action setEnv = (val: CreateApp['env']): void => {
        this.env = val
    }
    @action setIsLivenessProbe = (val: CreateApp['is_liveness_probe']): void => {
        this.isLivenessProbe = val
    }
    @action setLivenessProbe = (val: CreateApp['liveness_probe']): void => {
        this.livenessProbe = val
    }



}

export default new Store()


