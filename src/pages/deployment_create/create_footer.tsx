import React, { Component } from 'react'
import { observer } from 'mobx-react'
import { Link } from 'react-router-dom'
import { Button, Space } from 'antd';
import store from './store'

interface StepsProps {
    evts(): boolean//回调函数 - 跳转验证
}

@observer
class Index extends Component<StepsProps> {
    step(order: string): void {
        const { current, setCurrent } = store;
        if (order === 'prev') setCurrent(current - 1);//上一页
        else if (this.props.evts() && order === 'next') setCurrent(current + 1)//下一页
    }

    render() {
        const { current } = store
        return (
            <div className="steps-action">
                <Space>
                    {current > 0 && (
                        <Button onClick={() => this.step('prev')}>上一步</Button>
                    )}
                    {current < 2 && (
                        <Button type="primary" onClick={() => this.step('next')}>下一步</Button>
                    )}
                    {current === 2 && (
                        <Button type="primary" onClick={() => this.step('next')}>确认</Button>
                    )}
                </Space>
            </div>
        )
    }
}

export default Index
