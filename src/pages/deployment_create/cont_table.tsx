import React, {Component} from 'react'
import {Button, Table} from 'antd';
import {PlusCircleOutlined} from '@ant-design/icons';
import './styles.less'

import {data, TableConfig} from './interfaces'

class Index extends Component<TableConfig, {}> {

    handleAdd = () => {
        this.props.add()
    }

    render() {
        const {datas, columns, dataLength} = this.props
        const config = {
            dataSource: datas,//内容
            columns: columns,//标题
            bordered: true,//边框样式
            rowKey: (record: data) => {
                return JSON.stringify(record)
            }
        }
        return (
            <>
                <Table {...config} pagination={false}/>

                {/* 添加内容 */}
                {
                    dataLength && dataLength > 0 && datas.length < dataLength ? (
                        <div className="advanced-cont-add" onClick={this.handleAdd}>
                            <Button shape="circle" icon={<PlusCircleOutlined/>}/>
                            <span>增加数据</span>
                        </div>
                    ) : (null)
                }

            </>
        )
    }
}


export default Index
