import React, { Component, useState, useEffect } from 'react'
import { observer } from 'mobx-react'
import { Cascader, Button, Collapse, Input, InputNumber, message, Modal, Popconfirm, Radio, Select, Table, Divider } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import useInputValue from '../../utils/inputValue'
import ContainerRow from '../../components/ContainerRow'
import store from './store'
import './styles.less'
import { data, CreateApp, IObject } from './interfaces'
import ContTable from './cont_table'
import CreateFooter from './create_footer'

const { Option } = Select;
const { Panel } = Collapse;


/**
 * 高级配置 - 公用方法
 */

// 根据 key 查找下标
const inquireIndex = (data: data[] | IObject[], key: number | string, name: string = 'key'): number => {
    let index = 0;
    data.forEach((item, i) => {
        if (item[name] === key) index = i
    });
    return index
}

// 根据 index 删除 list 上的数据
const removeList = (data: data[], index: number): data[] => {
    data.splice(index, 1);
    return [...data]
}

// 根据 index 修改 list 上的状态
const updateList = (data: data[], index: number): data[] => {
    data[index].editing = true
    return [...data]
}

// 只返回 editing 为 false 的 data
const submitList = (data: data[]): data[] => data.filter(item => !item.editing)


/**
 * 高级配置
 */
interface Config {
    confirm(data: data[]): void//同意的回调函数
}

//集群
const Clusters: React.FC<Config> = ({ confirm }) => {
    const [list, setList] = useState([] as Array<data>);
    return (
        <ContTable
            dataLength={10}
            columns={[
                {
                    title: '名称',
                    key: 'key',
                    dataIndex: 'cluster_label',
                    render: (text: string, rcd: data) => {
                        return rcd.editing ? (
                            <Select
                                style={{ width: 120 }}
                                defaultValue={text || '请选择'}
                                onChange={(e, option: any) => {
                                    list[inquireIndex(list, rcd.key)].cluster_label = e
                                    list[inquireIndex(list, rcd.key)].cluster_name = option.children
                                }}>
                                {store.cluster.map(({ cluster_label, cluster_name }, i) => <Option key={i} value={cluster_label}>{cluster_name}</Option>)}
                            </Select>
                        ) : (rcd.cluster_name)
                    }
                },
                {
                    title: '实例数',
                    key: 'key',
                    dataIndex: 'replicas',
                    render: (text: string, rcd: data) => {
                        return rcd.editing ? (
                            <Input className="form-input" placeholder="请输入"
                                defaultValue={text}
                                onBlur={e => {
                                    let val = e.target.value
                                    list[inquireIndex(list, rcd.key)].replicas = val
                                }} />
                        ) : (text)
                    }
                },
                {
                    title: '操作',
                    key: 'action',
                    render: (text: any, record: data) => record.editing ? (
                        <><a onClick={() => {
                            list[inquireIndex(list, record.key)].editing = false
                            setList([...list])
                            confirm(submitList(list));
                        }}>确定</a></>
                    ) : (<>
                        <Popconfirm title="您确认要删除吗?" okText="确认" cancelText="取消"
                            onConfirm={() => setList(removeList(list, inquireIndex(list, record.key)))}>
                            <a>删除</a>
                        </Popconfirm>&nbsp;&nbsp;
                        <a onClick={() => setList(updateList(list, inquireIndex(list, record.key)))}>修改</a>
                    </>)
                }
            ]}
            add={() => {
                setList([...list, {
                    key: Date.now(),
                    cluster_label: '',
                    cluster_name: '',
                    replicas: 1,
                    editing: true
                }])
                console.log(list);
            }}

            datas={list} />
    )
}


interface ICPUPorps {
    data: Array<IObject>
}
//容器规格
const CPUContainer: React.FC<ICPUPorps> = ({ data }) => {
    const [nodeType, setNodeType] = useState('')
    const [nodeList, setNodeList] = useState([{}]);

    const rowSelection = {
        onChange: (selectedRowKeys: any, selectedRows: any) => {
            console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
        },
    };

    return (<>
        {/* 规格选择 */}
        <section className="conts-type">
            <Radio.Group value={nodeType}
                onChange={e => {
                    const val = e.target.value
                    const index = inquireIndex(data, val, 'node_type')
                    const { pod_models } = data[index]
                    setNodeType(val)
                    setNodeList([...pod_models])
                }}>
                {data.map(({ node_type, node_type_cn }, i) =>
                    <Radio.Button value={node_type} key={Date.now() * i}> {node_type_cn} </Radio.Button>
                )}
            </Radio.Group>
        </section>

        {/* cpu选择 */}
        <section className='conts-cpu'>
            <Table
                pagination={false}
                bordered={true}
                rowSelection={{
                    type: 'radio',
                    ...rowSelection,
                }}
                columns={[
                    {
                        title: '名称',
                        dataIndex: 'name',
                    },
                    {
                        title: 'CPU',
                        dataIndex: 'cpu',
                    },
                    {
                        title: '内存',
                        dataIndex: 'mem',
                    },
                    {
                        title: '存储',
                        dataIndex: 'disk',
                    },
                    {
                        title: 'GPU',
                        dataIndex: 'gpu',
                    },
                ]}
                dataSource={
                    nodeList.map((item: IObject) => ({
                        key: item.id,
                        name: item.name,
                        cpu: (item.cpu || '- ') + '核',
                        gpu: (item.gpu_name || '-') + ' * ' + (item.gpu_num || '-'),
                        mem: (item.mem || '- ') + 'G',
                        disk: (item.disk || '- ') + 'G',
                    }))
                }
            />
        </section>
    </>)
}



//镜像
interface IImagePorps {
    data: IObject
    onClick(): void
}
const ImageContainer: React.FC<IImagePorps> = ({ data, onClick }) => {
    const [radio, setRadio] = useState(0)
    const [image, setImage] = useState(0)
    let { pri_mirror, pub_mirror } = data
    let list: Array<IObject> = []
    if (!pri_mirror) return <></>;
    pri_mirror = pri_mirror.map((item: IObject) => ({ ...item }))
    pub_mirror = pub_mirror.map((item: IObject) => ({ ...item }))

    if (!radio) {//自有 pri_mirror
        list = pri_mirror
    } else {//公共 pub_mirror
        list = pub_mirror
    }

    return (<>
        <Radio.Group value={radio} onChange={e => {
            setRadio(e.target.value)
        }}>
            <Radio value={0}>自有镜像</Radio>
            <Radio value={1}>公共镜像</Radio>
        </Radio.Group>
        <Cascader
            options={
                list.map((item) => {
                    const mirrorTags = [...item.MirrorTags]
                    return {
                        value: item.id,
                        label: item.name,
                        children: mirrorTags.map(item => ({
                            value: item.id,
                            label: item.tag_name,
                        })),
                    }
                })
            }
            placeholder="请选择"
            expandTrigger="hover"
            style={{ marginTop: '10px' }}
            displayRender={(label) => {
                return label[label.length - 1];
            }}
            onChange={() => {
                console.log('object');
            }}
        />
    </>)
}

//负载均衡
const LoadBalance: React.FC<Config> = ({ confirm }) => {
    const [list, setList] = useState([] as Array<data>);
    return (
        <ContTable
            dataLength={1}
            columns={[
                {
                    title: '服务端口',
                    key: 'key',
                    dataIndex: 'container_port',
                    render: (text: number, rcd: data) => {
                        return rcd.editing ? (
                            <Input className="form-input" placeholder="请输入"
                                defaultValue={text}
                                onBlur={e => {
                                    let val = e.target.value
                                    if (/^[123456789]\d{1,4}$/.test(val)) {
                                        list[inquireIndex(list, rcd.key)].container_port = val
                                    } else {
                                        message.warning("请输入有效端口")
                                    }
                                }} />
                        ) : (text)
                    }
                },
                {
                    title: '协议',
                    key: 'key',
                    dataIndex: 'protocol',
                    render: (text: string, rcd: data) => {
                        return rcd.editing ? (
                            <Select defaultValue="TCP" style={{ width: 120 }} onChange={e => {
                                list[inquireIndex(list, rcd.key)].protocol = e
                            }}>
                                <Option value="TCP">TCP</Option>
                                <Option value="UDP">UDP</Option>
                            </Select>
                        ) : (text)
                    }
                },
                {
                    title: '操作',
                    key: 'action',
                    render: (text: any, record: data) => record.editing ? (
                        <><a onClick={() => {
                            list[inquireIndex(list, record.key)].editing = false
                            setList([...list])
                            confirm(submitList(list));
                        }}>确定</a></>
                    ) : (<>
                        <Popconfirm title="您确认要删除吗?" okText="确认" cancelText="取消"
                            onConfirm={() => setList(removeList(list, inquireIndex(list, record.key)))}>
                            <a>删除</a>
                        </Popconfirm>&nbsp;&nbsp;
                        <a onClick={() => setList(updateList(list, inquireIndex(list, record.key)))}>修改</a>
                    </>)
                }
            ]}
            add={() => {
                setList([...list, {
                    key: Date.now(),
                    editing: true,
                    container_port: 0,
                    protocol: 'TCP'
                }])
            }}

            datas={list} />
    )
}

//目录映射
const HostPathVolumes: React.FC<Config> = ({ confirm }) => {
    const [list, setList] = useState([] as Array<data>);
    return (
        <ContTable
            dataLength={10}
            columns={[
                {
                    title: '容器地址',
                    key: 'key',
                    dataIndex: 'to',
                    render: (text: string, rcd: data) => {
                        return rcd.editing ? (
                            <Input className="form-input" placeholder="请输入"
                                defaultValue={text}
                                onBlur={e => {
                                    let val = e.target.value
                                    list[inquireIndex(list, rcd.key)].to = val
                                }} />
                        ) : (text)
                    }
                },
                {
                    title: '主机地址',
                    key: 'key',
                    dataIndex: 'from',
                    render: (text: string, rcd: data) => {
                        return rcd.editing ? (
                            <Input className="form-input" placeholder="请输入"
                                defaultValue={text}
                                onBlur={e => {
                                    let val = e.target.value
                                    list[inquireIndex(list, rcd.key)].from = val
                                }} />
                        ) : (text)
                    }
                },
                {
                    title: '权限',
                    key: 'key',
                    dataIndex: 'read_only',
                    render: (text: number, rcd: data) => {
                        return rcd.editing ? (
                            <Select defaultValue={Number(rcd.read_only)} style={{ width: 120 }}  onChange={e => {
                                list[inquireIndex(list, rcd.key)].read_only = Boolean(e)
                            }}>
                                <Option value={1}>只读</Option>
                                <Option value={0}>读写</Option>
                            </Select>
                        ) : (rcd.read_only ? '只读' : '读写')
                    }
                },
                {
                    title: '操作',
                    key: 'action',
                    render: (text: any, record: data) => record.editing ? (
                        <><a onClick={() => {
                            list[inquireIndex(list, record.key)].editing = false
                            setList([...list])
                            confirm(submitList(list));
                        }}>确定</a></>
                    ) : (<>
                        <Popconfirm title="您确认要删除吗?" okText="确认" cancelText="取消"
                            onConfirm={() => setList(removeList(list, inquireIndex(list, record.key)))}>
                            <a>删除</a>
                        </Popconfirm>&nbsp;&nbsp;
                        <a onClick={() => setList(updateList(list, inquireIndex(list, record.key)))}>修改</a>
                    </>)
                }
            ]}
            add={() => {
                setList([...list, {
                    key: Date.now(),
                    to: '',
                    from: '',
                    read_only: true,
                    editing: true
                }])
            }}

            datas={list} />
    )
}

//环境变量
const ENV: React.FC<Config> = ({ confirm }) => {
    const [list, setList] = useState([] as Array<data>);
    return (
        <ContTable
            dataLength={10}
            columns={[
                {
                    title: '变量名称',
                    key: 'key',
                    dataIndex: 'prop',
                    render: (text: string, rcd: data) => {
                        return rcd.editing ? (
                            <Input className="form-input" placeholder="请输入"
                                defaultValue={text}
                                onBlur={e => {
                                    let val = e.target.value
                                    list[inquireIndex(list, rcd.key)].prop = val
                                }} />
                        ) : (text)
                    }
                },
                {
                    title: '变量值',
                    key: 'key',
                    dataIndex: 'value',
                    render: (text: string, rcd: data) => {
                        return rcd.editing ? (
                            <Input className="form-input" placeholder="请输入"
                                defaultValue={text}
                                onBlur={e => {
                                    let val = e.target.value
                                    list[inquireIndex(list, rcd.key)].value = val
                                }} />
                        ) : (text)
                    }
                },
                {
                    title: '操作',
                    key: 'action',
                    render: (text: any, record: data) => record.editing ? (
                        <><a onClick={() => {
                            list[inquireIndex(list, record.key)].editing = false
                            setList([...list])
                            confirm(submitList(list));
                        }}>确定</a></>
                    ) : (
                            <>
                                <Popconfirm title="您确认要删除吗?" okText="确认" cancelText="取消"
                                    onConfirm={() => setList(removeList(list, inquireIndex(list, record.key)))}>
                                    <a>删除</a>
                                </Popconfirm>&nbsp;&nbsp;
                                <a onClick={() => setList(updateList(list, inquireIndex(list, record.key)))}>修改</a>
                            </>)
                }
            ]}
            add={() => {
                setList([...list, {
                    key: Date.now(),
                    prop: '',
                    value: '',
                    editing: true
                }])
            }}
            datas={list} />
    )
}


/**
 * 健康检查 - HTTP请求检查
 */
interface HttpValue {
    path: string
    port: number
    initialDelaySeconds: number
    timeoutSeconds: number
}

interface HttpProbeProps {
    // value?: GPUValue;
    onChange: (value: HttpValue) => void;
}

const HttpProbe: React.FC<HttpProbeProps> = ({ onChange }) => {
    const path = useInputValue(''),
        port = useInputValue(0),
        initialDelaySeconds = useInputValue(0),
        timeoutSeconds = useInputValue(0);

    onChange({
        path: path.value,
        port: port.value,
        initialDelaySeconds: initialDelaySeconds.value,
        timeoutSeconds: timeoutSeconds.value,
    });
    return (<>
        <ContainerRow title="路径" inputWidth="60%" >
            <Input {...path} />
        </ContainerRow>

        <ContainerRow title="端口" inputWidth="60%" >
            <InputNumber {...port} />
        </ContainerRow>

        <ContainerRow title="延迟时间/秒" inputWidth="60%" >
            <InputNumber {...initialDelaySeconds} />
        </ContainerRow>

        <ContainerRow title="超时时间/秒" inputWidth="60%" >
            <InputNumber {...timeoutSeconds} />
        </ContainerRow>
    </>)
}


/**
 * 健康检查 - 执行命令检查
 */
interface OrderValue {
    command: string
    initialDelaySeconds: number
    timeoutSeconds: number
}

interface OrderProbeProps {
    // value?: GPUValue;
    onChange: (value: OrderValue) => void;
}

const OrderProbe: React.FC<OrderProbeProps> = ({ onChange }) => {
    const command = useInputValue(''),
        initialDelaySeconds = useInputValue(0),
        timeoutSeconds = useInputValue(0);

    onChange({
        command: command.value,
        initialDelaySeconds: initialDelaySeconds.value,
        timeoutSeconds: timeoutSeconds.value,
    });
    return (<>
        <ContainerRow title="执行命令" inputWidth="60%" >
            <Input {...command} />
        </ContainerRow>

        <ContainerRow title="延迟时间/秒" inputWidth="60%" >
            <InputNumber {...initialDelaySeconds} />
        </ContainerRow>

        <ContainerRow title="超时时间/秒" inputWidth="60%" >
            <InputNumber {...timeoutSeconds} />
        </ContainerRow>
    </>)
}


/**
 * 健康检查 - TCP检查
 */
interface TCPValue {
    port: number
    initialDelaySeconds: number
    timeoutSeconds: number
}

interface TCPProbeProps {
    // value?: GPUValue;
    onChange: (value: TCPValue) => void;
}

const TCPProbe: React.FC<TCPProbeProps> = ({ onChange }) => {
    const port = useInputValue(0),
        initialDelaySeconds = useInputValue(0),
        timeoutSeconds = useInputValue(0);

    onChange({
        port: port.value,
        initialDelaySeconds: initialDelaySeconds.value,
        timeoutSeconds: timeoutSeconds.value,
    });
    return (<>
        <ContainerRow title="端口" inputWidth="60%" >
            <InputNumber {...port} />
        </ContainerRow>

        <ContainerRow title="延迟时间/秒" inputWidth="60%" >
            <InputNumber {...initialDelaySeconds} />
        </ContainerRow>

        <ContainerRow title="超时时间/秒" inputWidth="60%" >
            <InputNumber {...timeoutSeconds} />
        </ContainerRow>
    </>)
}


// ===========================================
// 初始化
// ===========================================

interface GpuType {
    name_cn: string
    name_en: string
}

@observer
class Index extends Component {
    state = {
        // 错误提示
        required: {
            addr: false,
            image: false,
        },
        //健康检查
        type: 0,


    };

    //更改 required 或者 error 内容
    empty = (key: string, value: boolean): void => {
        let { required } = this.state
        // @ts-ignore
        required[key] = value;
        this.setState({ required })
    }

    // 提交验证 - 非空验证
    required(data: object): Array<string> {
        let nullValue: string[] = [];
        console.log(data)
        for (const key in data) {
            // @ts-ignore
            const value = data[key];
            if (!value) nullValue.push(key)
        }
        return nullValue;
    }

    //更改状态
    setRequired = (name: string, state: boolean): void => {
        const required = Object.assign({}, this.state.required, { name: state });
        this.setState({ required })
    }


    render() {
        const { cluster, areas, addr, image, cpu, mem, protocol, isProxy, containerPort, hostPathVolumes, env } = store;
        const { required } = this.state;
        let httpProbeData = {},
            orderProbeData = {},
            tcpProbeData = {};


        return (<div className='create-container'>

            {/* 区域 */}
            <ContainerRow title='区域' inputWidth='264px' required={required.addr}>
                <Select
                    value={addr || '请选择'}
                    onFocus={() => { this.empty('addr', false) }}
                    onChange={(e) => {
                        const { area_name, cluster } = areas[e as unknown as number]
                        store.cluster = [...cluster]
                        store.setAddr(area_name)
                    }}>
                    {areas.map((item, i) =>
                        <Option key={i} value={i}>{item.area_name}</Option>)}
                </Select>
            </ContainerRow>


            {/* 集群 */}
            <ContainerRow title='集群' inputWidth='514px' className='advanced' required={required.image}>
                <div className="advanced-cont">
                    <Clusters confirm={data => {
                        // console.log(data);
                        // const datas = data.map(({ to, from, read_only }) => ({ to, from, read_only }))
                        // store.setHostPathVolumes(datas)

                        const clusterLabels = data.map(item => item.cluster_label)
                        if (!clusterLabels.length) return;
                        store.initGetNodeType(clusterLabels)
                    }} />
                </div>
            </ContainerRow>


            {/* 容器规格开始 */}
            <ContainerRow title="容器规格" inputWidth='100%' className='conts paralleling' required={required.image}>
                <CPUContainer data={[...store.nodeList]} />
            </ContainerRow>
            {/* 容器规格结束 */}



            {/* 镜像 */}
            <ContainerRow title="镜像" className='image paralleling' required={required.image}>
                <ImageContainer data={{ ...store.images }} onClick={() => {

                }} />
            </ContainerRow>


            {/* 高级配置开始 */}
            <ContainerRow title="高级配置" inputWidth='1260px' className='paralleling'>
                <section className='advanced advanced-paralleling'>
                    <Collapse ghost onChange={(key: string | string[]) => {

                    }}>
                        <Panel header="负载均衡" key="1">
                            <div className="advanced-cont">
                                <LoadBalance confirm={data => {
                                    const { container_port, protocol } = data[0]
                                    store.setContainerPort(Number(container_port))
                                    store.setProtocol(protocol)
                                    store.setIsProxy(true)
                                }} />
                            </div>
                        </Panel>

                        {/* 目录映射 */}
                        <Panel header="目录映射" key="2">
                            <div className="advanced-cont">
                                <HostPathVolumes confirm={data => {
                                    const datas = data.map(({ to, from, read_only }) => ({ to, from, read_only }))
                                    store.setHostPathVolumes(datas)
                                }} />
                            </div>
                        </Panel>

                        {/* 环境变量 */}
                        <Panel header="环境变量" key="3">
                            <div className="advanced-cont">
                                <ENV confirm={data => {
                                    const datas: { [prop: string]: string } = {};
                                    data.forEach(({ prop, value }) => datas[prop] = value)
                                    store.setEnv(datas)
                                }} />
                            </div>
                        </Panel>


                        {/* 健康检查 */}
                        <Panel className="advanced-check" header="健康检查" key="4">
                            <div className="advanced-cont">
                                {/* 存活探针开始 */}
                                <div className="liveness-title">应用存活探针：</div>
                                <div className='liveness-probe'>
                                    <ContainerRow title="检查方式" className='probe-title'>
                                        <Radio.Group value={this.state.type}
                                            onChange={(e) => this.setState({ type: e.target.value })}>
                                            <Radio value={0}>不配置</Radio>
                                            <Radio value={1}>HTTP请求检查</Radio>
                                            <Radio value={2}>执行命令检查</Radio>
                                            <Radio value={3}>TCP检查</Radio>
                                        </Radio.Group>
                                    </ContainerRow>
                                    {/* 内容 */}
                                    <div className="liveness-probe-cont">
                                        {/* ============================ */}
                                        {this.state.type === 1 &&
                                            <HttpProbe onChange={value => httpProbeData = value} />}

                                        {this.state.type === 2 &&
                                            <OrderProbe onChange={value => orderProbeData = value} />}

                                        {this.state.type === 3 &&
                                            <TCPProbe onChange={value => tcpProbeData = value} />}
                                        {/* ============================ */}
                                    </div>
                                </div>
                            </div>
                            <div className="advanced-cont">
                                {/* 应用业务探针 */}
                                <div className="liveness-title">应用业务探针：</div>
                                <div className='liveness-probe'>
                                    <ContainerRow title="检查方式" className='probe-title'>
                                        <Radio.Group value={this.state.type}
                                            onChange={(e) => this.setState({ type: e.target.value })}>
                                            <Radio value={0}>不配置</Radio>
                                            <Radio value={1}>HTTP请求检查</Radio>
                                            <Radio value={2}>执行命令检查</Radio>
                                            <Radio value={3}>TCP检查</Radio>
                                        </Radio.Group>
                                    </ContainerRow>
                                    {/* 内容 */}
                                    <div className="liveness-probe-cont">
                                        {/* ============================ */}
                                        {this.state.type === 1 &&
                                            <HttpProbe onChange={value => httpProbeData = value} />}

                                        {this.state.type === 2 &&
                                            <OrderProbe onChange={value => orderProbeData = value} />}

                                        {this.state.type === 3 &&
                                            <TCPProbe onChange={value => tcpProbeData = value} />}
                                        {/* ============================ */}
                                    </div>
                                </div>
                            </div>
                        </Panel>

                        {/* 重启策略 */}
                        <Panel header="重启策略" key="5">
                            <div className="advanced-cont">
                                <Radio.Group>
                                    <Radio value={1}>总是重启</Radio>
                                    <Radio value={2}>失败时重启</Radio>
                                    <Radio value={3}>不重启</Radio>
                                </Radio.Group>
                            </div>
                        </Panel>

                        {/* 启动命令 */}
                        <Panel header="启动命令" key="6">
                            <div className="advanced-cont">
                                <Select mode="tags" style={{ width: '100%' }} onChange={(arr: string[]) => store.setCommand(arr)}
                                    tokenSeparators={[' ']} placeholder="请输入启动命令" ></Select>
                            </div>
                        </Panel>
                    </Collapse>
                </section>
            </ContainerRow>
            {/* 高级配置结束 */}


            {/* 底部按钮开始 */}
            <CreateFooter evts={() => {
                return true;
                /**
                 * 返回为true则会进入下一页，否之则会弹出
                 * 
                 * 这下面的代码是提交代码，由于现在第二个页面不做提交了所以下面的内容需要放在第三个页面上
                 * 本页只需要加一些必填非必填的校验即可，内容样式是根据我封装的 ContainerRow 里面的 required这个
                 * 字段进行判断是否给红色边框提示错误的，这一页的表单必填验证我还没试如果有问题的话最好表单给他个
                 * 默认的让他填写或者是等我回来把样式补上，辛苦啦
                 */
                // const { appId, addr, namespace, appName, replicas, labels, nodeAffinityLabels, podAffinityLabels, describe, restartPolicy, command, image, cpu, mem, gpu, gpuType, isProxy, containerPort, protocol, hostPathVolumes, env, isLivenessProbe, livenessProbe } = store
                // const required: string[] = this.required({ image })
                // let data: CreateApp = {
                //     cpu,
                //     addr,
                //     image,
                //     replicas,
                //     describe,
                //     namespace,
                //     mem: Math.round(mem * 1024),
                //     app_id: appId,
                //     app_name: appName,
                //     is_proxy: isProxy,
                //     cluster_label: clusterLabel,
                //     is_liveness_probe: isLivenessProbe,
                // }
                // if (labels) data.labels = { ...labels }
                // if (nodeAffinityLabels) data.node_affinity_labels = { ...nodeAffinityLabels }
                // if (podAffinityLabels) data.pod_affinity_labels = { ...podAffinityLabels }
                // if (restartPolicy) data.restart_policy = restartPolicy
                // // if (command) data.command = [...command]
                // if (gpu) data.gpu = gpu
                // if (gpuType) data.gpu_type = gpuType
                // if (containerPort) data.container_port = containerPort
                // if (protocol) data.protocol = protocol
                // if (hostPathVolumes) data.host_path_volumes = hostPathVolumes
                // if (env) data.env = env
                // if (livenessProbe) data.liveness_probe = livenessProbe
                // if (required.length) { //非空校验
                //     let states: object = {}
                //     // @ts-ignore
                //     required.forEach(item => states[item] = true)
                //     this.setState({ required: states })
                //     return false
                // } else {
                //     store.setCreateApp(data).then(id => {
                //         if (id) {
                //             const { setCreateAppId, setCurrent, current } = store
                //             setCreateAppId(id);
                //             setCurrent(current + 1)
                //             return true;
                //         }
                //     })
                // }
                // return false;
            }} />
        </div>
        );
    }
}


export default Index
