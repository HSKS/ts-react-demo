import React, { Component } from 'react'
import { observer } from 'mobx-react'
import { Result, Button, Space } from 'antd';
import { Link } from "react-router-dom";
import routes from "../../config/routes";
import store from './store'
import './styles.less'


@observer
class Index extends Component {

    render() {
        const OK = require('../../assets/ok.png');
        return (
            <div className='create-finish'>
                <section className='finish'>
                    <div className='finish-title'>
                        <img src={OK} />
                        <strong>创建成功</strong>
                        <p>
                            容器应用创建成功，您可重新创建一个新应用，或者返回到概 览页面查看该应用的详细信息。
                        </p>
                    </div>
                    <div className='finish-cont'>
                        <strong>应用创建日志</strong>
                        <ul>
                            <li className='finish-cont-list'>
                                <span className='time'>2020-09-16 17:12:12</span>
                                <span className='text'>检查应用配置区域信息</span>
                            </li>
                            <li className='finish-cont-list'>
                                <span className='time'>2020-09-17 14:21:25</span>
                                <span className='text'>检查应用配置区域信息</span>
                            </li>
                            <li className='finish-cont-list'>
                                <span className='time'>2020-09-18 16:10:45</span>
                                <span className='text'>检查应用配置区域信息</span>
                            </li>
                            <li className='finish-cont-list'>
                                <span className='time'>2020-09-19 13:01:22</span>
                                <span className='text'>检查应用配置区域信息</span>
                            </li>
                            <li className='finish-cont-list'>
                                <span className='time'>2020-09-19 13:01:22</span>
                                <span className='text'>检查应用配置区域信息</span>
                            </li>
                        </ul>
                    </div>
                </section>

                {/* 底部按钮 */}
                <div className="steps-action">
                    <Space>
                        <Button >继续创建</Button>
                        <Button type="primary" >返回</Button>
                    </Space>
                </div>
            </div>
            // <Result
            //     status="success"
            //     title="创建成功"
            //     subTitle="容器应用创建成功，您可以点击确认查看详细信息。"
            //     extra={[
            //         ( <Link key="console" to={{ pathname: `${routes.deployment_details}/${store.createAppId}` }} ><Button type="primary">确认</Button></Link>),
            //         (<Link key="buy" to={routes.deployment}><Button>返回</Button></Link>),
            //     ]}
            // />
        )
    }
}


export default Index
