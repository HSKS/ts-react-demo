export interface CreateSystem {
    name_cn: string //应用ID
    name_en: string //区域
    domain_id: number //集群
    is_private: number //资源组
    private_user: string
    private_pass: string
    describe: string//描述
    cpu: number //应用名称
    mem: number//mem
    disk: number //实例数
    gpu_high: number//gpu
    gpu_mid: number
    gpu_low: number
}

export interface NsInfo {
    app_number: number
    cpu: number
    created_at: string
    describe: string
    disk: number
    domain_id: number
    gpu_high: number
    gpu_low: number
    gpu_mid: number
    group_id: number
    group_name: string
    id: number
    is_deleted: number
    mem: number
    name_cn: string
    name_en: string
    pod_number: number
    real_name: string
    second_domain_id: number
    second_level_domain: string
    top_domain_id: number
    top_level_domain: string
    total_cpu: number
    total_mem: number
    used_cpu: number
    used_mem: number
    username: string
}
