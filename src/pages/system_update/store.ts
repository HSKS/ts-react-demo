import {observable, action} from "mobx"
import request from '../../utils/request'
import apis from '../../config/apis'
import {Domain} from "../system/interfaces";
import {CascaderOptionType} from "antd/lib/cascader";
import {NsInfo} from "./interfaces";

class Store {
    namespace: string = ''
    @observable name_cn:undefined|string = undefined //应用ID
    @observable name_en:undefined|string = undefined //区域
    @observable  domain_id = new Array<number>()//域ID
    @observable is_private = 1 //资源组
    // @observable private_user = ''
    // @observable private_pass = ''
    @observable describe = ''//描述
    @observable cpu = 0 //应用名称
    @observable mem = 0//mem
    @observable disk = 0 //实例数
    @observable gpu_high = 0//gpu
    @observable gpu_mid = 0
    @observable gpu_low = 0

    @action GetNsInfo = async () => {
        const api = apis.GetNsInfo
        const resp: NsInfo = await request(api, {name_en: this.namespace})
        if (!resp) return
        this.name_cn = resp.name_cn
        this.name_en = resp.name_en
        this.domain_id = [resp.top_domain_id, resp.second_domain_id]
        this.describe = resp.describe
        this.cpu = resp.cpu
        this.mem = resp.mem
        this.disk = resp.disk
        this.gpu_high = resp.gpu_high
        this.gpu_mid = resp.gpu_mid
        this.gpu_low = resp.gpu_low
    }

    @action submit = async () => {
        const api = apis.CreateNamespace
        const params = {
            name_cn: this.name_cn,
            name_en: this.name_en,
            domain_id: this.domain_id[this.domain_id.length-1],
            is_private: this.is_private,
            // private_user: this.private_user,
            // private_pass: this.private_pass,
            describe: this.describe,
            cpu: this.cpu,
            mem: this.mem,
            disk: this.disk,
            gpu_high: this.gpu_high,
            gpu_mid: this.gpu_mid,
            gpu_low: this.gpu_low
        }
        const resp = await request(api, params)
        if (!resp) return false
        return true
    }

    buildCascaderOptions = (arr: Domain[] | undefined): CascaderOptionType[] | undefined => {
        if (arr && arr.length) {
            return arr.map(v => {
                return {
                    label: v.domain_name,
                    value: v.id,
                    children: this.buildCascaderOptions(v.Child)
                } as CascaderOptionType
            })
        } else {
            return undefined
        }
    }

    @observable domains: CascaderOptionType[] | undefined = new Array<CascaderOptionType>()
    @action GetDomain = async () => {
        const api = apis.GetDomain
        const resp = await request(api)
        if (!resp) return
        let data = resp as Domain[]
        let dm = this.buildCascaderOptions(data)
        console.log(dm)
        this.domains = dm
    }

}

export default new Store()