import React, {Component} from "react"
import {observer} from "mobx-react"
import store from './store'
import {RouteComponentProps, withRouter} from 'react-router-dom';
import {message} from 'antd'
import "./styles.less"
import routes from "../../config/routes";
import Container from '../../components/Container'
import StringInput from "../../components/StringInput";
import TextAreaInput from "../../components/TextAreaInput";
import RadioSelect from "../../components/RadioSelect";
import MultiSelect from "../../components/MultiSelect";
import CButton from "../../components/CButton";
import Counter from "../../components/Counter";
import NumberInput from "../../components/NumberInput";

interface ITitle {
    title: string
}

const CTitle: React.FC<ITitle> = ({title}) => {
    return (
        <div className='c-title'>
            <i className='line'/><span className='title'>{title}</span>
        </div>
    )
}

interface Params {
    namespace: string
}
@observer
class Index extends Component<RouteComponentProps, {}> {
    componentDidMount() {
        const {namespace} = this.props.match.params as Params
        store.namespace = namespace
        store.GetDomain()
        store.GetNsInfo()
    }

    checkParams = () => {
        const {name_cn, name_en, domain_id} = store
        let tip = ''
        if (!name_cn) tip = '请输入业务系统名称'
        if (!name_en) tip = '请输入业务系统英文名称'
        if (!domain_id) tip = '请选择一级域二级域'
        if (tip) {
            message.warning(tip)
            return false
        }
        return true
    }

    render() {
        const {name_cn, name_en, domains, domain_id, is_private, describe, cpu, mem, disk, gpu_high, gpu_mid, gpu_low} = store
        return (
            <Container title='编辑业务系统' className='system-update'>
                <div className='content-cont'>
                    <CTitle title='基础信息'/>
                    <StringInput titleWidth={140} title='业务系统名称' placeholder='请输入业务系统名称' value={name_cn}
                        required={name_cn !== ''}
                        onChange={(val) => {
                            store.name_cn = val
                        }} />
                    <StringInput titleWidth={160} title='业务系统英文名称' placeholder='请输入业务系统英文名称' value={name_en}
                        required={name_en !== ''}
                        onChange={(val) => {
                            store.name_en = val
                        }} />

                    <MultiSelect titleWidth={140} title='一级、二级域' data={domains} value={domain_id}
                        required={domain_id !== null}
                        onChange={(val) => {
                            store.domain_id = val
                        }} />
                    <RadioSelect titleWidth={140} title='镜像仓库类型' data={[{ label: '公开', value: 0 }, { label: '私有', value: 1 }]}
                        value={is_private} onChange={(val) => {
                            store.is_private = val
                        }} />
                    <TextAreaInput titleWidth={140} title='业务系统描述' placeholder='请输入业务系统描述' required value={describe}
                        onChange={(val) => {
                            store.describe = val
                        }} />
                    <CTitle title='配额信息' />
                    <div className='resource-quota'>
                        <NumberInput title='CPU(Core)' value={cpu} min={0} onChange={(val) => {
                            if (typeof val === 'number') store.cpu = val
                        }}/>
                        <NumberInput title='内存(GB)' value={mem} min={0} onChange={(val) => {
                            if (typeof val === 'number') store.mem = val
                        }}/>
                        <NumberInput title='磁盘(GB)' value={disk} min={0} onChange={(val) => {
                            if (typeof val === 'number') store.disk = val
                        }}/>
                    </div>


                    <div className='resource-quota'>
                        <NumberInput title='高规格GPU' value={gpu_high} min={0} onChange={(val) => {
                            if (typeof val === 'number') store.gpu_high = val
                        }}/>
                        <NumberInput title='中规格GPU' value={gpu_mid} min={0} onChange={(val) => {
                            if (typeof val === 'number') store.gpu_mid = val
                        }}/>
                        <NumberInput title='低规格GPU' value={gpu_low} min={0} onChange={(val) => {
                            if (typeof val === 'number') store.gpu_low = val
                        }}/>
                    </div>
                    <div className='submit'>
                        <CButton text='提交' type='primary' onClick={
                            async () => {
                                const ok = await store.submit()
                                if (ok) {
                                    this.props.history.replace(routes.system)
                                }
                            }
                        }/>
                    </div>
                </div>
            </Container>
        )
    }
}


export default withRouter(Index as any);