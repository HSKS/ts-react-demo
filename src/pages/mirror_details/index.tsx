import React, { Component } from "react"
import { observer } from "mobx-react"
import { Button, Input, PageHeader, Space, Tabs, Table,Spin} from 'antd'
import copy from 'copy-to-clipboard';
import { RouteComponentProps } from 'react-router-dom';
import "./styles.less"
import store from "./store";
import Container from '../../components/Container'
import { strict } from "assert";
import formatDate from "../../utils/formatDate";
import {MirrorInfo, MirrorTags} from './interfaces'

const { TabPane } = Tabs;
interface ITitle {
    title: string
}

const CTitle: React.FC<ITitle> = ({ title }) => {
    return (
        <div className='c-title'>
            <i className='line' /><span className='title'>{title}</span>
        </div>
    )
}
interface Search {
    name: string | undefined
    setName: (val: string) => void
    search: () => void
}
const Search: React.FC<Search> = ({ search, name, setName }) => {
    return (
        <Space className='space'>
            <Input
                className='app-exp-input input'
                placeholder='请输入镜像版本名称检索'
                value={name}
                onChange={(e) => {
                    setName(e.target.value)
                }}
            />
            <Button className='app-but' type='primary' onClick={() => search()}>搜索</Button>
        </Space>

    )
}

interface EditInput {
    isEdit: boolean
    title: string
    value: string
    onChange?: (val: string) => void
}

const EditInput: React.FC<EditInput> = ({ isEdit, title, value, onChange }) => {

    return isEdit ? (
        <div className='edit-input'>
            <label className='label'>{title}：</label>
            <Input className='input' value={value} onChange={(e) => {
                if (onChange) onChange(e.target.value)
            }} />
        </div>
    ) : (
            <div className='edit-input'>
                <label className='label'>{title}：</label>
                <span className='input'>{value}</span>
            </div>
        )
}

@observer
class Index extends Component<RouteComponentProps, {}> {
    componentDidMount(): void {
        const mirrorInfoStr = localStorage.getItem("mirror_center_details")
        if (mirrorInfoStr) {
            try {
                const mirrorInfo: MirrorInfo = JSON.parse(mirrorInfoStr)
                console.log(mirrorInfo)
                store.mirror_name = mirrorInfo.name
                store.description = mirrorInfo.description
                store.download_count = mirrorInfo.download_count
                store.updated_at = mirrorInfo.updated_at
                store.collect_count = mirrorInfo.collect_count
                store.imageTags = mirrorInfo.mirror_tags
                store.showImageTags = mirrorInfo.mirror_tags
            } catch (e) {
                console.log(e)
            }
        }
    }

    render() {
        const {  mirror_name, collect_count,  description, imageTags,searchMirrorName, showImageTags } = store
        return (
            <Container title='镜像中心详情' className='image-details'>
                {/* 内容开始 */}
                <section className="image-content">
                    <CTitle title='基础信息' />
                    <div className='top'>
                        <div className='row'>
                            <div className='col'>
                                <EditInput isEdit={false} title='镜像名称' value={mirror_name} onChange={(val) => {
                                    store.mirror_name = val
                                }} />
                                <EditInput isEdit={false} title='下载量' value={`${description}`} />
                            </div>

                            <div className='col'>
                                <EditInput isEdit={false} title='最近更新' value={`${description}`} onChange={(val) => {
                                   
                                }} />
                                <EditInput isEdit={false} title='收藏量' value={`${collect_count}`}onChange={(val) => {
 
                                }} />
                            </div>
                        </div>
                    </div>
                    <Tabs defaultActiveKey="1">
                        <TabPane tab="镜像描述" key="1">
                            <h2>镜像描述</h2>
                            <p>{description || '该镜像没有任何描述信息...'}</p>
                        </TabPane>
                        <TabPane tab="镜像版本" key="2">
                            <Space size='middle' className="content-input">
                                <Search
                                    name={searchMirrorName}
                                    search={() => {
                                        if (!searchMirrorName) {
                                            store.showImageTags = [...imageTags]
                                            return
                                        }
                                        let newImageTags = new Array<MirrorTags>()
                                        for (let i = 0;i < imageTags.length; i++) {
                                            let val = imageTags[i]
                                            if (val.tag_name.includes(searchMirrorName)) {
                                                newImageTags.push(val)
                                            }
                                        }
                                        store.showImageTags = newImageTags as Array<MirrorTags>
                                    }}
                                    setName={(val) => {
                                        console.log(val)
                                        store.searchMirrorName = val
                                    }}
                                />
                            </Space>
                            {/* 列表 */}

                            <Spin delay={300} tip='加载中...' spinning={false}>

                                <Table columns={[
                                    {
                                        title: '镜像版本',
                                        dataIndex: 'tag_name',
                                        key: 'tag_name',
                                        fixed: 'left',
                                        align: 'center',

                                    }, {
                                        title: '大小',
                                        dataIndex: 'size',
                                        key: 'size',
                                        align: 'center',
                                    },
                                    {
                                        title: '更新时间',
                                        dataIndex: 'updated_at',
                                        key: 'updated_at',
                                        align: 'center',
                                    }
                                ]}
                                    dataSource={[...showImageTags]}
                                    rowKey={(record: any) => {
                                        console.log(record)
                                        return record.id
                                    }}
                                    pagination={{ pageSize: 10, defaultCurrent: 1, total: showImageTags.length }}
                                />
                            </Spin>



                            {/* 列表内容结束 */}

                        </TabPane>

                    </Tabs>
                </section>
                {/* 内容结束 */}
            </Container>

        )

    }

}


export default Index;
