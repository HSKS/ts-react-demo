export interface MirrorTags {
    id: number
    tag_name: string
    mirror_id: number
    user_id: number
    state: number
    size: string
    address: string
    created_at: string
    updated_at: string
}
export interface MirrorInfo {
    name: string
    updated_at: string
    download_count: number
    collect_count: number
    description: string
    mirror_tags: Array<MirrorTags>
}
