import {action, observable} from "mobx"
import {MirrorInfo,MirrorTags} from './interfaces'
import request from '../../utils/request'
import apis from '../../config/apis'
class Store {
    @observable mirror_name: string = ''
    @observable description: string = ''
    @observable download_count: number = 0
    @observable updated_at: string = ''
    @observable collect_count: number = 0
    @observable size: string = ''
    @observable depots: string = ''
    @observable imageTags = new Array<MirrorTags>()
    @observable showImageTags = new Array<MirrorTags>()
    @observable searchMirrorName: string | undefined = undefined
}

export default new Store()