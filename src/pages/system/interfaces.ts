export interface TableColumns {
    id: number
    name_cn: string
    name_en: string
    cluster_label: string
    group_id: number
    cpu:number
    mem:number
    gpu: number
    disk:number
    pod_number:number
    svc_number:number
    is_deleted:number
    created_at:string
}

export interface Domain {
    domain_name: string
    id: number
    is_inner: number
    parent_id: number
    Child: Domain[] 
    
}