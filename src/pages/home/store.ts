import {observable, action} from "mobx"

class Store {
    @observable count: number = 0
    @action setCount = (ct: number): void => {
        this.count = ct
    }
}

export default new Store()