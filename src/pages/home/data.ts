let arr:Array<{
  date: string
  value: number
  category: string
}> = []

for (let i = 1;i <= 30;i++) {
  arr.push({
    date: `2020-01-0${i}`,
    value: i*2.5+parseInt(`${Math.random()*30}`),
    category: "CPU"
  })
  arr.push({
    date: `2020-01-0${i}`,
    value: i*2.5+parseInt(`${Math.random()*30}`),
    category: "MEM"
  })
  arr.push({
    date: `2020-01-0${i}`,
    value: i*2.5+parseInt(`${Math.random()*30}`),
    category: "DISK"
  })
}
export default arr