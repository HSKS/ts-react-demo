import React, {Component, useEffect, useState} from "react"
import {observer} from "mobx-react"
import store from "./store"
import "./styles.less"
import {Line, Gauge} from '@ant-design/charts';
import data from './data'

interface IChartLine {
    data: Array<{
        date: string
        value: number
        category: string
    }>
}

const ChartLine: React.FC<IChartLine> = (props: IChartLine) => {
    const [data, setData] = useState(props.data);
    const config = {
        title: {
            visible: true,
            text: '资源利用率',
        },
        padding: 'auto',
        forceFit: true,
        data,
        xField: 'date',
        yField: 'value',
        seriesField: 'category',
        xAxis: {type: 'time'},
        yAxis: {label: {formatter: (v: any) => `${v}%`.replace(/\d{1,3}(?=(\d{3})+$)/g, (s) => `${s},`)}},
        responsive: true,
        smooth: true
    };
    // @ts-ignore
    return <Line {...config} />;
};

interface IChartGauge {
    title: string
    value: number
    size?: number
}
const ChartGauge: React.FC<IChartGauge> = (props: IChartGauge) => {
    const config = {
        title: {
            visible: true,
            text: props.title,
        },
        width: props.size || 300,
        height: props.size || 300,
        value: props.value || 0,
        min: 0,
        max: 100,
        range: [0, 25, 50, 75, 100],
        color: ['#39B8FF', '#52619B', '#43E089', '#C0EDF3'],
        statistic: {
            visible: true,
            text: props.value < 90 ? '充足': '即将用尽',
            color: '#30bf78',
        },
        format: (v: any) => {
            return v + '%';
        },
    }
    // @ts-ignore
    return <Gauge {...config} />;
};

@observer
class Index extends Component {

    componentDidMount(): void {

    }

    render() {
        // @ts-ignore
        return (
            <div className='home'>
                <div className='top'>
                    <div className='left'>
                        <div className='item' style={{background: '#1890ff'}}>
                            <span className='num'>7</span>
                            <span className='title'>应用数量</span>
                        </div>
                        <div className='item' style={{background: 'deeppink'}}>
                            <span className='num'>143</span>
                            <span className='title'>容器数量</span>
                        </div>
                        <div className='item' style={{background: '#A5A5A6'}}>
                            <span className='num'>45</span>
                            <span className='title'>节点数量</span>
                        </div>
                        <div className='item' style={{background: 'darkgoldenrod'}}>
                            <span className='num'>12</span>
                            <span className='title'>系统数量</span>
                        </div>
                    </div>
                    <div className='right'>
                        <ChartLine data={data}/>
                    </div>
                </div>
                <div className='bottom'>
                    <ChartGauge title="CPU资源使用情况" value={57}/>
                    <ChartGauge title="MEM资源使用情况" value={78}/>
                    <ChartGauge title="DISK资源使用情况" value={94}/>
                    <ChartGauge title="GPU资源使用情况" value={24}/>
                </div>
            </div>
        )
    }
}

export default Index