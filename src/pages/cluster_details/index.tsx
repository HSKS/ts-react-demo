import React, { Component } from "react"
import { observer } from "mobx-react"
import store from "./store"
import routes from "../../config/routes";
import { Link, RouteComponentProps, useLocation} from "react-router-dom"
import { Gauge } from '@ant-design/charts';
import { Tag, Tabs, Descriptions, Table, Space, Button, Spin, PageHeader } from 'antd'
import { PlusOutlined, RedoOutlined } from '@ant-design/icons';
import {
    CheckCircleOutlined,
    SyncOutlined,
    CloseCircleOutlined,
    ExclamationCircleOutlined
} from '@ant-design/icons';
import "./styles.less"
import formatDate from "../../utils/formatDate";
const { TabPane } = Tabs;
// const data: Array<object> = [];
// const columns = [
//     {
//         title: '服务类型',
//         dataIndex: 'type',
//         key: 'type',
//         // render: (text: String) => <Link to={routes.cluster_details_node + '/1'} >{text}</Link>
//     },
//     {
//         title: '标签',
//         dataIndex: 'tag',
//         key: 'tag',
//     },
//     {
//         title: '节点IP',
//         dataIndex: 'ip',
//         key: 'ip',
//     },
//     {
//         title: '端口',
//         key: 'port',
//         dataIndex: 'port'
//     },
//     // {
//     //     title: '操作',
//     //     key: 'action',
//     //     render: (text: string, record: any) => (
//     //         <Space size="middle">
//     //             <a>删除</a>
//     //         </Space>
//     //     ),
//     // },
// ];


// 扇形仪盘表
interface IChartGauge {
    title: string
    value: number
    size?: number
}
const ChartGauge: React.FC<IChartGauge> = (props: IChartGauge) => {
    const config = {
        title: {
            visible: true,
            text: props.title,
            alignTo: 'middle' // 仪表盘标题居中
        },
        value: props.value,
        min: 0,
        max: 100,
        range: [0, props.value],
        center: ['50%', '60%'],
        format: (v: any) => {
            return v + '%';
        },
        color: ['l(0) 0:#b0d0ff 1:#5f92f9'],
        statistic: {
            visible: true,
            text: props.value + '%',
            color: '#30bf78',
        },
    };
    // @ts-ignore
    return <Gauge {...config} />;
};

// interface ClusterInfo {
//     code: number
//     status: string
//     data: {
//         version: string
//         ready_node_num: string
//         cpu_percent: string
//         mem_percent: string
//         components: Array<ClusterComponent>
//     }
// }

// interface ClusterComponent {
//     name: string
//     status: string
// }

interface ClusterParams {
    cluster_label: string
    cluster_name: string
    created_at: string
}

@observer
class Index extends Component<RouteComponentProps> {
    // state = {
    //     clusterInfo: {
    //         code: 0,
    //         status: '',
    //         data: {
    //             version: '',
    //             ready_node_num: '',
    //             cpu_percent: '',
    //             mem_percent: '',
    //             components: [
    //                 {
    //                     name: '',
    //                     status: ''
    //                 }
    //             ]
    //         }
    //     }
    // }

    componentDidMount(): void {
        const { setTableScrollHeight } = store
        // let cluster = (this.props.location && this.props.location.params) ? this.props.location.params.cluster : {}
        setTableScrollHeight(document.body.clientHeight - 327)
        // console.log('clusterParams_props: ', this.props)
        let cluster = this.props.match.params as ClusterParams
        // console.log('clusterParams：', clusterParams)
        store.getClusterInfoDetail(cluster.cluster_label)
        // store.getClusterInfoDetail("test2")
        // this.init()
    }

    // init = async () => {
    //     let res = await store.getClusterInfoDetail()
    //     this.setState({ clusterInfo: res })
    // }

    render() {

        let { ClusterInfoDetail } = store
        let data = { ...ClusterInfoDetail }
        // console.log('clusterInfoDetail', data)

        // let clusterInfoDetail = { ...ClusterInfoDetail }
        // let { code, status, data } = clusterInfoDetail
        // let { code, status, data } = this.state.clusterInfo
        // console.log('this.state.clusterInfo', this.state.clusterInfo)

        // let nodeList: Array<object> = [];
        // for (let i = 1; i < 50; i++) {
        //     nodeList.push({
        //         type: 'k8s_slave',
        //         tag: 'dcos:gpu' + i,
        //         ip: '188.104.2.149',
        //         port: '10250',
        //     });
        // }

        // console.log('props：', this.props)
        // console.log('传进来的参数在哪儿？')
        // let location = useLocation()
        // console.log('useLocation：', location)

        let nodeList = data.node_list

        const columns = [
            {
                title: '节点名称',
                dataIndex: 'name',
                key: 'name',
                // render: (text: String) => <Link to={routes.cluster_details_node + '/1'} >{text}</Link>
            },
            // {
            //     title: '服务类型',
            //     dataIndex: 'type',
            //     key: 'type',
            //     // render: (text: String) => <Link to={routes.cluster_details_node + '/1'} >{text}</Link>
            // },
            // {
            //     title: '标签',
            //     dataIndex: 'tag',
            //     key: 'tag',
            // },
            {
                title: '节点IP',
                dataIndex: 'ip',
                key: 'ip',
            },
            {
                title: 'Pod数量',
                dataIndex: 'pod_num',
                key: 'pod_num',
            },
            {
                title: '节点状态',
                dataIndex: 'status',
                key: 'status',
            },
            // {
            //     title: '端口',
            //     key: 'port',
            //     dataIndex: 'port'
            // },
            // {
            //     title: '操作',
            //     key: 'action',
            //     render: (text: string, record: any) => (
            //         <Space size="middle">
            //             <a>删除</a>
            //         </Space>
            //     ),
            // },
        ];

        // let cpu_percent = parseFloat(data.cpu_percent).toFixed(2)
        // let mem_percent = parseFloat(data.mem_percent).toFixed(2)
        let cpu_percent = Math.round(parseFloat(data.cpu_percent) * 100) / 100
        let mem_percent = Math.round(parseFloat(data.mem_percent) * 100) / 100

        // let { cluster } = this.props.location.params
        // let cluster = {
        //     cluster_name: '。。。',
        //     created_at: ''
        // }
        let cluster = this.props.match.params as ClusterParams

        return (
            <div className='cluster-details'>
                <div>
                    {/* 标题头开始 */}
                    <PageHeader className="site-page-header content-title" title={cluster.cluster_name} />
                    {/* 标题头结束 */}

                    <Tabs className="cluster content-cont" defaultActiveKey="1">
                        {/* 集群开始 */}
                        <TabPane tab="集群配置" key="1">
    
                            {/* 顶部基本信息 */}
                            <div className="top">
                                <Descriptions title="集群基本信息" size="small" bordered>
                                    <Descriptions.Item label="Kubernetes版本" className="desc-item" >{data.version}</Descriptions.Item>
                                    <Descriptions.Item label="主机数" className="desc-item">{data.ready_node_num}</Descriptions.Item>
                                    {/*<Descriptions.Item label="集群名称">1</Descriptions.Item>
                                    <Descriptions.Item label="创建人">admin</Descriptions.Item>*/}
                                    <Descriptions.Item label="创建时间" className="desc-item"
                                        // span={2}
                                    >{formatDate(new Date(cluster.created_at))}</Descriptions.Item>
                                </Descriptions>
                            </div>
    
                            {/* 资源统计指针 */}
                            <div className="statistic">
                                <ChartGauge title="CPU资源使用情况" value={cpu_percent} />
                                <ChartGauge title="MEM资源使用情况" value={mem_percent} />
                                {/*<ChartGauge title="GPU资源使用情况" value={parseFloat(data.)} />*/}
                            </div>
    
                            {/* 状态标签 */}
                            <Descriptions title="组件运行状态" size="small" />
                            <div className="bottom">
                                {
                                    data.components.map((item: any) => {
                                        switch (item.status) {
                                            case 'Running':
                                                return (
                                                    <Tag icon={<CheckCircleOutlined />} color="success">
                                                        {item.name}
                                                    </Tag>
                                                )
                                            default:
                                                return (
                                                    <Tag icon={<CheckCircleOutlined />} color="warning">
                                                        {item.name}
                                                    </Tag>
                                                )
                                        }
                                    })
                                }
                                {/*<Tag icon={<CheckCircleOutlined />} color="success">
                                    Etcd
                                </Tag>
                                <Tag icon={<SyncOutlined spin />} color="processing">
                                    Controller Manager
                                </Tag>
                                <Tag icon={<CloseCircleOutlined />} color="error">
                                    Scheduler
                                </Tag>
                                <Tag icon={<ExclamationCircleOutlined />} color="warning">
                                    Nodes
                                </Tag>*/}
                            </div>
    
                        </TabPane>
                        {/* 集群结束 */}
                        {/* 节点开始 */}
                        <TabPane className="nodes" tab="节点列表" key="2">
    
                            {/* 按钮栏 */}
                            
                            <Space className="cluster-btn">
                                <Button type="primary" size={'large'} icon={<PlusOutlined />}>添加节点</Button>
                                <Button type="primary" size={'large'} icon={<RedoOutlined />}>更新节点</Button>
                            </Space>
                            
    
                            {/* 列表 */}
                            <Table
                                columns={columns}
                                dataSource={nodeList}
                                // scroll={{ x: 1406, y: store.tableScrollHeight }}
                            />
    
                        </TabPane>
                        {/* 节点结束 */}
                    </Tabs>
                </div>
            </div>
        )
    }
}

export default Index