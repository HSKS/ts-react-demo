import { observable, action } from "mobx"
import request from '../../utils/request'
import apis from '../../config/apis'

class Store {
    @observable selectStatus: any = 0
    @action setSelectStatus = (arr: Array<number>): void => {
        this.selectStatus = arr
    }
    @observable tableScrollHeight: number = 300
    @action setTableScrollHeight = (height: number): void => {
        this.tableScrollHeight = height
    }

    @observable ClusterInfoDetail: any = {
        version: '',
        ready_node_num: '',
        cpu_percent: '',
        mem_percent: '',
        components: [
            {
                name: '',
                status: ''
            }
        ]
    }
    @action getClusterInfoDetail = async (cluster_label: any) => {
        let params = {
            cluster_label: cluster_label
        }
        const resp = await request(apis.GetClusterInfoDetail, params)
        // console.log('resp', resp)
        if (resp) {
            this.ClusterInfoDetail = resp
        }
        return resp
    }
}

export default new Store()