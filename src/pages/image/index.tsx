import React, { Component } from "react"

import { observer } from "mobx-react"

import { Button, Input, PageHeader, Space, Spin, Table, Modal } from 'antd'
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';
import { PlusOutlined } from '@ant-design/icons';
import routes from "../../config/routes";
import "./styles.less"
import "./styles.css"
import store from "./store";
import formatDate from '../../utils/formatDate'
import Container from '../../components/Container'
import { TableColumns } from './interfaces'
import CButton from "../../components/CButton";
import MirrorCreate from "../mirror_create"
//搜索
interface Search {
    mirrorName: string
    setMirrorName: (val: string) => void
    hubName: string
    setHubName: (val: string) => void
    onClick: () => void
    onClear: () => void
}


const Search: React.FC<Search> = ({ onClick, mirrorName, setMirrorName, hubName, setHubName, onClear }) => {
    return (
        <Space>
            <Input
                className='app-exp-input input'
                addonBefore='镜像名称：'
                placeholder='请输入'
                value={mirrorName}
                onChange={(e) => {
                    setMirrorName(e.target.value)
                }}
            />

            <Input
                className='app-exp-input input'
                addonBefore='镜像仓库：'
                placeholder='请输入'
                value={hubName}
                onChange={(e) => {
                    setHubName(e.target.value)
                }}

            />

            <CButton className='app-but' type='primary' text='查询' onClick={() => onClick()} />
            <CButton className='app-reset' text='重置' onClick={() => onClear()} />

        </Space>


    )

}


@observer
class Index extends Component<RouteComponentProps, {}> {
    componentDidMount(): void {
        store.getDepots()
    }

    showModal = () => {

        store.visible= false

    };

    handleOk = ()=> {
 
            store.visible= false

    };

    handleCancel = () => {

            store.visible= false

    };


    // tableChange = (e: any) => {
    //     store.page_num = e.current
    //     store.getDepots()
    // }

    render() {
        const { depots, page_num, page_size, total, mirror_name, hub_name ,visible} = store
        return (
            <Container title='自有镜像' className='image-page'>
                <section className="content-cont">
                    <Button type='primary' style={{ marginBottom: 20 }} icon={<PlusOutlined />} onClick={() => {
                        // this.showModal()
                        this.props.history.push(routes.mirror_create)
                    }}>创建镜像</Button>
                    {/* <Modal
                        title="创建镜像"
                        visible={visible}
                        onOk={this.handleOk}
                        onCancel={this.handleCancel}
                        footer={null}
                        width={700}
                        okText='确认'
                        cancelText='取消'
                    >
                         <MirrorCreate />
                    </Modal> */}
                    <Space size='middle' className="content-input">
                        <Search
                            mirrorName={mirror_name}
                            hubName={hub_name}
                            onClick={() => {
                                // store.page_num = 1
                                store.getDepots()
                            }}
                            onClear={() => {
                                store.hub_name = ''
                                store.mirror_name = ''
                            }}
                            setMirrorName={(val) => {
                                console.log(val)
                                store.mirror_name = val
                            }}
                            setHubName={(val) => {
                                store.hub_name = val

                            }} />

                    </Space>
                    {/* 列表 */}

                    <Spin delay={300} tip='加载中...' spinning={false}>

                        <Table columns={[
                            {
                                title: '镜像名称',
                                width: 150,
                                dataIndex: 'name',
                                fixed: 'left',
                                align: 'center',
                                render: (text: any, rcd: TableColumns) => (<a onClick={() => {
                                    let rcdStr = JSON.stringify(rcd)
                                    localStorage.setItem('image_details', rcdStr)
                                    this.props.history.push(routes.image_details)
                                }}>{text}</a>)
                            },
                            {
                                title: '镜像仓库',
                                dataIndex: 'mirror_hub',
                                align: 'center',
                                render: (text, rcd) => rcd.mirror_hub && rcd.mirror_hub.name
                            }, {
                                title: '仓库类型',
                                dataIndex: 'is_private',
                                align: 'center',
                                render: (text) => (text = "1" ? <span>私有</span> : <span>公开</span>)
                            },
                            {
                                title: '版本数',
                                dataIndex: 'version_count',
                                align: 'center',
                            },
                            {
                                title: '创建用户',
                                dataIndex: 'user',
                                align: 'center',
                                render: (text) => (<span>{text.username}</span>)
                            },
                            {
                                title: '更新时间',
                                dataIndex: 'updated_at',
                                align: 'center',
                                render: (date) => formatDate(new Date(date))
                            }
                        ]}
                            dataSource={[...depots]}

                            rowKey={(record: any) => record.id}

                            pagination={{ pageSize: 10, defaultCurrent: 1, total: [...depots].length }}

                        //    pagination={{

                        //        current: page_num, // 当前页数

                        //        pageSize: page_size,

                        //        total: total,

                        //    }}

                        //    onChange={this.tableChange}

                        />

                    </Spin>

                </section>

                {/* 列表内容结束 */}

            </Container>


        )


    }


}


export default withRouter(Index as any);