import {observable, action} from "mobx"
import {TableColumns} from './interfaces'
import request from '../../utils/request'
import apis from '../../config/apis'

class Store {
    @observable depots: Array<TableColumns> = []
    @observable mirror_name: string = ''
    @observable hub_name: string = ''
    @observable page_num = 1
    @observable page_size = 50
    @observable total = 0
    @observable visible :boolean=false
    @action getDepots = async () => {
        const api = apis.GetDepotList
        const params = {
            mirror_name: this.mirror_name,
            hub_name: this.hub_name,
            page_num: this.page_num,
            page_size: this.page_size,
            mirror_type:2,
        }
        const data = await request(api, params)
        if (!data) return
        this.total = data.length
        this.depots = data as Array<TableColumns>
    }
}

export default new Store()