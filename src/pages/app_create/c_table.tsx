import React, { FC } from "react"
import { Table } from 'antd'
import { ColumnsType, TablePaginationConfig } from "antd/lib/table";
import './c_table.less'
import { PlusCircleOutlined } from "@ant-design/icons";
import ContainerRow from '../../components/ContainerRow'

interface ITable {
    title?: string | undefined
    titleWidth?: number | undefined
    columns: ColumnsType<any> | undefined
    dataSource: any[] | undefined
    pagination?: TablePaginationConfig | false | undefined
    add?: () => void
    showAdd?: boolean | undefined
    hideTitle?: boolean | undefined
}

const CTable: FC<ITable> = ({ title, titleWidth, hideTitle, add, showAdd, columns, dataSource, pagination }) => {
    if (!titleWidth) titleWidth = 120
    const config = {
        columns,
        dataSource,
        pagination,
        bordered: true
    }

    const addList = () => (
        showAdd ? (<PlusCircleOutlined className='add-icon' onClick={() => {
            add && add()
        }} />) : null)

    return hideTitle ? (
        <div className='ctable-without-title c-table'>
            <Table {...config} /> {addList()}
        </div>
    ) : (
            <ContainerRow title={title} titleWidth={titleWidth} className='c-table'>
                <Table {...config} /> {addList()}
            </ContainerRow>
        )
}


export default CTable;