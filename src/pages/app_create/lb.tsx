import React, {Component} from "react"
import {observer} from "mobx-react"
import {InputNumber, Select} from 'antd'
import store from './store'
import "./height_options.less"
import CTable from "../../components/CTable";

@observer
class Index extends Component {

    render() {
        const {lbArr} = store
        return (
            <CTable columns={[{
                title: '服务端口',
                key: 'container_port',
                dataIndex: 'container_port',
                align: 'center',
                render: (val, {container_port, edit}, idx) => edit ? (
                    <InputNumber defaultValue={val} onChange={(v) => {
                        if (typeof v === 'number') store.lbArr[idx].container_port = v
                    }}/>
                ) : val
            }, {
                title: '协议',
                key: 'protocol',
                dataIndex: 'protocol',
                align: 'center',
                render: (val, {protocol, edit}, idx) => edit ? (
                    <Select placeholder='请选择' style={{minWidth: 140}} defaultValue={val} onChange={(v: 'TCP' | 'UDP') => {
                        store.lbArr[idx].protocol = v
                    }}>
                        <Select.Option value='TCP'>TCP</Select.Option>
                        <Select.Option value='UDP'>UDP</Select.Option>
                    </Select>
                ) : val
            }, {
                title: '操作',
                key: 'action',
                dataIndex: 'action',
                align: 'center',
                render: (val, {edit}, idx) => edit ? (
                    <a onClick={() => {
                        store.lbArr[idx].edit = false
                        store.lbArr = [...store.lbArr]
                    }}>确定</a>
                ) : (<div>
                    <a onClick={() => {
                        store.lbArr[idx].edit = true
                        store.lbArr = [...store.lbArr]
                    }}>编辑</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a onClick={() => {
                        store.lbArr.splice(idx, 1)
                        store.lbArr = [...store.lbArr]
                    }}>删除</a>
                </div>) 
            }]} dataSource={[...lbArr]} add={() => {
                store.lbArr.push({
                    edit: true,
                    container_port: 80,
                    protocol: 'TCP'
                })
            }} hideTitle={true} showAdd={lbArr.length === 0} pagination={false}/>
        )
    }
}

export default Index;