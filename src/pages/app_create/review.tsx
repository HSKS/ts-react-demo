import React, { Component } from 'react'
import { observer } from 'mobx-react'
import { Descriptions, Collapse, Table, InputNumber, Select, Input, message } from 'antd';
import store from './store'
import './review.less'
import ContainerRow from '../../components/ContainerRow'
import CButton from "../../components/CButton";
const { Panel } = Collapse;
@observer
class Index extends Component {
    render() {
        const { namespace_cn, app_name, app_id, describe, addr, image, nodeType, clusters_cn, resourceQuota, lbArr, host_path_volumes, command, restart_policy } = store
        //'Always' | 'OnFailure' | 'Never'
        let restartPolicy = '总是重启'
        if (restart_policy === 'OnFailure') {
            restartPolicy = '错误重启'
        } else {
            restartPolicy = '错误重启'
        }
        return (
            <div className='create-specification'>
                {/* 应用配置开始 */}
                <section className='content'>
                    <Descriptions title="应用配置:" column={4}>
                        <Descriptions.Item label="业务系统">{namespace_cn}</Descriptions.Item>
                        <Descriptions.Item label="应用名称">{app_name}</Descriptions.Item>
                        <Descriptions.Item label="APPID">{app_id}</Descriptions.Item>
                        <Descriptions.Item label="应用描述">{describe}</Descriptions.Item>
                    </Descriptions>
                </section>
                {/* 应用配置结束 */}

                {/* 容器配置开始 */}
                <section className='content'>
                    <Descriptions title="容器配置:" column={4} style={{ marginBottom: 20 }}>
                        <Descriptions.Item label="部署区域">{addr}</Descriptions.Item>
                        <Descriptions.Item label="镜像">{image}</Descriptions.Item>
                    </Descriptions>
                    <ContainerRow title='集群' style={{ marginBottom: 20 }}>
                        <Table
                            bordered={true}
                            pagination={false}
                            columns={[
                                {
                                    title: '集群名称',
                                    dataIndex: 'cluster_name',
                                    key: 'cluster_name',
                                    align: 'center',
                                },
                                {
                                    title: '实例数',
                                    dataIndex: 'replicas',
                                    key: 'replicas',
                                    align: 'center',
                                }
                            ]} dataSource={[...clusters_cn]} />
                    </ContainerRow>
                    <ContainerRow title='容器规格' style={{ marginBottom: 20 }}>
                        <Table
                            bordered={true}
                            pagination={false}
                            columns={[
                                {
                                    title: 'CPU',
                                    dataIndex: 'cpu',
                                    key: 'cpu',
                                    align: 'center',
                                },
                                {
                                    title: '内存',
                                    dataIndex: 'mem',
                                    key: 'mem',
                                    align: 'center',
                                },
                                {
                                    title: 'GPU',
                                    dataIndex: 'gpu',
                                    key: 'gpu',
                                    align: 'center',
                                    render: text => text || '~'
                                },
                                {
                                    title: 'GPU名称',
                                    dataIndex: 'gpu_name',
                                    key: 'gpu',
                                    align: 'center',
                                    render: text => text || '~'
                                },
                            ]} dataSource={[{
                                key: '1',
                                cpu: resourceQuota.cpu,
                                mem: resourceQuota.mem,
                                gpu: resourceQuota.gpu_num,
                                gpu_name: resourceQuota.gpu_name
                            }]} />
                    </ContainerRow>
                    {/* 高级配置 */}
                    <div className='advanced'>
                        <strong>高级配置：</strong>
                        <Collapse defaultActiveKey={['1']} onChange={() => {

                        }}>
                            {/* 负载均衡 */}
                            <Panel header="负载均衡" key="1">
                                <Table
                                    bordered={true}
                                    pagination={false}
                                    columns={[{
                                        title: '服务端口',
                                        key: 'container_port',
                                        dataIndex: 'container_port',
                                        align: 'center',
                                    }, {
                                        title: '协议',
                                        key: 'protocol',
                                        dataIndex: 'protocol',
                                        align: 'center',
                                    }]} dataSource={[...lbArr]} />
                            </Panel>

                            {/* 数据存储 */}
                            <Panel header="目录映射" key="2">
                                <Table
                                    bordered={true}
                                    pagination={false}
                                    columns={[{
                                        title: '主机路径',
                                        key: 'from',
                                        dataIndex: 'from',
                                        align: 'center',
                                        render: (val) => val
                                    }, {
                                        title: '容器映射路径',
                                        key: 'to',
                                        dataIndex: 'to',
                                        align: 'center',
                                        render: (val) => val
                                    }, {
                                        title: '读写权限',
                                        key: 'read_only',
                                        dataIndex: 'read_only',
                                        align: 'center',
                                        render: (val) => val ? '只读' : '可读可写'
                                    }]} dataSource={host_path_volumes} />
                            </Panel>

                            {/* 健康检查 */}
                            <Panel header="健康检查" key="4">
                                <p>暂无</p>
                            </Panel>

                            {/* 重启策略 */}
                            <Panel header="重启策略" key="5">
                                <p>{restartPolicy}</p>
                            </Panel>

                            {/* 启动命令 */}
                            <Panel header="启动命令" key="6">
                                <p>{command?.join(' ')}</p>
                            </Panel>
                        </Collapse>
                    </div>
                </section>
                <div className='option'>
                    <CButton text='上一步' style={{ marginRight: 30 }} onClick={() => store.current = 1} />
                    <CButton text='开始创建' type='primary' onClick={async () => {
                        store.setLoading()//转圈圈
                        const ok = await store.submit()
                        if (ok) store.current = 3;
                        return;
                    }} />
                </div>
            </div>
        )
    }
}


export default Index
