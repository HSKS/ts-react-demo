import React, { Component } from "react"
import { observer } from "mobx-react"
import { Collapse, Radio, Input } from 'antd'
import store from './store'
import "./height_options.less"
import ContainerRow from '../../components/ContainerRow'
import LB from './lb'
import CCheck from './c_check'
import HostPathVolume from './host_path_volume'
import ENV from './env'

const { Panel } = Collapse;

@observer
class Index extends Component {

    render() {
        const { restart_policy, command } = store
        return (
            <ContainerRow title='高级配置' className='height-options' inputWidth="1263px">
                <Collapse defaultActiveKey={['1']} style={{ 'width': '100%' }}>
                    <Panel header="负载均衡" key="1">
                            <LB {...this.props} />
                    </Panel>
                    <Panel header="目录映射" key="3">
                            <HostPathVolume {...this.props} />
                    </Panel>
                    <Panel header="环境变量" key="4">
                            <ENV {...this.props} />
                    </Panel>
                    <Panel header="健康检查" key="5">
                            <CCheck />
                    </Panel>
                    <Panel header="重启策略" key="6">
                        <Radio.Group value={restart_policy} onChange={(e) => {
                            store.restart_policy = e.target.value
                        }}>
                            <Radio value='Always'>总是重启</Radio>
                            <Radio value='OnFailure'>错误重启</Radio>
                            <Radio value='Never'>不重启</Radio>
                        </Radio.Group>
                    </Panel>
                    <Panel header="启动命令" key="7">
                        <Input placeholder='请输入启动命令' value={command && command.length ? command.join(' ') : undefined}
                            onChange={(e) => {
                                const v = e.target.value
                                store.command = v.split(' ')
                            }} />
                    </Panel>
                </Collapse>
            </ContainerRow>
        )
    }
}

export default Index;