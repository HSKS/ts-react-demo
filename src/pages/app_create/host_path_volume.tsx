import React, {Component} from "react"
import {observer} from "mobx-react"
import {Input, Select} from 'antd'
import store from './store'
import "./height_options.less"
import CTable from "../../components/CTable";

@observer
class Index extends Component {

    render() {
        const {host_path_volumes} = store
        return (
            <CTable columns={[{
                title: '主机路径',
                key: 'from',
                dataIndex: 'from',
                align: 'center',
                render: (val, {from, edit}, idx) => edit ? (
                    <Input defaultValue={val} onChange={(e) => {
                        let v = e.target.value
                        store.host_path_volumes[idx].from = v
                    }}/>
                ) : val
            }, {
                title: '容器映射路径',
                key: 'to',
                dataIndex: 'to',
                align: 'center',
                render: (val, {to, edit}, idx) => edit ? (
                    <Input defaultValue={val} onChange={(e) => {
                        let v = e.target.value
                        store.host_path_volumes[idx].to = v
                    }}/>
                ) : val
            }, {
                title: '读写权限',
                key: 'read_only',
                dataIndex: 'read_only',
                align: 'center',
                render: (val, {read_only, edit}, idx) => edit ? (
                    <Select placeholder='请选择' style={{minWidth: 140}} defaultValue={val ? 1 : 0}
                            onChange={(v: number) => {
                                if (v) {
                                    store.host_path_volumes[idx].read_only = true
                                } else {
                                    store.host_path_volumes[idx].read_only = false
                                }
                            }}>
                        <Select.Option value={0}>可读可写</Select.Option>
                        <Select.Option value={1}>只读</Select.Option>
                    </Select>
                ) : val ? '只读' : '可读可写'
            }, {
                title: '操作',
                key: 'action',
                dataIndex: 'action',
                align: 'center',
                render: (val, {edit}, idx) => edit ? (
                    <a onClick={() => {
                        store.host_path_volumes[idx].edit = false
                        store.host_path_volumes = [...store.host_path_volumes]
                    }}>确定</a>
                ) : (<div>
                    <a onClick={() => {
                        store.host_path_volumes[idx].edit = true
                        store.host_path_volumes = [...store.host_path_volumes]
                    }}>编辑</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a onClick={() => {
                        store.host_path_volumes.splice(idx, 1)
                        store.host_path_volumes = [...store.host_path_volumes]
                    }}>删除</a>
                </div>)
            }]} dataSource={[...host_path_volumes]} add={() => {
                store.host_path_volumes.push({
                    edit: true,
                    from: undefined,
                    to: undefined,
                    read_only: false
                })
            }} hideTitle={true} showAdd={true} pagination={false}/>
        )
    }
}

export default Index;