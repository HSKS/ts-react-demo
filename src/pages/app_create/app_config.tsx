import React, { Component } from "react"
import { observer } from "mobx-react"
import { message } from 'antd'
import store from './store'
import './app_config.less'
import StringInput from "../../components/StringInput";
import TextAreaInput from "../../components/TextAreaInput";
import CButton from "../../components/CButton";
import OneSelect from "../../components/OneSelect";

@observer
class Index extends Component {
    checkParams = () => {
        const { namespace, app_name, app_id } = store
        let tip = ''
        if (!namespace) tip = '请选择业务系统'
        if (!app_name) tip = '请输入应用名称'
        if (!app_id) tip = '请正确输入APPID'
        if (!/^[qwertyuiopasdfghjklzxcvbnm][qwertyuiopasdfghjklzxcvbnm1234567890-]+$/.test(app_id as string)) tip = '请正确输入APPID，字母开头，包含数字，字母，中横线-。'
        if (tip) {
            message.warning(tip)
            return false
        }
        return true
    }

    render() {
        const { namespaces, namespace, app_name, app_id, node_affinity_labels, describe } = store
        return (
            <div className='app-config'>
                <OneSelect title='业务系统' data={namespaces} value={namespace}
                    required={namespace !== ''}
                    onChange={(val, label) => {
                        store.namespace = val
                        store.namespace_cn = label
                    }} />
                <StringInput title='应用名称' placeholder='请输入应用名称' value={app_name}
                    required={app_name !== ''}
                    onChange={(val) => store.app_name = val} />
                <StringInput title='APPID' placeholder='请输入APPID' value={app_id}
                    required={app_id !== ''}
                    onChange={(val) => store.app_id = val} />
                <TextAreaInput title='应用描述' placeholder='请输入应用描述' value={describe} inputWidth="900px"
                    onChange={(val) => store.describe = val} />
                <div className='option'>
                    <CButton text='下一步' type='primary' onClick={() => {

                        if (this.checkParams()) {
                            store.current = 1
                        }
                    }} />
                </div>
            </div>
        )
    }
}

export default Index;