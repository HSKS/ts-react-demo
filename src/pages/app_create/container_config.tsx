import React, { Component } from "react"
import { observer } from "mobx-react"
import { ColumnsType } from "antd/lib/table";
import { InputNumber, message, Select } from "antd";
import store from './store'
import './container_config.less'
import OneSelect from "../../components/OneSelect";
import CButton from "../../components/CButton";
import CTable from "../../components/CTable";
import CResourceQuota from './c_resource_quota'
import CImage from "./c_image";
import HeightOptions from './height_options'
import { ResourceModel } from './interfaces'


@observer
class Index extends Component {
    render() {
        const { areas, areaMap, addr, clusters, priMirrors, pubMirrors, clusters_cn, clusterArr, nodeTypes, nodeType, resourceQuotaIndex, mirror_id, mirror_tag_id, mirror_type } = store
        let mirrors = pubMirrors
        if (mirror_type === 2) {
            mirrors = priMirrors
        }
        return (
            <div className='container-config'>
                <CImage
                    title='镜像'
                    mirrorType={mirror_type}
                    selectMirrorType={(val) => {
                        store.mirror_type = val
                        store.mirror_id = 0
                        store.mirror_tag_id = 0
                    }}
                    required
                    data={mirrors}
                    value={mirror_id !== 0 && mirror_tag_id !== 0 ? [mirror_id, mirror_tag_id] : undefined}
                    onChange={(val, img) => {
                        store.mirror_id = val[0] as number
                        store.mirror_tag_id = val[1] as number
                        store.image = img
                    }} />
                {/*选择区域*/}
                <OneSelect title='区域' required data={areas} value={addr} onChange={(val) => {
                    store.addr = val
                    for (let i = 0; i < areas.length; i++) {
                        if (areas[i].value === val) {
                            store.clusterArr = areas[i].children
                            break
                        }
                    }
                }} />
                {/*选择集群*/}
                {addr && <CTable
                    showAdd={true}
                    title={'集群'}
                    columns={[{
                        title: '集群',
                        dataIndex: 'cluster_name',
                        key: 'cluster_name',
                        align: 'center',
                        render: (text, { cluster_name, edit }, idx) => edit ? (
                            <Select placeholder='请选择' style={{ minWidth: 140 }} defaultValue={cluster_name}
                                onChange={lb => {
                                    // @ts-ignore
                                    store.clusters_cn[idx].cluster_name = areaMap[addr][lb]
                                    store.clusters[idx].cluster_label = lb
                                }}>
                                {
                                    clusterArr.map(clu => {
                                        return <Select.Option key={`${clu.value}#${clu.label}`}
                                            value={clu.value}>{clu.label}</Select.Option>
                                    })
                                }
                            </Select>) : text
                    }, {
                        title: '实例数',
                        dataIndex: 'replicas',
                        key: 'replicas',
                        align: 'center',
                        render: (text, { replicas, edit }, idx) => edit ? (
                            <InputNumber placeholder='请输入实例数' min={1} max={65535} defaultValue={replicas} onChange={val => {
                                if (typeof val === 'number') {
                                    store.clusters_cn[idx].replicas = val
                                    store.clusters[idx].replicas = val
                                }
                            }
                            } />) : text
                    }, {
                        title: '操作',
                        dataIndex: 'action',
                        key: 'action',
                        align: 'center',
                        render: (text, { edit }, idx) => edit ? (<div>
                            <a onClick={async () => {
                                if (!store.clusters_cn[idx].cluster_name) {
                                    message.warning('请选择集群')
                                    return
                                }
                                if (store.clusters_cn[idx].replicas <= 0) {
                                    message.warning('请输入有效实例数')
                                    return
                                }
                                let mp: {
                                    [prop: string]: boolean
                                } = {}
                                let flag = false
                                for (let i = 0; i < clusters.length; i++) {
                                    let clu = clusters[i]
                                    if (mp[clu.cluster_label as string]) {
                                        flag = true
                                        break
                                    }
                                    mp[clu.cluster_label as string] = true
                                }
                                if (flag) {
                                    message.warning('集群重复，请重新选择集群')
                                    return
                                }
                                store.clusters_cn[idx].edit = false
                                store.clusters_cn = [...store.clusters_cn]
                                await store.getNodeTypes()
                                if (store.nodeTypes.length > 0) {
                                    store.nodeType = store.nodeTypes[0]
                                    store.resourceQuotaIndex = 0
                                    if (store.nodeType.pod_models && store.nodeType.pod_models.length) {
                                        store.resourceQuota = store.nodeType.pod_models[0]
                                    }
                                }
                            }}>确定</a>&nbsp;&nbsp;&nbsp;&nbsp;<a onClick={async () => {
                                store.clusters_cn.splice(idx, 1)
                                store.clusters.splice(idx, 1)
                                store.clusters_cn = [...store.clusters_cn]
                                await store.getNodeTypes()
                                if (store.nodeTypes.length > 0) {
                                    store.nodeType = store.nodeTypes[0]
                                    store.resourceQuotaIndex = 0
                                    if (store.nodeType.pod_models && store.nodeType.pod_models.length) {
                                        store.resourceQuota = store.nodeType.pod_models[0]
                                    }
                                }
                            }}>删除</a>
                        </div>) : (
                                <div>
                                    <a onClick={() => {
                                        store.clusters_cn[idx].edit = true
                                        store.clusters_cn = [...store.clusters_cn]
                                    }}>编辑</a>&nbsp;&nbsp;&nbsp;&nbsp;<a onClick={async () => {
                                        store.clusters_cn.splice(idx, 1)
                                        store.clusters.splice(idx, 1)
                                        store.clusters_cn = [...store.clusters_cn]
                                        await store.getNodeTypes()
                                        if (store.nodeTypes.length > 0) {
                                            store.nodeType = store.nodeTypes[0]
                                            store.resourceQuotaIndex = 0
                                            if (store.nodeType.pod_models && store.nodeType.pod_models.length) {
                                                store.resourceQuota = store.nodeType.pod_models[0]
                                            }
                                        }
                                    }}>删除</a>
                                </div>
                            )
                    }] as ColumnsType<any>} dataSource={[...clusters_cn]} pagination={false} add={() => {
                        store.clusters_cn.push({ cluster_name: undefined, replicas: 1, edit: true })
                        store.clusters.push({ cluster_label: undefined, replicas: 1 })
                    }} />}
                {/*容器规格*/}
                {nodeTypes && nodeTypes.length > 0 &&
                    <CResourceQuota title='容器规格' nodeTypes={nodeTypes} nodeType={nodeType} selectNodeType={(val) => {
                        store.nodeType = val
                        store.resourceQuotaIndex = 0
                        if (store.nodeType.pod_models && store.nodeType.pod_models.length) {
                            store.resourceQuota = store.nodeType.pod_models[0]
                        }
                    }} pagination={false} resourceQuotaIndex={resourceQuotaIndex} selectResourceQuota={(val, idx) => {
                        store.resourceQuotaIndex = idx
                        store.resourceQuota = val
                    }} />}
                {/*高级配置*/}
                <HeightOptions {...this.props} />
                <div className='option'>
                    <CButton text='上一步' style={{ marginRight: 30 }} onClick={() => store.current = 0} />
                    <CButton text='下一步' type='primary' onClick={async () => {
                        if (store.checkParams() && store.checkEdit()) {
                            store.current = 2
                        }
                    }} />
                </div>
            </div>
        )
    }
}

export default Index;