import React, {Component} from "react"
import {observer} from "mobx-react"
import {Steps} from 'antd'
import store from './store'
import {RouteComponentProps, withRouter} from 'react-router-dom';
import "./index.less"
import Loading from '../../components/Loading'
import Container from '../../components/Container'
import AppConfig from './app_config'
import ContainerConfig from './container_config'
import Review from './review'
import Finish from './finish'
import HeaderSteps from './header_steps'


@observer
class Index extends Component<RouteComponentProps, {}> {
    componentDidMount() {
        store.getNamespaces()
        store.getArea()
        store.GetMirrorSelectData()
    }

    componentWillUnmount() {
        store.init()
    }

    render() {
        const {current, loading} = store
        return (
            <Container title='应用创建' className='app-create'>
                {/* 头部步骤开始 */}
                <HeaderSteps current={current}/>

                {/* 内容开始 */}
                <div className="cont">
                    {current === 0 ? <AppConfig {...this.props} /> : null}
                    {current === 1 ? <ContainerConfig {...this.props} /> : null}
                    {current === 2 ? <Review {...this.props} /> : null}
                    {current === 3 ? <Finish {...this.props} /> : null}
                </div>

                {/* 加载中 */}
                <Loading show={loading}/>
            </Container>
        )
    }
}

export default withRouter(Index as any);