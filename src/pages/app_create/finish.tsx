import React, {Component} from "react"
import {observer} from "mobx-react"
import {Button, Space} from "antd";
import formatDate from "../../utils/formatDate";
import {Link} from 'react-router-dom';
import './finish.less'
import store from "./store";
import routes from "../../config/routes";

@observer
class Index extends Component {
    render() {
        const OK = require('../../assets/ok.png');
        const {namespace, app_id} = store
        return (
            <div className='create-finish'>
                <section className='finish'>
                    <div className='finish-title'>
                        <img src={OK}/>
                        <strong>创建成功</strong>
                        <p>
                            容器应用创建成功，您可重新创建一个新应用，或者返回到概 览页面查看该应用的详细信息。
                        </p>
                    </div>
                    <div className='finish-cont'>
                        <strong>应用创建日志</strong>
                        <ul>
                            <li className='finish-cont-list'>
                                <span className='time'>{formatDate(new Date())}</span>
                                <span className='text'>检查应用配置区域信息</span>
                            </li>
                            <li className='finish-cont-list'>
                                <span className='time'>{formatDate(new Date())}</span>
                                <span className='text'>检查应用配置区域信息</span>
                            </li>
                            <li className='finish-cont-list'>
                                <span className='time'>{formatDate(new Date())}</span>
                                <span className='text'>检查应用配置区域信息</span>
                            </li>
                            <li className='finish-cont-list'>
                                <span className='time'>{formatDate(new Date())}</span>
                                <span className='text'>检查应用配置区域信息</span>
                            </li>
                            <li className='finish-cont-list'>
                                <span className='time'>{formatDate(new Date())}</span>
                                <span className='text'>检查应用配置区域信息</span>
                            </li>
                        </ul>
                    </div>
                </section>
                {/* 底部按钮 */}
                <div className="steps-action">
                    <Space>
                        <Button onClick={() => {
                            store.init()
                            store.current = 0
                        }}>继续创建</Button>
                        <Link to={`${routes.deployment_details}/${namespace}/${app_id}`}>
                            <Button type="primary"
                                    onClick={() => {
                                        setTimeout(() => {
                                            store.init()
                                            store.current = 0
                                        }, 1000)
                                    }}>返回</Button>
                        </Link>
                    </Space>
                </div>
            </div>
            // <Result
            //     status="success"
            //     title="创建成功"
            //     subTitle="容器应用创建成功，您可以点击确认查看详细信息。"
            //     extra={[
            //         ( <Link key="console" to={{ pathname: `${routes.deployment_details}/${store.createAppId}` }} ><Button type="primary">确认</Button></Link>),
            //         (<Link key="buy" to={routes.deployment}><Button>返回</Button></Link>),
            //     ]}
            // />
        )
    }
}

export default Index;