import React from "react";
import './header_steps.less'

interface IStepsPorps {
    current: number
}

const HeaderSteps: React.FC<IStepsPorps> = ({current}) => {
    let className = 'one';
    switch (current) {
        case 0:
            className = 'one'
            break;
        case 1:
            className = 'two'
            break;
        case 2:
            className = 'three'
            break;
        case 3:
            className = 'four'
            break;
    }

    return (
        <section className={'header-steps ' + className}>
            {/* 步骤 */}
            <div className='steps'>
                <span>应用配置</span>
                <span>容器配置</span>
                <span>规格确认</span>
                <span>完成</span>
            </div>
            {/* 线 */}
            <div className='steps-hr'>
                <i className='this'/>
            </div>
        </section>)
}

export default HeaderSteps