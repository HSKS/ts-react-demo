import React, {FC} from "react"
import {Cascader, Radio} from 'antd'
import './c_image.less'
import {CascaderOptionType, CascaderValueType} from "antd/lib/cascader";
import ContainerRow from '../../components/ContainerRow'

interface IImage {
    title: string
    titleWidth?: number
    mirrorType: number
    selectMirrorType: (val: number) => void
    data: CascaderOptionType[]
    value: number[] | undefined
    onChange: (val: CascaderValueType, image: string | undefined) => void
    required?:boolean
}

const CImage: FC<IImage> = ({title, titleWidth, mirrorType, selectMirrorType, data, value, required,onChange}) => {
    const imageMap = new Map<string, string>()
    for (let i = 0; i < data.length; i++) {
        let iv = data[i]
        if (iv.children && iv.children.length) {
            for (let j = 0; j < iv.children.length; j++) {
                let jv = iv.children[j]
                const key = `${iv.value}#${jv.value}`
                const image = `${iv.label}:${jv.label}`
                imageMap.set(key, image)
            }
        }
    }
    return <ContainerRow title={title} titleWidth={titleWidth} className='c-image paralleling' required={required}>
        <div className='content'>
            <Radio.Group className='content-radio' value={mirrorType} onChange={(e) => {
                selectMirrorType(e.target.value)
            }}>
                <Radio value={1}>中心镜像</Radio>
                <Radio value={2}>业务镜像</Radio>
            </Radio.Group>
            <Cascader placeholder='请选择镜像' allowClear={false} options={data} value={value} onChange={(val) => {
                let key = val.join('#')
                let image = imageMap.get(key)
                onChange(val, image)
            }}/>
        </div>
    </ContainerRow>
}

export default CImage;