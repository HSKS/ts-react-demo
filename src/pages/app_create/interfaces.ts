export interface Cluster {
    cluster_label: string | undefined
    replicas: number | undefined
}

export interface HostPathVolume {
    read_only: boolean
    from: string | undefined
    to: string | undefined
    edit: boolean
}

export interface LivenessProbe {
    type: number
    command: string[] | undefined
    port: number | undefined
    path: string | undefined
    headers: { [prop: string]: string } | undefined
    initial_delay_seconds: number | undefined
    period_seconds: number | undefined
    timeout_seconds: string | undefined
    edit: boolean | undefined
}

export interface CreateAppParam {
    addr: string | undefined
    clusters: Cluster[]
    namespace: string | undefined
    app_name: string | undefined
    app_id: string | undefined
    describe?: string | undefined
    mirror_id: number
    mirror_tag_id: number
    mirror_type: number
    node_affinity_labels?: { [prop: string]: string[] } | undefined
    cpu: number
    mem: number
    gpu_num?: number | undefined
    gpu_id?: number | undefined
    is_proxy: boolean
    container_port?: number | undefined
    protocol?: 'TCP' | 'UDP' | undefined
    is_check_resource_quotas: boolean
    host_path_volumes?: HostPathVolume[] | undefined
    env?: { [prop: string]: string } | undefined
    is_liveness_probe: boolean
    liveness_probe?: LivenessProbe | undefined
    restart_policy?: 'Always' | 'OnFailure' | 'Never' | undefined
    labels?: { [prop: string]: string } | undefined
    command?: string[] | undefined
}

export interface ResourceModel {
    cluster_label: string
    cpu: number
    disk: number
    gpu_id: number
    gpu_name: string
    gpu_num: number
    id: number
    is_deleted: number
    mem: number
    name: string
}

//NodeType
export interface NT {
    node_type: string
    node_type_cn: string
    pod_models: Array<ResourceModel>
}

export interface MirrorInfo {
    id: number
    mirror_type: number
    user_id: number
    hub_id: number
    mirror_address: string
    name: string
    description: string
    system: string
    MirrorTags: Array<MirrorTagInfo>
}
export interface MirrorTagInfo {
    id: number
    tag_name: string
    mirror_id: number
}
export interface MirrorData {
    pub_mirror: Array<MirrorInfo>
    pri_mirror: Array<MirrorInfo>
}