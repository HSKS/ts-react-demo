import React, {FC} from "react"
import {Checkbox, Radio, Table} from 'antd'
import {TablePaginationConfig} from "antd/lib/table";
import './c_resource_quota.less'
import {NT, ResourceModel} from './interfaces'
import ContainerRow from '../../components/ContainerRow'

interface IResourceQuota {
    title: string
    titleWidth?: number
    pagination?: TablePaginationConfig | false | undefined
    nodeTypes: Array<NT>
    nodeType: NT
    selectNodeType: (val: NT) => void
    resourceQuotaIndex: number
    selectResourceQuota: (val: ResourceModel, idx: number) => void
}

const CResourceQuota: FC<IResourceQuota> = ({title, titleWidth, nodeTypes, nodeType: {node_type, node_type_cn, pod_models}, selectNodeType, pagination, resourceQuotaIndex, selectResourceQuota}) => {
    if (!titleWidth) titleWidth = 120
    let dataSource = new Array<ResourceModel>()
    if (pod_models) dataSource = [...pod_models]
    return <ContainerRow title={title} titleWidth={titleWidth} className='c-resource-quota' required>
        <div className='content'>
            <Radio.Group value={node_type} onChange={(e) => {
                const val = e.target.value
                for (let i = 0; i < nodeTypes.length; i++) {
                    const nt = nodeTypes[i]
                    if (nt.node_type === val) {
                        selectNodeType(nt)
                        break
                    }
                }
            }}>
                {
                    nodeTypes.map(item => <Radio.Button className='radio-button' key={item.node_type}
                                                        value={item.node_type}>{item.node_type_cn}</Radio.Button>)
                }
            </Radio.Group>
            <Table bordered={true} size="small" columns={node_type === 'GPU' ? [{
                title: '选择',
                dataIndex: 'action',
                key: 'action',
                align: 'center',
                render: (text, rcd, idx) => {
                    return <Checkbox checked={idx === resourceQuotaIndex} onChange={(val) => {
                        selectResourceQuota(rcd, idx)
                    }
                    }/>
                }
            }, {
                title: '名称',
                dataIndex: 'string',
                key: 'string',
                align: 'center',
            }, {
                title: 'CPU',
                dataIndex: 'cpu',
                key: 'cpu',
                align: 'center',
                render: text => `${text}核`
            }, {
                title: '内存',
                dataIndex: 'mem',
                key: 'mem',
                align: 'center',
                render: text => `${text}G`
            }, {
                title: 'GPU',
                dataIndex: 'gpu_num',
                key: 'gpu_num',
                align: 'center',
                render: text => `${text}G`
            }, {
                title: 'GPU名称',
                dataIndex: 'gpu_name',
                key: 'gpu_name',
                align: 'center',
            }] : [{
                title: '选择',
                dataIndex: 'action',
                key: 'action',
                align: 'center',
                render: (text, rcd, idx) => {
                    return <Checkbox checked={idx === resourceQuotaIndex} onChange={(val) => {
                        selectResourceQuota(rcd, idx)
                    }
                    }/>
                }
            }, {
                title: '名称',
                dataIndex: 'string',
                key: 'string',
                align: 'center',
            }, {
                title: 'CPU',
                dataIndex: 'cpu',
                key: 'cpu',
                align: 'center',
                render: text => `${text}核`
            }, {
                title: '内存',
                dataIndex: 'mem',
                key: 'mem',
                align: 'center',
                render: text => `${text}G`
            }]} dataSource={[...dataSource]} pagination={pagination}/>
        </div>
    </ContainerRow>
}

export default CResourceQuota;