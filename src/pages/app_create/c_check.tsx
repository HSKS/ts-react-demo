import React, { Component, useState, useEffect } from "react"
import { observer } from "mobx-react"
import { Cascader, Button, Collapse, Input, InputNumber, message, Modal, Popconfirm, Radio, Select, Table, Divider } from 'antd';
import ContainerRow from '../../components/ContainerRow'
import StringInput from '../../components/StringInput'
import store from './store'
import "./c_check.less"


const Index: React.FC = () => {
    const [typeRadio, setTypeRadio] = useState(0)
    const { liveness_probe, setLivenessProbe } = store

    useEffect(() => {
        if (liveness_probe) liveness_probe.type = typeRadio
    }, [typeRadio])

    return (<div className="advanced-check">
        {/* 存活探针开始 */}
        <div className="liveness-title">应用存活探针：</div>
        <div className='liveness-probe'>
            <ContainerRow title="检查方式" className='probe-title'>
                <Radio.Group value={typeRadio}
                    onChange={(e) => setTypeRadio(e.target.value)}>
                    <Radio value={0}>不配置</Radio>
                    <Radio value={1}>HTTP请求检查</Radio>
                    <Radio value={2}>执行命令检查</Radio>
                    <Radio value={3}>TCP检查</Radio>
                </Radio.Group>
            </ContainerRow>
            {/* 内容 */}
            <div className="liveness-probe-cont">
                {/* ============================ */}
                {typeRadio === 1 && (<>
                    <StringInput title='路径' value={liveness_probe && liveness_probe.path} onChange={val => setLivenessProbe({ path: val })} />
                    <StringInput title='端口' value={liveness_probe && String(liveness_probe.port)} onChange={val => setLivenessProbe({ port: Number(val) })} />
                    <StringInput title='延迟时间/秒' value={liveness_probe && String(liveness_probe.initial_delay_seconds)} onChange={val => setLivenessProbe({ initial_delay_seconds: Number(val) })} />
                    <StringInput title='超时时间/秒' value={liveness_probe && String(liveness_probe.timeout_seconds)} onChange={val => setLivenessProbe({ timeout_seconds: Number(val) })} />
                </>)}
                {typeRadio === 2 && <>
                    <StringInput title='执行命令' value={liveness_probe && liveness_probe.command?.join()} onChange={val => {
                        // setLivenessProbe({ path: val })
                    }} />
                    <StringInput title='延迟时间/秒' value={liveness_probe && String(liveness_probe.initial_delay_seconds)} onChange={val => setLivenessProbe({ initial_delay_seconds: Number(val) })} />
                    <StringInput title='超时时间/秒' value={liveness_probe && String(liveness_probe.timeout_seconds)} onChange={val => setLivenessProbe({ timeout_seconds: Number(val) })} />
                </>}
                {typeRadio === 3 && <>
                    <StringInput title='端口' value={liveness_probe && String(liveness_probe.port)} onChange={val => setLivenessProbe({ port: Number(val) })} />
                    <StringInput title='延迟时间/秒' value={liveness_probe && String(liveness_probe.initial_delay_seconds)} onChange={val => setLivenessProbe({ initial_delay_seconds: Number(val) })} />
                    <StringInput title='超时时间/秒' value={liveness_probe && String(liveness_probe.timeout_seconds)} onChange={val => setLivenessProbe({ timeout_seconds: Number(val) })} />
                </>}
                {/* ============================ */}
            </div>
        </div>
    </div>)
}

export default Index;