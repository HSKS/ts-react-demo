import React, {Component} from "react"
import {observer} from "mobx-react"
import {Input} from 'antd'
import store from './store'
import "./height_options.less"
import CTable from "../../components/CTable";

@observer
class Index extends Component {

    render() {
        const {envArr} = store
        return (
            <CTable columns={[{
                title: '键',
                key: 'key',
                dataIndex: 'key',
                align: 'center',
                render: (val, {key, edit}, idx) => edit ? (
                    <Input defaultValue={val} onChange={(e) => {
                        let v = e.target.value
                        store.envArr[idx].key = v
                    }}/>
                ) : val
            }, {
                title: '值',
                key: 'value',
                dataIndex: 'value',
                align: 'center',
                render: (val, {value, edit}, idx) => edit ? (
                    <Input defaultValue={val} onChange={(e) => {
                        let v = e.target.value
                        store.envArr[idx].value = v
                    }}/>
                ) : val
            }, {
                title: '操作',
                key: 'action',
                dataIndex: 'action',
                align: 'center',
                render: (val, {edit}, idx) => edit ? (
                    <a onClick={() => {
                        store.envArr[idx].edit = false
                        store.envArr = [...store.envArr]
                    }}>确定</a>
                ) : (<div>
                    <a onClick={() => {
                        store.envArr[idx].edit = true
                        store.envArr = [...store.envArr]
                    }}>编辑</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a onClick={() => {
                        store.envArr.splice(idx, 1)
                        store.envArr = [...store.envArr]
                    }}>删除</a>
                </div>)
            }]} dataSource={[...envArr]} add={() => {
                store.envArr.push({
                    edit: true,
                    key: '',
                    value: ''
                })
            }} hideTitle={true} showAdd={true} pagination={false}/>
        )
    }
}

export default Index