export interface AppDetails {
    name: string
    app_id: string
    username: string
    namespace: string
    namespace_cn: string
    mirror_address: string
    mirror_name: string
    tag_name: string
    cpu: number
    mem: number
    describe: string
    created_at: string
    updated_at: string
    is_hpa: number
}

export interface PodInfo {
    cluster_label: string
    cpu: string
    created_at: string
    mem: string
    name: string
    namespace: string
    status: string
}

export interface HaInfo {
    cluster_name: string
    cluster_label: string
    protocol: string
    ip: string
    container_port: number
    port: number
    created_at: string
}

export interface Cluster {
    id: number,
    cluster_label: string,
    cluster_name: string,
    cluster_type: number,
    master_url: string,
    status: number,
    area_id: number,
    created_at: string,
    updated_at: string
}



export interface AppHpas {
    cpu: Array<HpaInfo>
    mem: Array<HpaInfo>
}

export interface HpaInfo {
    id?: number
    app_id: string
    metric_type: string
    val_type: number
    expr_value: number
    start_time: number
    end_time: number
    min_num: number
    max_num: number
    expr_time: number
    is_delete: number
    cluster_labels: string
}



