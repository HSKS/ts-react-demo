import React, { Component } from "react"
import { observer } from "mobx-react"
import {
    Descriptions,
    Tabs,
    Table,
    Tag,
    message, Button
} from 'antd'
import { withRouter, RouteComponentProps } from 'react-router-dom';

import store from "./store"
import "./styles.less"
import formatDate from '../../utils/formatDate'
import Container from '../../components/Container'
import CSlide from '../../components/CSlide'
import PodLogs from './podLogs'
import CElasticity from './c_elasticity'
import apis from "../../config/apis";
import host from "../../config/host";

const { TabPane } = Tabs;

interface Params {
    app_id: string
    namespace: string
}

@observer
class Index extends Component<RouteComponentProps, {}> {

    componentDidMount(): void {
        const { app_id, namespace } = this.props.match.params as Params
        store.app_id = app_id
        store.namespace = namespace
        store.GetAppDetails(app_id, namespace)
        store.GetPodList(app_id, namespace)
        store.GetHaList(app_id, namespace)
        store.GetHpaDetail(app_id)
        store.GetAppClusterList(app_id)
    }

    //状态展示样式
    totalStatus = (state: string) => {
        let color = ''
        let title = ''
        console.log(state);
        switch (state) {
            case 'Succeeded':
                title = '成功'
                color = 'success'
                break;
            case 'Running':
                title = '运行中'
                color = 'processing'
                break;
            case 'Pending':
                title = '等待中'
                color = 'warning'
                break;
            case 'Failed':
                title = '失败'
                color = 'error'
                break;
            case 'Unknown':
                title = '未知状态'
                color = 'default'
                break;
        }
        return <Tag color={color}>{title}</Tag>
    }

    render() {
        const { appDetails, podList, haList } = store;

        return (<Container title={appDetails.name} className='deployment-details' options={<div>
            <Button size='small'>启动Haproxy</Button>
        </div>}>
            {/* 列表内容开始 */}
            <section>
                {/* 基本信息 */}
                {/* <Descriptions column={2} className="content-cont">
                        <Descriptions.Item label="应用名称">{appDetails.name}</Descriptions.Item>
                        <Descriptions.Item label="创建用户">{appDetails.username}</Descriptions.Item>
                        <Descriptions.Item label="APPID">{appDetails.app_id}</Descriptions.Item>
                        <Descriptions.Item label="业务系统">{appDetails.namespace_cn}</Descriptions.Item>
                        <Descriptions.Item label="CPU">{appDetails.cpu}Core</Descriptions.Item>
                        <Descriptions.Item label="内存">{appDetails.mem}Gi</Descriptions.Item>
                        <Descriptions.Item
                            label="创建时间">{formatDate(new Date(appDetails.created_at))}</Descriptions.Item>
                        <Descriptions.Item
                            label="更新时间">{formatDate(new Date(appDetails.updated_at))}</Descriptions.Item>
                        <Descriptions.Item label="描述">{appDetails.describe || '暂无'}</Descriptions.Item>
                        <Descriptions.Item
                            label="镜像">{`${appDetails.mirror_address}:${appDetails.tag_name}`}</Descriptions.Item>
                    </Descriptions> */}

                <div className='header-cont'>
                    <h3>Test1</h3>
                    {/* 滑块 */}
                    <CSlide success={1} processing={4} />
                </div>

                {/* tab选卡项 */}
                <div className="content-cont">
                    <Tabs defaultActiveKey="1" onChange={() => {
                    }}>
                        <TabPane tab="实例列表" key="1">
                            <Table pagination={false}
                                columns={[{
                                    title: '实例名称/ ID',
                                    dataIndex: 'name',
                                    key: 'name',
                                    align: 'center',
                                    render: (text: string) => <a>{text}</a>,
                                },
                                {
                                    title: '集群标签',
                                    dataIndex: 'cluster_label',
                                    key: 'cluster_label',
                                    align: 'center',
                                },
                                {
                                    title: '状态',
                                    dataIndex: 'status',
                                    key: 'status',
                                    align: 'center',
                                    render: (status) => this.totalStatus(status)
                                },
                                {
                                    title: 'CPU',
                                    dataIndex: 'cpu',
                                    key: 'cpu',
                                    align: 'center',
                                    render: text => `${text}Core`
                                },
                                {
                                    title: '内存',
                                    dataIndex: 'mem',
                                    key: 'mem',
                                    align: 'center',
                                    render: text => `${(text / 1024).toFixed(2)}GB`
                                },
                                {
                                    title: '创建时间',
                                    dataIndex: 'created_at',
                                    key: 'created_at',
                                    align: 'center',
                                    render: text => formatDate(new Date(text))
                                }, {
                                    title: '操作',
                                    dataIndex: 'action',
                                    key: 'action',
                                    align: 'center',
                                    render: (text, { name, namespace, cluster_label }) => {
                                        return (
                                            <div>
                                                <a onClick={() => {
                                                    const ok = store.restartPod(store.app_id, cluster_label, name, namespace)
                                                    if (ok) {
                                                        message.success('重启成功')
                                                    }
                                                }}>重启</a>
                                                           &nbsp;&nbsp;&nbsp;&nbsp;
                                                <a onClick={() => {
                                                    const api = apis.DownloadLogs
                                                    const link = document.createElement('a');
                                                    link.setAttribute("download", "");
                                                    link.href = `${host + api.url}?cluster_label=${cluster_label}&namespace=${namespace}&pod_name=${name}`;
                                                    link.click();
                                                }}>日志下载</a>
                                                           &nbsp;&nbsp;&nbsp;&nbsp;
                                                <a onClick={() => {
                                                    store.currentPodName = name
                                                    store.currentNamespace = namespace
                                                    store.currentClusterLabel = cluster_label
                                                    store.GetPodLogs()
                                                    store.logModalStatus = true
                                                }}>日志查看</a>
                                            </div>
                                        )
                                    }
                                }]}
                                dataSource={[...podList]}
                                rowKey={(record: any) => record.app_id}
                            />
                        </TabPane>
                        <TabPane tab="负载均衡" key="2">
                            <Table pagination={false}
                                columns={[{
                                    title: '集群名称',
                                    dataIndex: 'cluster_name',
                                    key: 'cluster_name',
                                    align: 'center',
                                    render: (text: string) => <a>{text}</a>,
                                },
                                {
                                    title: '集群标签',
                                    dataIndex: 'cluster_label',
                                    key: 'cluster_label',
                                    align: 'center',
                                },
                                {
                                    title: '协议',
                                    dataIndex: 'protocol',
                                    key: 'protocol',
                                    align: 'center'
                                },
                                {
                                    title: 'IP',
                                    dataIndex: 'ip',
                                    key: 'ip',
                                    align: 'center',
                                },
                                {
                                    title: '端口号',
                                    dataIndex: 'port',
                                    key: 'port',
                                    align: 'center',
                                },
                                {
                                    title: '创建时间',
                                    dataIndex: 'created_at',
                                    key: 'created_at',
                                    align: 'center',
                                    render: text => formatDate(new Date(text))
                                },
                                {
                                    title: '操作',
                                    dataIndex: 'action',
                                    key: 'action',
                                    align: 'center',
                                    render: (text, { cluster_label, container_port, protocol }, idx) => <div>
                                        <a onClick={() => {
                                            store.CreateHaproxy(cluster_label, protocol, container_port)
                                        }}>启动</a>
                                                   &nbsp;&nbsp;&nbsp;&nbsp;
                                                   <a onClick={() => {
                                            store.DeleteHaproxy(cluster_label)
                                        }}>停止</a>
                                    </div>
                                }]}
                                dataSource={[...haList]}
                                rowKey={(record: any) => `${record.cluster_label}`}
                            />
                        </TabPane>

                        <TabPane tab="弹性策略" key="4">
                            <CElasticity
                                visible={appDetails.is_hpa !== 0}
                                params={this.props.match.params}
                                hpaList={store.updateAppHpa}
                                appClusterList={store.appClusterList}
                                onClick={value => store.SetUpdateAppHpa(value)}
                            />
                        </TabPane>
                        <TabPane tab="参数配置" key="5">
                            参数配置
                            </TabPane>
                    </Tabs>
                </div>
            </section>
            {/* 列表内容结束 */}
            <PodLogs />
        </Container>
        )
    }
}

export default withRouter(Index as any)