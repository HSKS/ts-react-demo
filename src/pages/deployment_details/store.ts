import { observable, action } from "mobx"
import { message } from "antd";
import request from '../../utils/request'
import apis from '../../config/apis'
import { AppDetails, PodInfo, HaInfo, AppHpas, Cluster } from './interfaces'
// @ts-ignore
import { fetch as fetchPolyfill } from "whatwg-fetch"
import host from '../../config/host'
import transformUtils from "../../utils/transformUtils";

class Store {
    @observable logModalStatus = false
    @observable app_id = ''
    @observable namespace = ''

    @observable appDetails = {} as AppDetails
    @action GetAppDetails = async (app_id: string, namespace: string) => {
        const data = await request(apis.GetAppDetails, { app_id, namespace })
        if (!data) return
        this.appDetails = data as AppDetails
    }

    @observable podList: Array<PodInfo> = []
    @action GetPodList = async (app_id: string, namespace: string, cluster_label: string = '') => {
        const data = await request(apis.GetPodList, { app_id, namespace, cluster_label })
        if (!data) return
        this.podList = data
    }

    @observable haList: Array<HaInfo> = []
    @action GetHaList = async (app_id: string, namespace: string) => {
        const data = await request(apis.GetHaproxyList, { app_id, namespace })
        if (!data) return
        this.haList = data
    }

    @observable appClusterList: Array<Cluster> = []
    @action GetAppClusterList = async (app_id: string) => {
        const data = await request(apis.GetAppClusterList, { app_id })
        if (!data) return
        this.appClusterList = data
    }


    // 更改弹性策略
    @observable updateAppHpa: AppHpas = { cpu: [], mem: [] }

    @action SetUpdateAppHpa = async (params: any) => {
        const data = await request(apis.UpdateAppHpa, params)
        if (data !== void 0) message.success('保存成功')
    }

    // 弹性策略初始化
    @action GetHpaDetail = async (app_id: string) => {
        this.updateAppHpa = { cpu: [], mem: [] }
        let msg = { ...apis.GetHpaDetail }
        msg.url = msg.url + app_id
        const data: AppHpas = await request(msg)
        if (!data.cpu.length && !data.mem.length) return
        this.updateAppHpa = data
    }


    @observable restartPod = async (app_id: string, cluster_label: string, pod_name: string, namespace: string) => {
        const api = apis.RestartPod
        const resp = await request(api, { app_id, cluster_label, pod_name, namespace })
        if (!resp) return false
        setTimeout(() => {
            this.GetPodList(app_id, namespace, cluster_label)
        }, 6000)
        return true
    }

    currentPodName = ''
    currentClusterLabel = ''
    currentNamespace = ''


    @observable podLogs: string = ''
    @action GetPodLogs = async () => {
        const api = apis.GetPodLogs
        const resp = await fetchPolyfill(host + api.url + `?pod_name=${this.currentPodName}&namespace=${this.currentNamespace}&cluster_label=${this.currentClusterLabel}`, {
            method: api.method,
            credentials: 'include',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'token': localStorage.getItem("token")
            },
        })
        this.podLogs = await resp.text()
    }

    @action CreateHaproxy = async (cluster_label:string, protocol: string, container_port: number) => {
        const api = apis.CreateHaproxy
        const params = {
            cluster_labels: [cluster_label],
            container_port: container_port,
            namespace: this.namespace,
            app_id: this.app_id,
            protocol: protocol
        }
        const resp = request(api, params)
        if (!resp) return
        message.success('负载均衡启动成功')
        this.GetHaList(this.app_id, this.namespace)
    }

    @action DeleteHaproxy = async (cluster_label: string) => {
        const api = apis.DeleteHaproxy
        const params = {
            cluster_labels: [cluster_label],
            namespace: this.namespace,
            app_id: this.app_id,
        }
        const resp = request(api, params)
        if (!resp) return
        message.success('负载均衡已停止')
        this.GetHaList(this.app_id, this.namespace)
    }
}

export default new Store()