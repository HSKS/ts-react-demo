import React from "react"
import { Select } from 'antd'
const { Option } = Select;

interface ISelect {
    data: Array<{
        label: string
        value: string
        [prop: string]: any
    }>
    values: Array<string>
    onChange: (val: any, label?: any) => void
}

const Index:React.FC<ISelect> = ({values,data,onChange}) => {
    return (<Select className='input-com' value={values}
        mode="multiple"
        style={{width:570}}
        onChange={(val,option) => {
            // let lb = ''
            // for (let i = 0; i < data.length; i++) {
            //     if (data[i].value === val) {
            //         lb = data[i].label
            //         break
            //     }
            // }
            // console.log(val,option);
            onChange(val,option)
        }}
        optionLabelProp="label"
    >
        {
            data.map((item) => (
            <Option key={item.value} value={item.value} label={item.label}>
                {item.label}
            </Option>))
        }

    </Select>)
}

export default Index;