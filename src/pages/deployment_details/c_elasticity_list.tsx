import React from "react"
import moment from 'moment'

import {
    Space,
    Card,
    InputNumber,
    Select,
    DatePicker
} from 'antd'

import { PlusCircleOutlined, MinusCircleOutlined } from '@ant-design/icons';
import NumberSlider from '../../components/NumberSlider'
import CElastictySelect from "./c_elasticty_select";
import { HpaInfo, Cluster } from './interfaces'
const { Option } = Select;
const { RangePicker } = DatePicker;


// 弹性策略
interface IListPorps {
    type: string
    item: HpaInfo
    appClusterList: Array<Cluster>
    add(): void
    del(): void
    onChange(value: HpaInfo, type: string): void
}


const Index: React.FC<IListPorps> = ({ type, item, appClusterList, add, del, onChange }) => {
    let title = (type === 'cpu') ? 'CPU' : '内存'
    return (<Card size="small" title={`${title}策略`} >
        {/* 集群 */}
        <div className='rule-cont'>
            <span className='rule-title'>集群：</span>
            <CElastictySelect data={appClusterList.map(({ cluster_label, cluster_name }) => ({
                value: cluster_label,
                label: cluster_name
            }))} values={item.cluster_labels ? item.cluster_labels.split(',') : []}
                onChange={(val: Array<string>) => {
                    item.cluster_labels = val.join()
                    onChange(item, type)
                }} />
        </div>
        {/* CPU期望阈值 */}
        <div className='rule-cont'>
            <span className='rule-title'>{title}期望阈值：</span>
            <NumberSlider unit="%" message={item.expr_value} min={1} max={100}
                evts={val => {
                    item.expr_value = val as number
                    onChange(item, type)
                }} />
        </div>


        {/* 触发时间 */}
        <div className="rule-cont">
            <span className='rule-title'>触发时间：</span>
            <Space direction="vertical" size={12}>
                <RangePicker showTime
                    value={[moment(item.start_time), moment(item.end_time)]}
                    onChange={(val, dateString: string[]) => {
                        if (!val) return;
                        const [start, end] = dateString;
                        item.start_time = +new Date(start)
                        item.end_time = +new Date(end)
                        onChange(item, type)
                    }} />
            </Space>
        </div>


        {/* 最小值 */}
        <div className="rule-cont">
            <span className='rule-title'>最小值：</span>
            <InputNumber min={1} max={10} value={item.min_num} onChange={val => {
                item.min_num = val as number
                onChange(item, type)
            }} />

            {/* 最大值 */}
            <span >最大值：</span>
            <InputNumber min={1} max={10} value={item.max_num} onChange={val => {
                item.max_num = val as number
                onChange(item, type)
            }} />

            {/* 执行周期 */}
            <span >执行周期：</span>
            <span>
                <InputNumber min={1} max={10} defaultValue={item.expr_time} onChange={val => {
                    item.expr_time = val as number
                    onChange(item, type)
                }} />
                <Select defaultValue="0" style={{ width: 80 }}>
                    <Option value="0">秒</Option>
                </Select>
            </span>

            {/* 操作 */}
            <span className='operation'>
                <PlusCircleOutlined onClick={() => add()} />&nbsp;&nbsp;
            <MinusCircleOutlined onClick={() => del()} />
            </span>

        </div>
    </Card>
    )
}

export default Index;