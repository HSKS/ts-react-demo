import React, {Component} from "react"
import {observer} from "mobx-react"
import {CloseOutlined} from '@ant-design/icons'
import store from "./store"
import "./podLogs.less"

@observer
class Index extends Component {
    timer: NodeJS.Timeout | undefined
    pre: HTMLPreElement | null | undefined

    componentDidMount() {
        this.startPodLogTimer()
        if (this.pre) {
            const ch = this.pre.clientHeight
            const sh = this.pre.scrollHeight
            this.pre.scrollTo({
                top: sh-ch
            })
        }
    }

    componentWillUnmount() {
        this.stopPodLogTimer()
    }

    handleCancel = () => {
        // this.stopPodLogTimer()
        store.currentClusterLabel = ''
        store.currentNamespace = ''
        store.currentPodName = ''
        store.podLogs = ''
        store.logModalStatus = false
    }
    startPodLogTimer = () => {
        if (!this.timer) {
            this.timer = setInterval(async () => {
                if (!store.logModalStatus) return
                await store.GetPodLogs()
                if (this.pre) {
                    const ch = this.pre.clientHeight
                    const sh = this.pre.scrollHeight
                    this.pre.scrollTo({
                        top: sh-ch
                    })
                }
            }, 3000)
        }
    }
    stopPodLogTimer = () => {
        if (this.timer) {
            clearInterval(this.timer)
            this.timer = undefined
        }
    }
    render() {
        const {logModalStatus, podLogs} = store
        return (
            <div className='pod-logs' style={{display: logModalStatus ? 'flex' : 'none'}}>
                <div className='content'>
                    <CloseOutlined className='close-icon' onClick={this.handleCancel}/>
                    <pre className='pre' ref={ref => this.pre = ref}>{podLogs}</pre>
                </div>
            </div>
        )
    }
}

export default Index