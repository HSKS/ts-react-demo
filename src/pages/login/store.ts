import { observable, action } from "mobx"
import { message } from 'antd';
import request from '../../utils/request'
import apis from '../../config/apis'
import { aesEn } from '../../utils/crypto'
import { signKey } from '../../config/consts'
class Store {
    // cookie : 判断是否记住密码
    @observable cookie = false
    @observable username = ''
    @observable password = ''
    @action login = async () => {
        const api = apis.Login
        const { username, password } = this

        if (!username || !password) {
            message.warning('账号密码不可为空');
            return false
        }

        const params = {
            username: username,
            password: aesEn(signKey, password)
        }

        const resp = await request(api, params)
        if (!resp) return false;
        localStorage.setItem('token', resp)
        // 存储用户登录信息
        if (this.cookie) {
            const time = new Date(Date.now() + 604800000);
            document.cookie = `login=${encodeURIComponent(JSON.stringify({
                username, password
            }))};expires=${time.toUTCString()}`
        }
        return true
    }
}

export default new Store()