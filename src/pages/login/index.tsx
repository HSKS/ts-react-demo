import React, { Component } from "react"
import { observer } from "mobx-react"
import { Checkbox, Button, Input, message } from 'antd';
import { RouteComponentProps, withRouter } from "react-router-dom";
import store from "./store"
import "./styles.less"
import routes from "../../config/routes";

interface Login {
    username: string
    password: string
}

@observer
class Index extends Component<RouteComponentProps, {}> {

    state = {
        active: false,
    }

    componentDidMount(): void {
        // 读取用户本地登录信息
        if (document.cookie) {
            const login = document.cookie.match(/(?<=login=).*?(?=;|$)/g);
            if (!login) return;
            const { username, password }: Login = JSON.parse(decodeURIComponent(login[0]))
            store.username = username
            store.password = password
        }
    }

    render() {
        const { active } = this.state
        const { username, password } = store

        return (

            <div className={active ? 'login active' : 'login'}>
                {/* 头部开始 */}
                <header className='login-header'>
                    <img src="/images/logo1.png" />
                </header>
                {/* 头部结束 */}


                {/* 内容开始 */}
                <div className='login-content'>
                    {/* 展示文字开始 */}
                    <div className='content-title'>
                        <h3>欢迎来到星河</h3>
                        <p>开发环境无缝迁移到生产环境</p>
                        <p>提高部署服务和系统运维的效率</p>
                    </div>
                    {/* 展示文字结束 */}

                    {/* 登陆盒子开始 */}
                    <main className='content-box'>
                        <header className='content-box-tab'>
                            <div className='inside' onClick={() => this.setState({ active: false })}>内部租户</div>
                            <div className='outside' onClick={() => this.setState({ active: true })}>政企租户</div>
                        </header>
                        <section className='content-box-cont'>
                            {/*<img className='logo' src="/images/logo.png" />*/}
                            {/*<div className='input'>*/}
                            {/*    <label className='name'>账号</label>*/}
                            {/*    <Input bordered={false} value={username}*/}
                            {/*        onChange={(e) => {*/}
                            {/*            store.username = e.target.value*/}
                            {/*        }} />*/}
                            {/*</div>*/}
                            {/*<div className='input'>*/}
                            {/*    <label className='name'>密码</label>*/}
                            {/*    <Input.Password bordered={false} value={password}*/}
                            {/*        onChange={(e) => {*/}
                            {/*            store.password = e.target.value*/}
                            {/*        }} />*/}
                            {/*</div>*/}
                            {/*<Button className='submit' type='primary' block onClick={async (e) => {*/}
                            {/*    const ok = await store.login()*/}
                            {/*    if (ok) {*/}
                            {/*        message.success('登陆成功')*/}
                            {/*        setTimeout(() => {*/}
                            {/*            this.props.history.replace(routes.home)*/}
                            {/*        }, 500)*/}
                            {/*    }*/}
                            {/*}}>登陆</Button>*/}
                            {/*<div className='option'>*/}
                            {/*    <Checkbox onChange={(e) => store.cookie = e.target.checked}>记住密码</Checkbox>*/}
                            {/*    <a>忘记密码？</a>*/}
                            {/*</div>*/}
                        </section>

                    </main>
                    {/* 登陆盒子结束 */}
                </div>
                {/* 内容结束 */}


                {/* 底部开始 */}
                <footer className="login-footer">
                    {/* 浙江中国移动Kubenetes云管平台 */}
                </footer>
                {/* 底部结束 */}


                {/* 背景图片 */}
                <div className='background'></div>
            </div>

            // <div className='login' style={{backgroundImage: `url("/images/cloudBG.jpg")`}}>
            //     <Header className='header'>{base.appName}</Header>
            //     <Content className='content'>
            //         <div className='login-box'>
            //             <Tabs defaultActiveKey="1" centered>
            //                 <TabPane tab="登陆" key="1">
            //                     <div className='box'>
            //                         <span className='title'>用户名密码登陆</span>
            //                         <div className='input-item'>
            //                             <label className='tip'>用户名：</label>
            //                             <Input className='input' placeholder='请输入用户名' value={username}
            //                                    onChange={(e) => {
            //                                        store.username = e.target.value
            //                                    }}/>
            //                         </div>
            //                         <div className='input-item'>
            //                             <label className='tip'>密码：</label>
            //                             <Input className='input' placeholder='请输入密码' value={password} onChange={(e) => {
            //                                 store.password = e.target.value
            //                             }}/>
            //                         </div>
            //                         <div className='submit-box'>
            //                             <Button className='submit' type='primary' block onClick={async (e) => {
            //                                 const ok = await store.login()
            //                                 if (ok) {
            //                                     this.props.history.replace(routes.home)
            //                                 }
            //                             }}>登陆</Button>
            //                         </div>
            //                     </div>
            //                 </TabPane>
            //                 <TabPane tab="短信验证登陆" key="2">
            //                     <div className='box'>
            //                         <span className='title'>短信验证登陆</span>
            //                         <div className='input-item'>
            //                             <label className='tip'>手机号：</label>
            //                             <Input className='input' placeholder='请输入手机号'/>
            //                         </div>
            //                         <div className='input-item'>
            //                             <label className='tip'>验证码：</label>
            //                             <div className='input1'>
            //                                 <Input className='code' placeholder='请输入验证码'/>
            //                                 <div className='get-code'>获取验证码</div>
            //                             </div>
            //                         </div>
            //                         <div className='submit-box'>
            //                             <Link to={routes.home}>
            //                                 <Button className='submit' type='primary' block onClick={(e) => {

            //                                 }}>登陆</Button>
            //                             </Link>
            //                         </div>
            //                     </div>
            //                 </TabPane>
            //             </Tabs>
            //         </div>
            //     </Content>
            // </div>
        )
    }
}

export default withRouter(Index as any)