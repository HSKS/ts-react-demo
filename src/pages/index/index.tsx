import React, {Component} from "react"
import {Route, Switch} from "react-router-dom"
import Layout from "../../layout"
import routes from "../../config/routes";
import "./styles.less"
import Alerter from '../alerter'
import Cluster from '../cluster'
import Deployment from '../deployment'
import SystemCreate from '../system_create'
import Home from '../home'
import Image from '../image'
import Mirror from '../mirror'
import MirrorDetails from '../mirror_details'
import Collect from '../collect'
import Logger from '../logger'
import Permission from '../permission'
import System from '../system'
import Member from '../member'
import MirrorCreate from '../mirror_create'
import DeploymentDetails from '../deployment_details'
import ClusterDetails from '../cluster_details'
import ClusterDetailsNode from '../cluster_details_node'
import DeploymentDetails1 from '../deployment_details1'
import ImageDetails from '../image_details'
import SystemDetails from '../system_details'
import AppCreate from '../app_create'
import SystemUpdate from '../system_update'

class Index extends Component {
    render() {
        return (
            <div className='index'>
                <Layout {...this.props}>
                    <Switch>
                        <Route exact path={routes.home} component={Home}/>
                        <Route path={routes.deployment} component={Deployment}/>
                        <Route path={routes.cluster} component={Cluster}/>
                        <Route path={routes.image} component={Image}/>
                        <Route path={routes.mirror} component={Mirror}/>
                        <Route path={routes.mirror_create} component={MirrorCreate}/>
                        <Route path={`${routes.member}/:namespace_id`} component={Member}/>
                        <Route path={routes.collect} component={Collect}/>
                        <Route path={routes.mirror_details} component={MirrorDetails}/>
                        <Route path={routes.logger} component={Logger}/>
                        <Route path={routes.alerter} component={Alerter}/>
                        <Route path={routes.system} component={System}/>
                        <Route path={routes.permission} component={Permission}/>
                        <Route path={routes.deployment_create} component={AppCreate}/>
                        <Route path={`${routes.deployment_details}/:namespace/:app_id`} component={DeploymentDetails}/>
                        <Route path={`${routes.cluster_details}/:cluster_label/:cluster_name/:created_at`}
                               component={ClusterDetails}/>
                        <Route path={`${routes.cluster_details_node}/:cluster_label/:node_id`}
                               component={ClusterDetailsNode}/>
                        <Route path={routes.system_create} component={SystemCreate}/>
                        <Route path={routes.image_details} component={ImageDetails}/>
                        <Route path={routes.deployment_create1} component={DeploymentDetails1}/>
                        <Route path={`${routes.system_details}/:namespace`} component={SystemDetails}/>
                        <Route path={routes.app_create} component={AppCreate}/>
                        <Route path={`${routes.system_update}/:namespace`} component={SystemUpdate}/>
                    </Switch>
                </Layout>
            </div>
        )
    }
}

// @ts-ignore
export default Index;