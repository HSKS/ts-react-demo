import {action, observable} from "mobx"
import request from '../../utils/request'
import apis from '../../config/apis'
import {MirrorList} from './interfaces'
import {CascaderOptionType} from "antd/lib/cascader";

class Store {
    @observable name: string = ''
    @observable description: string = ''
    @observable is_collect: number = 1
    @observable download_count: number = 0
    @observable collect_count: number = 0
    @observable icon: string = ''
    @observable os: string | undefined = undefined
    @observable middle: string | undefined = undefined
    @observable framework: string | undefined = undefined
    @observable mirrorList: Array<MirrorList> = []
    @action init = () => {
        this.os = undefined
        this.middle = undefined
        this.framework = undefined
        this.GetmirrorList()
    }
    @action GetmirrorList = async () => {
        const api = apis.GetmirrorList
        const params = {
            mirror_name: this.name,
            system: this.os,
            system_framework: this.framework,
            middle: this.middle,
            is_collection:this.is_collect
        }
        const resp = await request(api, params)
        if (!resp) return
        this.mirrorList = resp
    }
    @action MirrorCollect = async (id: number, status: number) => {
        const api = apis.MirrorCollect
        if (status === 0) {
            status = 2
        } else if (status === 1) {
            status = 1
        }
        const params = {
            id: id,
            status: status
        }
        const resp = await request(api, params)
        if (!resp) return
        this.GetmirrorList()
    }
}

export default new Store()