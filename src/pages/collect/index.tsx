import React, { Component } from "react"
import { observer } from "mobx-react"
import { Button, Col, Input, Cascader, Progress, Row, Select, Space, Spin, Checkbox, Card, Rate, } from 'antd'
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';
import { PlusOutlined } from '@ant-design/icons';
import routes from "../../config/routes";
import "./styles.less"
import Container from '../../components/Container'
import formatDate from '../../utils/formatDate'
import store from "./store";
import { CascaderOptionType } from "antd/lib/cascader";

const { Option } = Select;

//搜索

interface Search {
    name: string | undefined
    setName: (val: string) => void
    onClick: () => void
}

const Search: React.FC<Search> = ({ onClick, name, setName }) => {
    return (
        <Space className='space'>
            <Input
                className='app-exp-input input'
                placeholder='名称搜索'
                value={name}
                onChange={(e) => {
                    setName(e.target.value)
                }}
            />
            <Button className='app-but' type='primary' onClick={() => onClick()}>搜索</Button>
        </Space>

    )
}


@observer
class Index extends Component<RouteComponentProps, {}> {
    componentDidMount(): void {
        store.GetmirrorList()
    }
    render() {
        const { name ,mirrorList    } = store
        return (
            <Container title='我的收藏' className='system-page'>
                {/* 列表内容开始 */}
                <section className="system-content">
                    <Spin delay={300} tip='加载中...' spinning={false}>
                        <Space size='middle' className="content-input">
                        <Search
                                name={name}
                                onClick={() => {
                                    store.GetmirrorList()
                                }}
                                setName={(val) => {
                                    store.name = val
                                }}
                            />
                        </Space>

                        <Row className="mirror_card">
                            {mirrorList.map((item, idx) => {
                                return <Col span={8} key={idx} onClick={(e)=> {
                                    e.stopPropagation()
                                    const rcd = JSON.stringify(item)
                                    localStorage.setItem("mirror_center_details", rcd)
                                    this.props.history.push(routes.mirror_details)
                                }}>
                                    <Card>
                                        <Row>
                                            <Col span={10}><img style={{width: '100%', height:'100%'}} src={item.icon}/></Col>
                                            <Col span={14}>
                                                <Row>
                                                    <Col offset={17} span={7} onClick={e=>e.stopPropagation()}><Rate count={1} defaultValue={item.collect_count} onChange={async (val)=>{
                                                        await store.MirrorCollect(item.id, val)
                                                        store.GetmirrorList()}}/> <span> 收藏</span></Col>
                                                    <Col>
                                                        <p>镜像名称: {item.name}</p>
                                                        <p>下载量: {item.download_count}</p>
                                                        <p>描述: {item.description}</p>
                                                    </Col>
                                                </Row>

                                            </Col>
                                        </Row>
                                    </Card>
                                </Col>
                            })}
                        </Row>
                    </Spin>
                </section>
                {/* 列表内容结束 */}
            </Container>
        )

    }

}


export default withRouter(Index as any)
