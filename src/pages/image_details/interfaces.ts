export interface MirrorTags {
    id: number
    tag_name: string
    mirror_id: number
    user_id: number
    state: number
    size: string
    address: string
    created_at: string
    updated_at: string
}
export interface TableColumns {
    id: number
    mirror_type: number
    mirror_address: string
    name: string
    description: string
    system: string
    system_framework: string
    middle: string
    icon: string
    download_count: number
    version_count: number
    collect_count: number
    is_collect: number
    created_at: string
    updated_at: string
    mirror_hub: {
        id: number
        hub_address: string
        name: string
        desc: string
        remark: string
        namespace_id: number
        is_private: number
    }
    user: {
        id: number
        username: string
    }
    mirror_tags: Array<MirrorTags>
}