import {action, observable} from "mobx"
import {TableColumns} from './interfaces'
import request from '../../utils/request'
import apis from '../../config/apis'
import {MirrorTags} from './interfaces'
class Store {
    @observable isEdit = false
    @observable imageName = ''
    @observable imageRepo = ''
    @observable repoType = ''
    @observable versionNum = 1
    @observable username = ''
    @observable updatedAt = ''
    @observable imageUrl = '1234567889990900'
    @observable description = ''
    @observable imageTags = new Array<MirrorTags>()

    @action deleteMirrorTag = async () => {

    }
}

export default new Store()