import React, {Component} from "react"
import {observer} from "mobx-react"
import {Button, Input, Tabs, Table, Row, Col} from 'antd'
import copy from 'copy-to-clipboard'
import {RouteComponentProps} from 'react-router-dom'
import "./styles.less"
import store from "./store"
import Container from '../../components/Container'
import {TableColumns} from './interfaces'
import formatDate from "../../utils/formatDate"

const {TabPane} = Tabs;

interface EditInput {
    isEdit: boolean
    title: string
    value: string
    onChange?: (val: string) => void
}

const EditInput: React.FC<EditInput> = ({isEdit, title, value, onChange}) => {

    return isEdit ? (
        <div className='edit-input'>
            <label className='label'>{title}：</label>
            <Input className='input' value={value} onChange={(e) => {
                if (onChange) onChange(e.target.value)
            }}/>
        </div>
    ) : (
        <div className='edit-input'>
            <label className='label'>{title}：</label>
            <span className='input'>{value}</span>
        </div>
    )
}

interface ITitle {
    title: string
}

const CTitle: React.FC<ITitle> = ({title}) => {
    return (
        <div className='c-title'>
            <i className='line'/><span className='title'>{title}</span>
        </div>
    )
}

@observer
class Index extends Component<RouteComponentProps, {}> {
    componentDidMount(): void {
        let imageDetailsStr = localStorage.getItem('image_details')
        console.log(imageDetailsStr)
        let imageDetails: TableColumns
        if (imageDetailsStr) {
            try {
                imageDetails = JSON.parse(imageDetailsStr)
                store.imageName = imageDetails.name
                store.imageRepo = imageDetails.mirror_hub.hub_address
                store.description = imageDetails.description
                store.repoType = imageDetails.mirror_type === 1 ? '公有' : '私有'
                store.versionNum = imageDetails.mirror_tags.length
                store.username = imageDetails.user.username
                store.updatedAt = formatDate(new Date(imageDetails.updated_at))
                store.imageUrl = imageDetails.mirror_address
                store.imageTags = imageDetails.mirror_tags
            } catch (e) {
                console.log(e)
            }
        }
    }

    render() {
        const {isEdit, imageName, imageRepo, repoType, versionNum, username, updatedAt, imageUrl, description, imageTags} = store
        return (
            <Container title='镜像详情' className='image-details'>
                {/* 内容开始 */}
                <section className="image-content">
                    <div className='top'>

                        <CTitle title='基本信息'/>
                        <div className='row'>
                            <div className='col'>
                                <EditInput isEdit={false} title='镜像名称' value={imageName} onChange={(val) => {
                                    store.imageName = val
                                }}/>
                                <EditInput isEdit={false} title='版本数' value={`${versionNum}`}/>
                            </div>
                            <div className='col'>
                                <EditInput isEdit={false} title='镜像仓库' value={imageRepo}/>
                                <EditInput isEdit={false} title='创建用户' value={username} onChange={(val) => {
                                    store.username = val
                                }}/>
                            </div>
                            <div className='col'>
                                <EditInput isEdit={false} title='仓库类型' value={repoType} onChange={(val) => {
                                    store.repoType = val
                                }}/>
                                <EditInput isEdit={false} title='更新时间' value={updatedAt} onChange={(val) => {
                                    store.updatedAt = val
                                }}/>
                            </div>
                            <div className='col'>
                                <div className='edit-input'>
                                    <label className='label'>镜像地址：</label>
                                    <span className='input'>{imageUrl}<a
                                        style={{display: 'inline-block', marginLeft: 10}} onClick={() => {
                                        copy(imageUrl)
                                    }}>复制</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <Tabs defaultActiveKey="1">
                        <TabPane tab="镜像版本" key="1">
                            <Table columns={[
                                {
                                    title: '镜像版本',
                                    dataIndex: 'tag_name',
                                    key: 'tag_name',
                                    align: 'center',
                                },
                                {
                                    title: '大小',
                                    dataIndex: 'size',
                                    key: 'size',
                                    align: 'center',
                                },
                                {
                                    title: '镜像扫描',
                                    dataIndex: 'state',
                                    key: 'state',
                                    align: 'center',
                                    render: (text) => {
                                        return (`${text}` === "1" ? <span style={{color: '#0f0'}}>· 安全</span> :
                                            <span style={{color: '#f00'}}>· 危险</span>);
                                    }
                                },
                                {
                                    title: '更新时间',
                                    align: 'center',
                                    key: 'updated_at',
                                    dataIndex: 'updated_at',
                                    render: text => formatDate(new Date(text))
                                },
                                {
                                    title: '下载指令',
                                    align: 'center',
                                    key: 'address',
                                    dataIndex: 'address',
                                    render: text => <div>
                                        <span>{text}</span>&nbsp;&nbsp;<a onClick={() => {
                                        copy(text)
                                    }
                                    }>复制</a>
                                    </div>
                                },
                                // {
                                //     title: '操作',
                                //     align: 'center',
                                //     key: 'action',
                                //     render: (text, {id}) => <a onClick={() => {
                                //         console.log(id)
                                //     }}>删除</a>,
                                // },
                            ]} dataSource={[...imageTags]}/>
                        </TabPane>
                        <TabPane tab="镜像描述" key="2">
                            <Row className="button_edit">
                                <Col span={22}>
                                    {/* <h2>镜像描述</h2> */}
                                    <p>{description || '该镜像没有任何描述信息...'}</p>
                                </Col>
                                <Col span={2}>
                                    {/* <Button className='edit-btn' onClick={async () => {
                                        if (isEdit) {
                                            //调用编辑提交的接口
                                        }
                                        store.isEdit = !store.isEdit
                                    }}>{isEdit ? '编辑完成' : '编辑镜像'}</Button> */}
                                </Col>
                            </Row>
                        </TabPane>

                        <TabPane tab="操作指南" key="3">
                            <p>
                                <h3>操作指南</h3>
                                <h4>一、上传镜像</h4>
                                <p>
                                    1、准备好可执行文件，配置文件，编写Dockerfile；
                                </p>
                                <p>
                                    2、执行 <strong>docker build .</strong> 命令来构建镜像；
                                </p>
                                <p>
                                    3、执行 <strong>docker save -o &lt;指定地址和文件名称&gt;.tar.gz &lt;镜像名称&gt;</strong> 来打包镜像；
                                </p>
                                <p>
                                    3、进入镜像上传页面编写必要镜像信息，上传镜像文件，点击上传按钮完成镜像上传。
                                </p>
                                <h4>二、使用镜像</h4>
                                <p>
                                    1、应用发布前请先选择一个镜像版本复制镜像下载指令；
                                </p>
                                <p>
                                    2、到应用发布页面将该镜像下载指令复制到镜像地址输入框内即可；
                                </p>
                            </p>
                        </TabPane>
                    </Tabs>
                </section>
                {/* 内容结束 */}
            </Container>

        )

    }

}


export default Index;
