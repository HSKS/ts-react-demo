import React, { Component } from "react"
import { Collapse, Layout } from 'antd'
import { observer } from "mobx-react"
import { Gauge } from '@ant-design/charts';
import "./styles.less"
const { Header, Footer, Sider, Content } = Layout;
const { Panel } = Collapse;
const text = `基本信息：123`;


// 扇形仪盘表
interface IChartGauge {
    title: string
    value: number
    size?: number
}
const ChartGauge: React.FC<IChartGauge> = (props: IChartGauge) => {
    const config = {
        title: {
            visible: true,
            text: props.title,
        },
        value: props.value,
        min: 0,
        max: 100,
        range: [0, props.value],
        center: ['50%', '50%'],
        format: (v: any) => {
            return v + '%';
        },
        color: ['l(0) 0:#b0d0ff 1:#5f92f9'],
        statistic: {
            visible: true,
            text: props.value + '%',
            color: '#30bf78',
        },
    };
    // @ts-ignore
    return <Gauge {...config} />;
};

@observer
class Index extends Component {
    render() {
        return (

            <Layout className='cluster-details-node'>
                {/* 左侧内容开始 */}
                <Sider theme="light" className="left" width="50%">
                    <Collapse defaultActiveKey={[1, 2]}>
                        <Panel header="节点1" key="1">
                            <p>{text}</p>
                        </Panel>
                        <Panel header="节点2" key="2">
                            <p>{text}</p>
                        </Panel>
                        <Panel header="节点2" key="2">
                            <p>{text}</p>
                        </Panel>
                        <Panel header="节点2" key="2">
                            <p>{text}</p>
                        </Panel>
                    </Collapse>
                </Sider>
                {/* 左侧内容结束 */}

                {/* 右侧内容开始 */}
                <Content className="right">
                    {/* 统计表 */}
                    <div className="statistic">
                        <ChartGauge title="节点使用情况" value={57} />
                        <ChartGauge title="节点使用情况" value={78} />
                        <ChartGauge title="节点使用情况" value={78} />
                        <ChartGauge title="节点使用情况" value={78} />
                    </div>
                </Content>
                {/* 右侧内容结束 */}
            </Layout>



        )
    }
}

export default Index