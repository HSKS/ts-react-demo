import { observable, action } from "mobx"

class Store {
    @observable selectStatus: any = 0
    @action setSelectStatus = (arr: Array<number>): void => {
        this.selectStatus = arr
    }
    @observable tableScrollHeight: number = 300
    @action setTableScrollHeight = (height: number): void => {
        this.tableScrollHeight = height
    }
}

export default new Store()