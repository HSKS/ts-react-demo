import {observable, action} from "mobx"
import request from '../../utils/request'
import apis from '../../config/apis'

class Store {
    @observable selectStatus: any = 0
    @action setSelectStatus = (arr: Array<number>): void => {
        this.selectStatus = arr
    }
    @observable tableScrollHeight: number = 300
    @action setTableScrollHeight = (height: number): void => {
        this.tableScrollHeight = height
    }

    @observable clusterList = {
        list: []
    }
    @action getClusterList = async () => {
        const resp = await request(apis.GetClusterList)
        // console.log('resp', resp)
        if (resp) {
            this.clusterList = resp
        }
        return resp
    }
}

export default new Store()