import React, { Component } from "react"
import { observer } from "mobx-react"
import store from "./store"
import "./styles.less"
import { Link } from "react-router-dom";
import { Table, Button, Layout, Space, Tag, PageHeader } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import formatDate from "../../utils/formatDate";
import routes from "../../config/routes";
import Container from '../../components/Container'


// const data: Array<object>  = []
// const columns: Array<object>  = [
//     {
//         title: '集群名称',
//         width: 180,
//         dataIndex: 'name',
//         key: 'name',
//         fixed: 'left',
//         render: (text: string) => <Link to={routes.cluster_details + '/1'} >{text}</Link>,
//     },
//     {
//         title: '标签',
//         dataIndex: 'tag',
//         key: 'tag',
//         width: 100,
//     },
//     {
//         title: '节点数量',
//         dataIndex: 'address',
//         key: '1',
//         width: 150,
//     },
//     {
//         title: '所属资源地',
//         dataIndex: 'address',
//         key: '2',
//         width: 150,
//     },
//     {
//         title: '创建人',
//         dataIndex: 'username',
//         key: '3',
//         width: 150,
//     },
//     {
//         title: '平台',
//         dataIndex: 'address',
//         key: '4',
//         width: 150,
//     },
//     {
//         title: '状态',
//         dataIndex: 'state',
//         key: '5',
//         width: 150,
//     },
//     {
//         title: '创建时间',
//         dataIndex: 'date',
//         key: '6',
//         width: 150
//     },
//     {
//         title: '操作',
//         key: 'operation',
//         fixed: 'right',
//         width: 100,
//         render: () => (
//             <Space>
//                 <a>编辑</a>
//                 <a>删除</a>
//             </Space>),
//     },
// ]


// // 集群状态
// const status = (num: number): any => {
//     switch (num) {
//         case 1:
//             return (<Tag color="green">运行中</Tag>);
//         case 2:
//             return (<Tag color="blue">发布中</Tag>);
//         case 3:
//             return (<Tag color="gray">已暂停</Tag>);
//         case 4:
//             return (<Tag color="orange">重启中</Tag>);
//         case 5:
//             return (<Tag color="yellow">正常退出</Tag>);
//         case 6:
//             return (<Tag color="red">异常退出</Tag>);
//         default:
//             return (<Tag color="geekblue">未知状态</Tag>);
//     }
// }

@observer
class Index extends Component {

    componentDidMount(): void {
        const { setTableScrollHeight } = store
        setTableScrollHeight(document.body.clientHeight - 264)
        store.getClusterList()
    }

    handleClick = () => {
        console.log('this.props', this.props)
        // this.props.history.push({
        //     pathname: routes.cluster_details + '/1',
        //     state: {...record}
        // })
    }

    render() {
        let { clusterList } = store
        let data = { ...clusterList }
        let list = data.list

        let columns: Array<object> = [
            {
                title: '集群名称',
                // width: 180,
                // width: 30,
                dataIndex: 'cluster_name',
                key: 'cluster_name',
                // fixed: 'left',
                render: (text: string, record: any) => <Link to={routes.cluster_details + '/' + record.cluster_label + '/' + text + '/' + record.created_at} >{text}</Link>
                // render: (text: string, record: any, index: number) => {
                //     console.log('本条记录主要信息为：', record)
                //     console.log('本条记录的顺序下标为：', index)
                //     return <Link to={{pathname: routes.cluster_details + '/1', state: { cluster: record }}} >{text}</Link>
                // }
                // render: (text: string, record: any, index: number) => <a onClick={() => this.handleClick} >{text}</a>
            },
            {
                title: '集群标签',
                dataIndex: 'cluster_label',
                key: 'cluster_label',
                // width: 50,
            },
            {
                title: '集群类型',
                dataIndex: 'cluster_type',
                key: 'cluster_type',
                // width: 50,
                render: (text: number) => {
                    switch (text) {
                        case 0:
                            return (<p>Kubernetes</p>)
                        case 1:
                            return (<p>KubeEdge</p>)
                        default:
                            return (<p>Kubernetes</p>)
                    }
                }
            },
            {
                title: '集群状态',
                dataIndex: 'status',
                key: 'status',
                // width: 50,
                render: (text: number) => {
                    switch (text) {
                        case 0:
                            return <p>未启用</p>
                        case 1:
                            return <p>启用中</p>
                        default:
                            return <p>未知状态</p>
                    }
                }
            },
            {
                title: '创建时间',
                dataIndex: 'created_at',
                key: 'created_at',
                // width: 50,
                render: (text: string) => <p>{formatDate(new Date(text))}</p>
            },
            // {
            //     title: '操作',
            //     key: 'operation',
            //     fixed: 'right',
            //     width: 100,
            //     render: () => (
            //         <Space>
            //         <a>编辑 < /a>
            //         < a > 删除 < /a>
            //         < /Space>),
            // },
        ]

        // for (let i = 1; i < 50; i++) {
        //     data.push({
        //         key: i,
        //         name: `服务器命名 ${i}`,
        //         tag: 'xs-text-k8s',
        //         platform:'',
        //         address: 'CORE',
        //         username: 'admin',
        //         a: 'Kubernetes',
        //         date: formatDate(new Date(), 'yyyy/MM/dd hh:mm:ss'),
        //         state: status(i)
        //     });
        // }
        return (
            <Container title='集群管理' className='cluster'>
                {/* 列表内容开始 */}
                <section className="content-cont">
                    {/* 按钮栏 */}
                    <Space className="cluster-btn" size='middle' >
                        <Button type="primary" icon={<PlusOutlined />}>创建集群</Button>
                    </Space>

                    {/* 列表 */}
                    <Table
                        columns={columns}
                        dataSource={list}
                        // scroll={{
                        //     // x: 1406,
                        //     y: store.tableScrollHeight
                        // }}
                    />
                </section>
                {/* 列表内容结束 */}
            </Container>
        )
    }
}

export default Index
