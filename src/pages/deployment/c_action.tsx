import React from "react"
import { Button, Popover, Space, Popconfirm, message } from 'antd'
import store from "./store"

//功能
const action = (value: any, total_status: string) => {
    const { app_id, namespace } = value
    // 删除
    const del = async () => {
        const data = await store.DeleteApp({ app_id, namespace })
        if (data) {
            message.success('已删除');
            store.getAppList()
        }
    }
    // 重启
    const restart = async () => {
        const data = await store.RestartApp({ app_id, namespace })
        if (data) {
            message.success('已重启');
            store.getAppList()
        }
    }
    // 暂停
    const stop = async () => {
        const data = await store.StopApp({ app_id, namespace })
        if (data) {
            message.success('已暂停');
            store.getAppList()
        }
    }
    // 启动
    const start = async () => {
        const data = await store.StartApp({ app_id, namespace })
        if (data) {
            message.success('已启动');
            store.getAppList()
        }
    }

    //判断状态
    const isStatus = (type: 'del' | 'restart' | 'stop' | 'start' | 'push' | 'back') => {
        //true表示可操作，false表示不可操作
        let status = {
            del: false,//删除
            restart: false,//重启
            stop: false,//暂停
            start: false,//启动
            push: false,//发布
            back: false,//退回
        }

        switch (total_status) {
            case 'PAUSE': // 暂停
                status.restart = true
                status.stop = true
                break;
            case 'SUCCESS': // 运行中
                status.del = true
                status.start = true
                break;
            case 'PENDING': // 升级中
                status.del = true
                status.restart = true
                status.start = true
                status.push = true
                status.back = true
                break;
        }

        return status[type]
    }

    return <Space>
        <Popconfirm
            title="确认删除吗？"
            onConfirm={del}
            okText="是"
            cancelText="否"
            disabled={isStatus('del')}
        >
            <a style={isStatus('del') ? { color: '#d9d9d9' } : {}}>删除</a>
        </Popconfirm>
        <Popconfirm
            title="确认重启吗？"
            onConfirm={restart}
            okText="是"
            cancelText="否"
            disabled={isStatus('restart')}
        >
            <a>重启</a>
        </Popconfirm>

        <Popover content={
            <div style={{ display: "flex", flexDirection: "column" }}>
                <Button disabled={isStatus('stop')} style={{ marginBottom: 8 }} onClick={() => stop()}>暂停</Button>
                <Button disabled={isStatus('start')} style={{ marginBottom: 8 }} onClick={() => start()}>启动</Button>
                <Button disabled={isStatus('push')} style={{ marginBottom: 8 }}>升级</Button>
                <Button disabled={isStatus('back')} style={{ marginBottom: 8 }}>回退</Button>
                <Button style={{ marginBottom: 8 }}>容量变更</Button>
                <Button style={{ marginBottom: 8 }}>应用迁移</Button>
                <Button style={{ marginBottom: 8 }} onClick={e => {

                    console.log(app_id, '===================');
                    store.getAppClusterList(app_id,namespace)
                    store.modalVisible = true
                }}>实例数变更</Button>
            </div>} title={null} trigger="click">
            <a>...</a>
        </Popover>
    </Space>;
}


export default action