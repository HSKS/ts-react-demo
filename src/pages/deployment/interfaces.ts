export interface AppInfo {
    id: number
    name: string
    app_id: string
    group_id: number
    username: string
    namespace: string
    replicas: number
    exp_replicas: number
    created_at: string
    status: string
}

export interface Cluster {
    id: number,
    cluster_label: string,
    cluster_name: string,
    cluster_type: number,
    master_url: string,
    status: number,
    area_id: number,
    created_at: string,
    updated_at: string
}

//应用实例数变更
export interface Clusters {
    cluster_label: string,
    replicas: number
}

//应用实例数变更
export interface AlterAppInstance {
    app_id: string
    namespace: string
    clusters: Array<Clusters>
}