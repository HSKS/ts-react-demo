import React, { Component } from "react"
import { observer } from "mobx-react"
import { Layout, Select, Table, Input, Button, Spin, Tag, InputNumber, Popover, Space, Popconfirm, message } from 'antd'
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { PlusOutlined } from '@ant-design/icons';
import store from "./store"
import "./styles.less"
import { Link } from "react-router-dom";
import routes from "../../config/routes";
import useInputValue from '../../utils/inputValue'
import formatDate from '../../utils/formatDate'
import Container from '../../components/Container'
import CModal from '../../components/CModal'
import CTable from "../../components/CTable";
import CAction from "./c_action";

//搜索
interface Search {
    onClick: (name: string) => void;
}

const Search: React.FC<Search> = ({ onClick }) => {
    const name = useInputValue('');

    return (
        <Space>
            <Input
                className='app-exp-input input'
                addonBefore='应用名称:'
                placeholder='请输入应用名称查询'
                {...name}
            />
            <Button>重置</Button>
            <Button type='primary' onClick={() => onClick(name.value)}>查询</Button>
        </Space>
    )
}


@observer
class Index extends Component<RouteComponentProps, {}> {
    componentDidMount(): void {
        const { setTableScrollHeight } = store
        setTableScrollHeight(document.body.clientHeight - 352)
        store.getAppList()
    }

    //状态展示样式
    totalStatus = (state: string) => {
        let color = ''
        let title = ''
        switch (state) {
            case 'SUCCESS':
                title = '运行中'
                color = 'success'
                break;
            case 'PENDING':
                title = '升级中'
                color = 'processing'
                break;
            case 'PAUSE':
                title = '暂停中'
                color = 'error'
                break;
            // case 'STOPPED':
            //     title = '已停止'
            //     color = 'default'
            //     break;
        }
        return <Tag color={color}>{title}</Tag>
    }




    render() {
        const { appList } = store
        return (
            <Container title='应用管理' className='deployment'>
                {/* 列表内容开始 */}
                <section className="content-cont">
                    {/* 按钮框 */}
                    <Space size='middle' className="content-input">
                        <Link to={routes.deployment_create} >
                            <Button type='primary' icon={<PlusOutlined />}>创建容器应用</Button>
                        </Link>
                        <Search onClick={(name) => store.getAppList({ app_name: name })} />
                    </Space>

                    {/* 列表 */}
                    <Spin delay={300} tip='加载中...' spinning={false}>
                        <Table columns={[
                            {
                                title: '应用名称',
                                width: 120,
                                dataIndex: 'name',
                                // fixed: 'left',
                                align: 'center',
                                render: (text, { app_id, namespace }) => <Link to={{ pathname: `${routes.deployment_details}/${namespace}/${app_id}` }} >{text}</Link>
                            },
                            {
                                title: 'APPID',
                                width: 120,
                                dataIndex: 'app_id',
                                // fixed: 'left',
                                align: 'center',
                                render: (text, { app_id, namespace }) => <Link to={{ pathname: `${routes.deployment_details}/${namespace}/${app_id}` }} >{text}</Link>
                            },
                            {
                                title: '业务系统',
                                width: 120,
                                dataIndex: 'namespace',
                                align: 'center',
                            },
                            {
                                title: '实例数（运行/总数）',
                                width: 170,
                                dataIndex: 'ready',
                                align: 'center',
                                render: (text, { replicas, exp_replicas }) => replicas > exp_replicas ? replicas : `${replicas}/${exp_replicas}`
                            },
                            {
                                title: '创建用户',
                                width: 150,
                                dataIndex: 'username',
                                align: 'center',
                            },
                            {
                                title: '运行状态',
                                width: 130,
                                dataIndex: 'status',
                                align: 'center',
                                render: (text) => this.totalStatus(text)
                            },
                            {
                                title: '创建时间',
                                width: 160,
                                dataIndex: 'created_at',
                                align: 'center',
                                render: (date) => formatDate(new Date(date))
                            },
                            {
                                title: '操作',
                                // fixed: 'right',
                                width: 150,
                                align: 'center',
                                render: (val, { status }) => CAction(val, status)
                            },
                        ]} dataSource={[...appList]}
                            scroll={{ x: 1120 }}
                            rowKey={(record: any) => record.app_id}
                        />
                    </Spin>
                </section>
                {/* 列表内容结束 */}



                {/* ========================= 弹出框 ======================== */}
                {/* 实例数变更 */}
                <CModal title='实例数变更' width='600px' visible={store.modalVisible} setVisible={visible => store.modalVisible = visible} onClick={() => {
                    const clusters = store.appInstanceList.map(item => item)
                    store.appInstance.clusters = clusters
                    const data = store.setAlterAppInstance({ ...store.appInstance })
                    if (data) {
                        message.success('已变更');
                        store.appInstanceList = []
                        store.getAppList()
                    }
                }}>
                    <CTable title='标题' columns={[{
                        title: '名称',
                        key: 'id',
                        dataIndex: 'cluster_name',
                        align: 'center',
                        render: (val) => val
                    }, {
                        title: '旧实例数',
                        key: 'id',
                        dataIndex: 'replicas',
                        align: 'center',
                        render: (val) => val
                    }, {
                        title: '新实例数',
                        key: 'id',
                        dataIndex: 'replicas',
                        align: 'center',
                        width: 120,
                        render: (val, { cluster_label }, idx) => (
                            <InputNumber value={val} min={1} onChange={v => {
                                store.appInstanceList[idx] = {
                                    cluster_label,
                                    replicas: v as number
                                }
                            }} />
                        )
                    }]} dataSource={[...store.appClusterList]} pagination={false} hideTitle={true} />
                </CModal>
            </Container >
        )
    }
}

export default withRouter(Index as any)

