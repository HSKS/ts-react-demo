import { observable, action } from "mobx"
import request from '../../utils/request'
import apis from '../../config/apis'
import { AppInfo, Cluster, Clusters, AlterAppInstance } from './interfaces'

class Store {
    @observable modalVisible: boolean = false
    @observable selectStatus: any = 0
    @action setSelectStatus = (arr: Array<number>): void => {
        this.selectStatus = arr
    }
    @observable tableScrollHeight: number = 300
    @action setTableScrollHeight = (height: number): void => {
        this.tableScrollHeight = height
    }
    // 应用列表
    @observable total: number = 0
    @observable current: number = 1
    @observable pageSize: number = 10
    @observable appList: Array<AppInfo> = []
    @action getAppList = async (params = { app_name: '' }) => {
        const data = await request(apis.GetAppList, params)
        if (!data) return
        this.appList = data.list as Array<AppInfo>
        this.appList = [...this.appList]
        return data
    }

    //获取集群
    @observable appClusterList: Array<Cluster> = []
    @observable appInstanceList: Array<Clusters> = []
    @observable appInstance = {} as AlterAppInstance
    @action getAppClusterList = async (app_id: string, namespace: string) => {
        const data: Array<Cluster> = await request(apis.GetAppClusterList, { app_id })
        if (!data) return
        this.appClusterList = data as Array<Cluster>
        this.appInstance.app_id = app_id
        this.appInstance.namespace = namespace
        this.appInstanceList = data.map(({ cluster_label }) => ({ cluster_label, replicas: 1 }))
    }

    // 应用启动
    @action setAlterAppInstance = async (params: AlterAppInstance) => {
        const data = await request(apis.AlterAppInstance, params)
        if (!data) return
        return data
    }

    // 应用删除
    @action DeleteApp = async (params: Object) => {
        const data = await request(apis.DeleteApp, params)
        if (!data) return
        return data
    }

    // 应用重启
    @action RestartApp = async (params: Object) => {
        const data = await request(apis.RestartApp, params)
        if (!data) return
        return data
    }

    // 应用停止
    @action StopApp = async (params: Object) => {
        const data = await request(apis.StopApp, params)
        if (!data) return
        return data
    }

    // 应用启动
    @action StartApp = async (params: Object) => {
        const data = await request(apis.StartApp, params)
        if (!data) return
        return data
    }
}

export default new Store()