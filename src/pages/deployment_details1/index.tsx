import React, {Component} from "react"
import {observer} from "mobx-react"
import {Select, Tabs} from 'antd'
import {RouteComponentProps} from 'react-router-dom';
import Container from '../../components/Container'
import "./styles.less"
import PodList from './podList'
import HaList from './haList'

const {Option} = Select;
const {TabPane} = Tabs;

interface InfoKV {
    title: string
    value: string
}

const InfoKV: React.FC<InfoKV> = ({title, value}) => {
    return (
        <div className='info-kv'>
            <label className='label'>{title}：</label>
            <span className='input'>{value}</span>
        </div>
    )
}

@observer
class Index extends Component<RouteComponentProps, {}> {
    componentDidMount(): void {
    }

    render() {
        return (
            <Container title='应用详情' className='deployment-details1'>
                <div className='top'>
                    <div className='title'>
                        <span className='title-text'>基本信息</span>
                    </div>
                    <div className='row'>
                        <InfoKV title='业务系统' value={'实例'}/>
                        <InfoKV title='部署区域' value={'杭州市'}/>
                    </div>
                    <div className='row'>
                        <InfoKV title='APPID' value={'ff77ff'}/>
                        <InfoKV title='部署集群' value={'三墩集群'}/>
                    </div>
                    <div className='row'>
                        <InfoKV title='节点亲和' value={'tag1'}/>
                        <InfoKV title='创建时间' value={'2019-02-14 11:44:57'}/>
                    </div>
                    <div className='row'>
                        <InfoKV title='应用描述' value={'应用描述飞洒发打赏范德萨法撒旦的萨芬的萨芬发射点范德萨分发大水范德萨分发生范德萨发生范德萨分发生范德萨范德萨发大水范德萨'}/>
                        <InfoKV title='更新时间' value={'2019-02-14 11:44:57'}/>
                    </div>
                </div>
                <Tabs defaultActiveKey="1">
                    <TabPane tab="实例列表" key="1">
                        <PodList/>
                    </TabPane>
                    <TabPane tab="负载均衡" key="2">
                        <HaList/>
                    </TabPane>
                    <TabPane tab="监控" key="3">
                        Content of Tab Pane 3
                    </TabPane>
                    <TabPane tab="弹性策略" key="4">
                        Content of Tab Pane 4
                    </TabPane>
                    <TabPane tab="参数配置" key="5">
                        Content of Tab Pane 5
                    </TabPane>
                </Tabs>
            </Container>
        )
    }
}

// @ts-ignore
export default Index;