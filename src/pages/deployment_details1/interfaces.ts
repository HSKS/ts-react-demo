export interface TableColumns {

}

export interface PodInfo {
    name: string
    namespace: string
    cpu: number
    mem: number
    resource_group: string
    created_at: string
    status: string
}