import React, {Component} from "react"
import {observer} from "mobx-react"
import {Table} from 'antd'
import "./podListStyles.less"
import store from "./store";
import formatDate from "../../utils/formatDate";

@observer
class Index extends Component {
    componentDidMount(): void {
    }

    render() {
        const {podList} = store
        return (
            <Table columns={[
                {
                    title: '实例名称/ID',
                    dataIndex: 'name',
                    key: 'name',
                }, {
                    title: '资源组',
                    dataIndex: 'resource_group',
                    key: 'resource_group',
                }, {
                    title: '状态',
                    dataIndex: 'status',
                    key: 'status',
                },{
                    title: '资源用量',
                    dataIndex: 'cpu',
                    key: 'cpu',
                    render: (text, rcd)=>{
                        return `${rcd.cpu}Core/${(rcd.mem/1024).toFixed(2)}Gi`
                    }
                },{
                    title: '创建时间',
                    dataIndex: 'created_at',
                    key: 'created_at',
                    render: (text)=>{
                        return formatDate(new Date(text))
                    }
                },{
                    title: '操作',
                    dataIndex: 'action',
                    key: 'action',
                },
            ]} dataSource={podList}/>
        )
    }
}

export default Index;
