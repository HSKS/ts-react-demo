import {action, observable} from "mobx"
import {PodInfo} from './interfaces'
import request from '../../utils/request'
import apis from '../../config/apis'

class Store {
    @observable podList = new Array<PodInfo>()
}

export default new Store()