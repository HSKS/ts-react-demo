import React, {Component} from "react"
import {observer} from "mobx-react"
import {Table} from 'antd'
import "./podListStyles.less"
import formatDate from "../../utils/formatDate";


@observer
class Index extends Component {
    componentDidMount(): void {
    }

    render() {
        return (
            <Table columns={[
                {
                    title: '集群名称',
                    dataIndex: 'cluster_name',
                    key: 'cluster_name',
                }, {
                    title: '协议',
                    dataIndex: 'protocol',
                    key: 'protocol',
                }, {
                    title: 'IP',
                    dataIndex: 'ip',
                    key: 'ip',
                }, {
                    title: '端口号',
                    dataIndex: 'port',
                    key: 'port',
                }, {
                    title: '创建时间',
                    dataIndex: 'created_at',
                    key: 'created_at',
                    render: text =>{
                        return formatDate(new Date(text))
                    }
                }, {
                    title: '操作',
                    dataIndex: 'action',
                    key: 'action',
                },
            ]} dataSource={[]}/>
        )
    }
}

export default Index;
