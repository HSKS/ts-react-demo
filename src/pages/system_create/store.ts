import {observable, action} from "mobx"
import { CreateSystem } from './interfaces'
import request from '../../utils/request'
import apis from '../../config/apis'
import {Domain} from "../system/interfaces";
import {CascaderOptionType} from "antd/lib/cascader";

class Store {
    @observable name_cn:undefined|string = undefined //应用ID
    @observable name_en:undefined|string = undefined //区域
    @observable  domain_id = new Array<number>()//域ID
    @observable is_private = 1 //资源组
    // @observable private_user = ''
    // @observable private_pass = ''
    @observable describe = ''//描述
    @observable cpu = 0 //应用名称
    @observable mem = 0//mem
    @observable disk = 0 //实例数
    @observable gpu_high = 0//gpu
    @observable gpu_mid = 0
    @observable gpu_low = 0

    

    @action submit = async () => {
        const api = apis.CreateNamespace
        const params = {
            name_cn: this.name_cn,
            name_en: this.name_en,
            domain_id: this.domain_id[this.domain_id.length-1],
            is_private: this.is_private,
            // private_user: this.private_user,
            // private_pass: this.private_pass,
            describe: this.describe,
            cpu: this.cpu,
            mem: this.mem,
            disk: this.disk,
            gpu_high: this.gpu_high,
            gpu_mid: this.gpu_mid,
            gpu_low: this.gpu_low
        }
        const resp = await request(api, params)
        if (!resp) return false
        return true
    }

    buildCascaderOptions = (arr: Domain[] | undefined): CascaderOptionType[] | undefined => {
        if (arr && arr.length) {
            return arr.map(v => {
                return {
                    label: v.domain_name,
                    value: v.id,
                    children: this.buildCascaderOptions(v.Child)
                } as CascaderOptionType
            })
        } else {
            return undefined
        }
    }

    @observable domains: CascaderOptionType[] | undefined = new Array<CascaderOptionType>()
    @action GetDomain = async () => {
        const api = apis.GetDomain
        const resp = await request(api)
        if (!resp) return
        let data = resp as Domain[]
        let dm = this.buildCascaderOptions(data)
        console.log(dm)
        this.domains = dm
    }

}

export default new Store()