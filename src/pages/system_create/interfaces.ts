export interface CreateSystem {
    name_cn: string //应用ID
    name_en: string //区域
    domain_id: number //集群
    is_private: number //资源组
    private_user: string
    private_pass: string
    describe: string//描述
    cpu: number //应用名称
    mem: number//mem
    disk: number //实例数
    gpu_high: number//gpu
    gpu_mid: number
    gpu_low: number
}
