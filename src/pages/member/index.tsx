import React, {Component} from "react"

import {observer} from "mobx-react"

import {Button, Input, Space, Spin, Table, Modal} from 'antd'
import { RouteComponentProps, withRouter} from 'react-router-dom';
import {PlusOutlined} from '@ant-design/icons';
import "./styles.less"
import store from "./store";
import Container from '../../components/Container'
import CButton from "../../components/CButton";
import MultiSelect from "../../components/MultiSelect";
import ManySelect from '../../components/ManySelect'
import OneSelect from '../../components/OneSelect'

//搜索
interface Search {
    real_name: string | undefined
    setMirrorName: (val: string) => void
    setHubName: (val: string) => void
    onClick: () => void
    onClear: () => void
}


const Search: React.FC<Search> = ({onClick, real_name, setMirrorName, onClear}) => {
    return (
        <Space>
            <Input
                className='app-exp-input input'
                addonBefore='用户名称：'
                placeholder='请输入'
                value={real_name}
                onChange={(e) => {
                    setMirrorName(e.target.value)
                }}
            />
            <CButton className='app-but' type='primary' text='查询' onClick={() => onClick()}/>
            <CButton className='app-reset' text='重置' onClick={() => onClear()}/>
        </Space>
    )
}

interface IParams {
    namespace_id: string
}

@observer
class Index extends Component<RouteComponentProps, {}> {
    componentDidMount(): void {
        const {namespace_id} = this.props.match.params as IParams
        try {
            store.namespace_id = parseInt(namespace_id)
            store.getNamespaceMemberList()
            store.NamespaceGroupList()
            // store.GetGroupMemberList()
        } catch (e) {
            console.log(e)
        }
    }

    handleRemove = (userId: number) => { // 展开移除界面
        store.remove = true
        store.newAdd = false
        store.deleteUserID = userId
    };

    handleOff = () => { // 移除界面取消
        store.remove = false
        store.newAdd = false
        store.deleteUserID = 0
    };

    handleAdd = () => { // 展开新增界面
        store.remove = false
        store.newAdd = true
    };


    render() {
        const {real_name, memberList, count, getRemove, groupMembers, remove, NamespaceAddMembers, newAdd, NamespaceGroupList, lessee, lesseeNumber, customer, customerNumber, roleNumber, role} = store
        return (
            <Container title='成员管理' className='image-page'>
                <section className="content-cont">
                    <Button type='primary' style={{marginBottom: 20}} icon={<PlusOutlined/>} onClick={() => {
                        store.newAdd = true
                    }}>新增成员</Button>
                    <Space size='middle' className="content-input">
                        <Search
                            real_name={real_name}
                            onClick={() => {
                                // store.page_num = 1
                                store.getNamespaceMemberList()
                            }}
                            onClear={() => {
                                store.real_name = ''

                            }}
                            setMirrorName={(val) => {
                                console.log(val)
                            }}
                            setHubName={(val) => {
                                store.real_name = val

                            }}/>

                    </Space>

                    {/* 列表 */}

                    <Spin delay={300} tip='加载中...' spinning={false}>

                        <Table columns={[
                            {
                                title: '序号',
                                width: 150,
                                dataIndex: 'name',
                                fixed: 'left',
                                align: 'center',
                            },
                            {
                                title: '用户名称',
                                dataIndex: 'real_name',
                                align: 'center',
                            }, {
                                title: '所属角色',
                                dataIndex: 'ns_role',
                                align: 'center',

                            },
                            {
                                title: '联系电话',
                                dataIndex: 'phone',
                                align: 'center',
                            },
                            {
                                title: '操作',
                                align: 'center',
                                render: (text, {user_id}) => (<div><a onClick={()=>{
                                    this.handleRemove(user_id)
                                }}>移除</a> <a>设为负责人</a></div>
                                )
                            }
                        ]}
                               dataSource={[...memberList]}

                               rowKey={(record: any) => record.id}

                               pagination={{pageSize: 10, defaultCurrent: 1, total: count}}

                            //    pagination={{

                            //        current: page_num, // 当前页数

                            //        pageSize: page_size,

                            //        total: total,

                            //    }}

                            //    onChange={this.tableChange}

                        />

                    </Spin>

                </section>
                <Modal
                    visible={remove}
                    onOk={getRemove}
                    onCancel={this.handleOff}
                    okText='确认'
                    cancelText='取消'
                >
                    <h3>移除成员</h3>
                    <p>您确定移除该用户吗？移除后，该用户将不能继续访问此业务系统</p>
                </Modal>
                <Modal

                >
                </Modal>
                <Modal
                    title='新增成员'
                    visible={newAdd}// 控制modal的状态true为展开false为关闭
                    onOk={NamespaceAddMembers}//点击确认按钮
                    onCancel={this.handleOff}//取消按钮
                    okText='确认'
                    cancelText='取消'
                >
                    <MultiSelect titleWidth={140} title='所属租户' data={lessee} value={lesseeNumber}
                                 required={lesseeNumber !== null}
                                 onChange={(val) => {
                                     store.lesseeNumber = val
                                 }}/>
                    <ManySelect titleWidth={140} title='选择用户' data={groupMembers} value={customerNumber}
                                 required={customerNumber !== null}
                                 onChange={(val) => {
                                     store.customerNumber = val
                                 }}/>
                    <MultiSelect titleWidth={140} title='选择用户' data={role} value={roleNumber}
                                 required={roleNumber !== null}
                                 onChange={(val) => {
                                     store.roleNumber = val
                                 }}/>
                </Modal>

                {/* 列表内容结束 */}

            </Container>


        )


    }


}


export default withRouter(Index as any);