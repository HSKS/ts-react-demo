import {observable, action} from "mobx"
import {TableColumns, GroupMembers} from './interfaces'
import request from '../../utils/request'
import apis from '../../config/apis'
import {IManySelectData} from '../../components/ManySelect'
class Store {
    @observable namespace_id: number | undefined = undefined
    @observable real_name: string | undefined  = undefined
    @observable ns_role: string | undefined = undefined
    @observable user_id: string | undefined = undefined
    @observable group_id: number | undefined = undefined
    @observable phone : string | undefined = undefined
    @observable memberList : any = []
    @observable remove : boolean = false
    @observable principal : boolean = false
    @observable newAdd : boolean = false
    @observable lessee : any = []
    @observable role : any = []
    @observable roleNumber : number = NaN
    @observable lesseeNumber : number = NaN
    @observable customer : any = []
    @observable customerNumber : any = []
    @observable page_num = 1
    @observable page_size = 20
    @observable count = 0
    @observable namespace : string = ''
    @action getNamespaceMemberList = async () => {
        const api = apis.NamespaceMemberList // 接口
        const params = { // 参数
            namespace_id: this.namespace_id,
            real_name: this.real_name,
            page_num: this.page_num,
            page_size: this.page_size
        }        
        const data = await request(api, params) // 接口路径+参数  data是后端返回结果
        if (!data) return // 返回错误 不再进行进一步操作
        console.log(data)
        this.count = data.count
        this.memberList = data.member_list as Array<TableColumns>
    }
    deleteUserID = 0
    @action getRemove = async () => {
        const api = apis.NamespaceDeleteMember
        const params = {
            user_id: this.deleteUserID,
        }
        const data = await request(api, params) // 接口路径+参数  data是后端返回结果
        if (!data) return // 返回错误 不再进行进一步操作
        this.remove=false
    }
    @action NamespaceGroupList = async () => {
        const api = apis.NamespaceGroupList
        const params = {
        }
        const data = await request(api, params) // 接口路径+参数  data是后端返回结果
        if (!data) return // 返回错误 不再进行进一步操作
        this.lessee=data
        console.log("grouplist ===>>> ", data)
        // this.lesseeNumber=data[0].user_id
        // this.NamespaceUserList()
    }
    @action NamespaceUserList = async () => {
        const api = apis.NamespaceUserList
        const params = {
            group_id: this.group_id,
        }
        const data = await request(api,params) // 接口路径+参数  data是后端返回结果
        if (!data) return // 返回错误 不再进行进一步操作
        this.customer=data
        console.log("用户 ===》》》 ",data)
    }
    @action NamespaceRoleList = async () => {
        const api = apis.NamespaceRoleList
        const data = await request(api) // 接口路径+参数  data是后端返回结果
        if (!data) return // 返回错误 不再进行进一步操作
        this.role=data
        console.log(data)
    }
    
    @action NamespaceAddMembers = async () => {
        const api = apis.NamespaceAddMembers
        const params = {
            namespace_id: this.namespace_id	,
            namespace: this.namespace,
            member_list: this.customerNumber,
            ns_role_id: this.roleNumber,
        }
        const data = await request(api,params) // 接口路径+参数  data是后端返回结果
        if (!data) return // 返回错误 不再进行进一步操作
        this.newAdd=false
    }
    @observable groupMembers = new Array<IManySelectData>()
    @action GetGroupMemberList = async () => {
        // const api = apis.GetGroupMemberList
        // const resp: Array<GroupMembers> = await request(api)
        // if (!resp) return
        // this.groupMembers = new Array<IManySelectData>()
        // for (let i = 0;i < resp.length; i++) {
        //     const val = resp[i]
        //     this.groupMembers.push({
        //         label: val.real_name,
        //         value: val.id
        //     })
        // }

    }
}

export default new Store()