export interface TableColumns {
    namespace_id:number
    real_name:string
    group_id:number
    namespace:string
    member_list:number
    ns_role_id:number
}

export interface GroupMembers {
    id: number
    username: string
    real_name: string
}