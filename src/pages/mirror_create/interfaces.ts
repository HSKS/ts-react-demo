export interface MirrorInfo {
    mirror_address:string
    mirror_name:string
    tag_name:string
    size:string
    hub_name:string
    desc:string
}

export interface HubInfo {
    desc: string
    hub_address: string
    id: number
    is_private: number
    name: string
    namespace_id: number
    remark: string
}
