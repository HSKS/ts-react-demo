import {observable, action} from "mobx"
import request from '../../utils/request'
import apis from '../../config/apis'
import {IOneSelectData} from '../../components/OneSelect'
import {HubInfo} from './interfaces'

class Store {
    @observable domain_id = new Array<number>()//域ID
    // @observable is_private = 1 //资源组
    // @observable private_user = ''
    // @observable private_pass = ''
    @observable describe = ''//描述
    @observable mirror_address = ''
    @observable mirror_name = ''
    @observable tag_name = ''
    @observable size = ''
    @observable hub_name = ''
    @observable desc = ''
    @observable mirrorArray: Array<IOneSelectData> = []

    @action HubList = async () => {
        const api = apis.HubList
        const data:Array<HubInfo> = await request(api)
        if (!data) return
        this.mirrorArray = []
        for (let item of data) {
            this.mirrorArray.push({
                label: item.name,
                value: item.name
            })
        }
    }

    @action submit = async () => {
        
    }
}

export default new Store()