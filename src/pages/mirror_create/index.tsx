import React, {Component} from "react"
import {observer} from "mobx-react"
import store from './store'
import {RouteComponentProps, withRouter} from 'react-router-dom';
import "./styles.less"
import TextAreaInput from "../../components/TextAreaInput";
import MultiSelect from "../../components/MultiSelect";
import CButton from "../../components/CButton";
import {Row, Col, Spin, Table, } from "antd";
import Container from '../../components/Container'
import {Upload, message, Button,Space} from 'antd';
import {UploadOutlined} from '@ant-design/icons';
import apis from '../../config/apis'
import host from '../../config/host'
import {MirrorInfo} from './interfaces'
import {RcFile, UploadChangeParam} from "antd/lib/upload/interface";

@observer
class Index extends Component<RouteComponentProps, {}> {
    componentDidMount() {
        // store.GetDomain()
        store.HubList()
    }

    render() {
        const {mirrorArray, hub_name, desc,} = store
 
        const props = {
            name: 'file',
            multiple: false,
            action: host + apis.upload.url,
            headers: {
                token: localStorage.getItem("token") as string
            },
            data: {
                hub_name:hub_name.toString()
                
            },
            

            beforeUpload(file: RcFile, FileList: RcFile[]): boolean | PromiseLike<void> {
                if (!hub_name) {
                    message.warning('请选择镜像仓库')
                    return false
                }
                if (!file.name.endsWith('.tar') && !file.name.endsWith('.tar.gz')) {
                    message.warning('文件类型必须为tar或tar.gz格式')
                    return false
                }
                return true
            },
            onChange(info: UploadChangeParam) {
                if (info.file.status === 'done') {
                    console.log(info)
                    message.success(`镜像 ${info.file.name} 上传成功`);
                } else if (info.file.status === 'error') {
                    console.log(info)
                    message.error(`镜像 ${info.file.name} 上传失败`);
                }
            },
        };
        return (
            <Container title='创建镜像' className='image-page'>
                <div className='content-cont'>

                    <MultiSelect titleWidth={140} title='镜像仓库' data={mirrorArray} value={hub_name} onChange={(val) => {
                        store.hub_name = val
                    }}/>
                    <TextAreaInput titleWidth={140} title='镜像描述' placeholder='选填，请输入镜像描述' value={desc}
                                   onChange={(val) => {
                                       store.desc = val
                                   }}/>
                </div>
                <Upload {...props}>
                    <Button icon={<UploadOutlined/>}>上传</Button>
                </Upload>
                <div className='annotation'>您还可以添加10个镜像压缩包，文件大小（含压缩后）不得超过2GB，支持tar、tar.gz格式，
                    仅支持上传1.11.2及以上容器引擎客户端版本制作的镜像压缩包，制作过程详见<a>如何创建镜像文件</a></div>
                    {/* <CButton className='app-but' type='primary' text='提交' onClick={() => onClick()}/> */}
            </Container>
            
        )
    }
}


export default withRouter(Index as any);