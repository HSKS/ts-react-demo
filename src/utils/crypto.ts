import CryptoJS, {AES} from 'crypto-js';
/*AES/CBC/PKCS7PADDING*/
/*aes 加密*/
export function aesEn(key: string, data: string): string {
    const aesKey = CryptoJS.enc.Utf8.parse(key)
    const encryptedDataByte = AES.encrypt(data, aesKey, {
        iv: aesKey,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
    })
    return encryptedDataByte.ciphertext.toString()
}
/*aes 解密*/
export function aesDe(key: string, encryptedData: string): string {
    const aesKey = CryptoJS.enc.Utf8.parse(key)
    const encryptedHexStr = CryptoJS.enc.Hex.parse(encryptedData)
    const encryptedBase64Str = CryptoJS.enc.Base64.stringify(encryptedHexStr)
    let decryptedData = AES.decrypt(encryptedBase64Str, aesKey, {
        iv: aesKey,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
    });
    return decryptedData.toString(CryptoJS.enc.Utf8)
}
