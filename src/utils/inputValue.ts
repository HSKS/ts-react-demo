import { isObject } from "util";

let { useState, useCallback } = require('react');
interface InputValue<T> {
    value: T,
    onChange: (event: React.ChangeEvent<HTMLInputElement> | number | string | undefined) => undefined,
}


export default function useInputValue<T>(initialValue: T, callback?: () => void): InputValue<T> {
    const [value, setValue] = useState(initialValue);
    const onChange = useCallback(function (e: any) {
        if (typeof e !== 'object') setValue(e);
        else setValue(e.currentTarget.value);
        if (callback) callback();
    }, []);

    return {
        value,
        onChange,
    };
};


