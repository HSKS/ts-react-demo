export default function formatDate(date: Date, fmt: string = "yyyy-MM-dd hh:mm:ss"): string {
    const o = {
        "M+": date.getMonth() + 1, //month
        "d+": date.getDate(),    //day
        "h+": date.getHours(),   //hour
        "m+": date.getMinutes(), //minute
        "s+": date.getSeconds(), //second
        "q+": Math.floor((date.getMonth() + 3) / 3),  //quarter
        "S": date.getMilliseconds() //millisecond
    }
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1,
        (date.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (let k in o) {
        k = k as string
        if (new RegExp("(" + k + ")").test(fmt)) {
            // @ts-ignore
            const val = o[k]
            fmt = fmt.replace(RegExp.$1,
                RegExp.$1.length === 1 ? val :
                    ("00" + val).substr(("" + val).length));
        }
    }
    return fmt;
}