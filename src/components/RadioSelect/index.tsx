import React, {FC} from "react"
import {Radio} from 'antd'
import "./index.less"
import ContainerRow from "../ContainerRow";

export interface IRadioSelectData {
    label: string
    value: any
    [prop: string]: any
}

interface IRadioSelect {
    title: string
    titleWidth?: number
    data: Array<IRadioSelectData>
    value: any
    onChange: (val: any, label: string) => void
    style?: React.CSSProperties | undefined
    required?: boolean | undefined
}

const RadioSelect: FC<IRadioSelect> = ({title, required, style,titleWidth, data, value, onChange}) => {
    if (!titleWidth) titleWidth = 120
    return (
        <ContainerRow title={title} titleWidth={titleWidth} required={required}>
            <Radio.Group value={value} onChange={(e)=>{
                let val = e.target.value
                let lb = ''
                for (let i = 0;i < data.length;i++) {
                    if (data[i].value === val) {
                        lb = data[i].label
                        break
                    }
                }
                onChange(val, lb)
            }}>
                {
                    data.map((item => (
                        <Radio value={item.value}>{item.label}</Radio>
                    )))
                }
            </Radio.Group>
        </ContainerRow>
    )
}


export default RadioSelect;