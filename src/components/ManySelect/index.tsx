import React, { FC } from "react"
import { Select } from 'antd'
import "./index.less"
import ContainerRow from '../ContainerRow'
const { Option } = Select;

export interface IManySelectData {
    label: string
    value: any
    [prop: string]: any
}

interface IManySelect {
    title: string
    titleWidth?: number
    placeholder?: string
    data: Array<IManySelectData>
    value: any
    onChange: (val: any) => void
    style?: React.CSSProperties | undefined
    required?: boolean
}

const ManySelect: FC<IManySelect> = ({ title, style, required, titleWidth, data, placeholder, value, onChange }) => {
    if (!titleWidth) titleWidth = 120
    if (!placeholder) placeholder = '请选择'
    return (
        <ContainerRow title={title} required={required} style={style}>
            <Select className='input-com' value={value} placeholder={placeholder}
                mode="multiple"
                onChange={onChange}
                optionLabelProp="label"
            >
                {
                    data.map((item) => (<Option key={item.value} value={item.value} label={item.label}>
                        {item.label}
                    </Option>))
                }

            </Select>,
        </ContainerRow>
    )
}


export default ManySelect;