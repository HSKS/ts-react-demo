import React, { useState, useEffect } from "react"
import {
    Slider,
    InputNumber,
    Row,
    Col,
} from 'antd'

interface SliderAttr {
    min: number,
    max: number,
    unit?: string,//单位
    message?: number, //默认参数
    evts(value: number): void,
}



// 弹性策略 - 数字滑块
const NumberSlider: React.FC<SliderAttr> = ({ message, min, max, unit, evts }) => {
    const attr = {
        min, max, value: message,
        onChange: (num: any) => {
            if (num >= min && num <= max) evts(num)
        }
    }

    return (
        <Row>
            <Col span={12}>
                <Slider {...attr} />
            </Col>
            <Col span={4} style={{ whiteSpace: 'nowrap' }}>
                <InputNumber {...attr} style={{ margin: '0 16px' }} />{unit}
            </Col>
        </Row>
    )
}


export default NumberSlider;