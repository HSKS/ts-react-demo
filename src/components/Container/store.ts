import { observable, action } from "mobx"

interface Route {
    path: string;
    breadcrumbName: string;
    children?: Array<{
        path: string;
        breadcrumbName: string;
    }>;
}

class Store {

    //面包屑导航
    @observable routeLists: Array<Route> = [];
    @action setRoutesFore = (route: Route): void => {
        //是否有重复路由，有则不添加
        const isRoute = this.routeLists.filter(({ breadcrumbName }) => breadcrumbName === route.breadcrumbName)
        if (!isRoute.length) this.routeLists = [...this.routeLists,route]
    }
    @action setRoutesBack = (): void => {

        // this.routeLists = [...this.routeLists,routeName]
    }


}

export default new Store()


