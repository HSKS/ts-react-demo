import React, {Component} from 'react'
import {observer} from 'mobx-react'
import {Link} from 'react-router-dom'
import {Breadcrumb} from 'antd';
import routes from '../../config/routes'
import './styles.less'
import store from './store';

interface IComponent {
    title: string//标题名
    subTitle?: string
    className?: string
    options?: any
}

/**
 * 内容盒子
 */
@observer
class Index extends Component<IComponent> {

    componentWillMount() {
        if (!this.props.className) return;
        const {routeLists, setRoutesFore} = store;
        // console.log(location.pathname,this.props, routes[this.props.className]);
        setRoutesFore({
            path: routes[this.props.className],
            breadcrumbName: this.props.title
        })
    }

    render() {

        const {children, className, title, subTitle, options} = this.props;
        return (
            <div className='c-container'>
                {/* 面包屑开始 */}
                {/* <Breadcrumb separator=">" className='container-nav'
                    routes={routeLists}
                    itemRender={({ path, breadcrumbName }) => {
                        return <Breadcrumb.Item>
                            <Link to={path}>{breadcrumbName}</Link>
                        </Breadcrumb.Item>
                    }}>
                </Breadcrumb> */}
                {/* 面包屑结束 */}

                {/* 内容面板开始 */}
                <div className='c-container-content'>
                    {/* 标题 */}

                    <div className='c-title'>
                        <h3 className='title'>{title} <span
                            style={{fontSize: 14, fontWeight: "normal"}}>{subTitle}</span></h3>
                        {options}
                    </div>

                    {/* 内容 */}
                    <div className={className}>{children}</div>
                </div>
                {/* 内容面板结束 */}

            </div>
        )
    }
}


export default Index
