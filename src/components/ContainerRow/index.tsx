import React, { Component } from 'react'
import { observer } from 'mobx-react'
import './styles.less'

/**
 * 公用input样式
 */

interface Attr {
    title: string | undefined
    titleWidth?: string | number
    inputWidth?: string
    required?: boolean //false：验证不通过    true:通过
    className?: string
    error?: string  //错误信息
    style?: React.CSSProperties | undefined
}

@observer
class Index extends Component<Attr> {
    constructor(props: any) {
        super(props);
    }

    render() {
        const { children, required, title, className, titleWidth, inputWidth, error, style } = this.props
        let is_required: boolean = false;//展示小星星
        if ('required' in this.props) is_required = true;
        const containerClass = className ? `container-row ${className}` : "container-row"
        const titleClass = is_required ? 'title-label required' : 'title-label';
        const contClass = (!required && is_required) ? "container-cont error" : "container-cont";

        return (
            // paralleling  合并行clss 加上这句title会变成 position: absolute;
            <div className={containerClass} style={style}>
                {/* 标题 */}
                <div className="container-title" style={{ width: titleWidth }}>
                    <span className={titleClass}>{title}：</span>
                </div>
                {/* 内容 */}
                <div className={contClass} style={{ width: inputWidth }}>
                    {children && children}
                    {(!required && is_required) && (<span className='error-text'>{error || '内容不可为空'}</span>)}
                </div>
            </div>
        )
    }
}


export default Index
