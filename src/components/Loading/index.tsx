import React, { useState } from 'react'
import './styles.less'

/**
 * 加载中
 */

interface ISwitch {
    show: boolean
}

const Loading: React.FC<ISwitch> = ({ show }) => {
    return (
        <div className={show ? 'loading active' : 'loading'}>
            <img src={require('../../assets/Loading.gif')} />
            <section className='loading-cont'>
                <h3>正在提交中</h3>
                <p>请稍后...</p>
            </section>
        </div>
    )
}

export default Loading
