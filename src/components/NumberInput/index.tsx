import React, {FC} from "react"
import {InputNumber} from 'antd'
import "./index.less"

interface INumberInput {
    title: string
    titleWidth?: number
    placeholder?: string
    value: number | undefined
    onChange: (val: number | string | undefined) => void
    style?: React.CSSProperties | undefined
    min?: number | undefined
    max?: number | undefined
}

const NumberInput: FC<INumberInput> = ({title,style,titleWidth, placeholder, min,max, value, onChange}) => {
    if (!titleWidth) titleWidth = 120
    if (!placeholder) placeholder = '请输入'
    return (
        <div className='number-input' style={style}>
            <label className='input-label' style={{width: titleWidth}}>{title}：</label>
            <InputNumber
                className='input-com'
                placeholder={placeholder}
                min={min}
                max={max}
                value={value}
                onChange={(val) => {
                    onChange(val)
                }}
            />
        </div>
    )
}


export default NumberInput;