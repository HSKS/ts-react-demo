import React, {FC} from "react"
import {Select} from 'antd'
import "./index.less"
import ContainerRow from '../ContainerRow'

export interface IOneSelectData {
    label: string
    value: any

    [prop: string]: any
}

interface IOneSelect {
    title: string
    titleWidth?: number
    placeholder?: string
    data: Array<IOneSelectData>
    value: any
    onChange: (val: any, label?: string) => void
    style?: React.CSSProperties | undefined
    required?: boolean
}

const OneSelect: FC<IOneSelect> = ({title, style, required, titleWidth, data, placeholder, value, onChange}) => {
    if (!titleWidth) titleWidth = 120
    if (!placeholder) placeholder = '请选择'
    return (<ContainerRow title={title} required={required} style={style}>
        <Select className='input-com' value={value} placeholder={placeholder} onChange={(val) => {
            let lb = ''
            for (let i = 0; i < data.length; i++) {
                if (data[i].value === val) {
                    lb = data[i].label
                    break
                }
            }
            onChange(val, lb)
        }}>
            {
                data.map((item => (
                    <Select.Option key={item.value} value={item.value}>{item.label}</Select.Option>
                )))
            }
        </Select>
    </ContainerRow>)
}


export default OneSelect;