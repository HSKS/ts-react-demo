import React, { FC, useState } from "react"
import { Input } from 'antd'
import "./index.less"
import ContainerRow from '../ContainerRow'
import { IConfigRow } from '../interfaces'

interface IStringInput extends IConfigRow {
    type?: string
    placeholder?: string
    value: string | undefined
    onChange: (val: string) => void
}

const StringInput: FC<IStringInput> = ({ title, titleWidth, required, type, placeholder, value, onChange }) => {
    let configRow: IConfigRow = { title, titleWidth }
    if (!type) type = 'text'
    if (!titleWidth) titleWidth = 120
    if (!placeholder) placeholder = '请输入'
    if (required !== undefined) configRow.required = required;
    return (
        <ContainerRow {...configRow}>
            <Input
                className='input-com'
                type={type}
                placeholder={placeholder}
                value={value}
                onChange={(e) => {
                    onChange(e.target.value)
                }}
            />
        </ContainerRow>
    )

}


export default StringInput;