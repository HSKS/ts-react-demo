import React, { useState } from "react"
import { Modal, Button } from 'antd';
import { ButtonType } from "antd/lib/button";
import './styles.less'

interface IModal {
    title: string
    text?: string
    width?: string | number
    className?: string
    visible: boolean
    onClick: () => void
    setVisible: (visible: boolean) => void
}

const Index: React.FC<IModal> = ({ title, text, visible, setVisible, className, width, onClick, children }) => {
    const [confirmLoading, setConfirmLoading] = useState(false)

    // 确认
    const handleOk = () => {
        setConfirmLoading(true)
        onClick()
        setTimeout(() => {
            setVisible(false)
            setConfirmLoading(false)
        }, 2000);
    };

    return (
        <>
            <Modal
                title={title}
                visible={visible}
                onOk={handleOk}
                centered={true}
                confirmLoading={confirmLoading}
                onCancel={() => setVisible(false)}
                width={width}
                okText='确认'
                cancelText='取消'
            >
                {text !== undefined ? <p>{text}</p> : children}
            </Modal>
        </>
    )



}


export default Index;