import React, { FC } from "react"
import { Button } from 'antd'
import { ButtonType } from "antd/lib/button";
import './styles.less'

interface ICButton {
    text: string
    type?: ButtonType
    onClick: (e: React.MouseEvent<HTMLElement, MouseEvent>) => void
    style?: React.CSSProperties | undefined
    className?: string | undefined
}

const CButton: FC<ICButton> = ({ text, style,className, type, onClick }) => {
    if (!type) type = 'default'
    return (
        <div className={`${className} c-btn`}>
            <Button style={style} type={type} onClick={onClick}>{text}</Button>
        </div>)
}


export default CButton;