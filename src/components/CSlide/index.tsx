import React, {FC} from "react"
import './styles.less'

interface ISlide {
    success?: number//数量 - 运行中
    processing?: number//等待中
    stop?: number//已暂停
    error?: number//异常
    cluster?: string//集群类型
}

const Index: FC<ISlide> = ({success = 0, processing = 0, stop = 0, error = 0, cluster}) => {
    const length = success + processing + stop + error
    // 展示标题
    const title = () => {
        if (success > 0) return <span className='success'>运行中…</span>;
        else if (length > 0 && success === 0) return <span className='processing'>等待中</span>;
        else if (length === 0) return <span className='stop'>已暂停</span>;
        else return <span className='error'>异常</span>
    }
    // 定义百分比
    const widthLength = (num: number) => {
        if (!num) return;
        const width = Math.ceil(100 / length * num) + '%'
        return {width}
    }
    return (<div className='c-slide'>
        <div className='c-slide-header'>
            <div className='c-slide-title'>
                {title()}
            </div>
            <span>({success}/{length}实例)</span>
            <span>@{cluster || '暂无'}</span>
        </div>
        {/* 内容开始 */}
        <div className='c-slide-content'>
            {/* 进度条 */}
            <section className='c-slide-cont'>
                <span className='c-slide-success' style={widthLength(success)}></span>
                <span className='c-slide-processing' style={widthLength(processing)}></span>
                <span className='c-slide-stop' style={widthLength(stop)}></span>
                <span className='c-slide-error' style={widthLength(error)}></span>
            </section>
            {/* 类型数量 */}
            <section className='c-slide-type'>
                <span className='c-type-success'>{success}运行中</span>
                <span className='c-type-processing'>{processing}等待中</span>
                <span className='c-type-stop'>{stop}已暂停</span>
                <span className='c-type-error'>{error}异常</span>
            </section>
        </div>
        {/* 内容结束 */}
    </div>)
}


export default Index;