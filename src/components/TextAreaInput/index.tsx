import React, { FC } from "react"
import { Input } from 'antd'
import "./index.less"
import ContainerRow from '../ContainerRow'
import { IConfigRow } from '../interfaces'

interface ITextAreaInput extends IConfigRow {
    rows?: number
    placeholder?: string
    value: string | undefined
    onChange: (val: string) => void
}

const TextAreaInput: FC<ITextAreaInput> = ({ title, titleWidth, inputWidth, required, rows, placeholder, value, onChange }) => {
    let configRow: IConfigRow = { title, titleWidth, inputWidth }
    if (!rows) rows = 4
    if (!titleWidth) titleWidth = 120
    if (!placeholder) placeholder = '请输入'
    if (required !== undefined) configRow.required = required;

    return (
        <ContainerRow {...configRow}>
            <Input.TextArea
                className='input-com'
                placeholder={placeholder}
                value={value}
                rows={rows}
                onChange={(e) => {
                    onChange(e.target.value)
                }}
            />
        </ContainerRow>
    )

}


export default TextAreaInput;