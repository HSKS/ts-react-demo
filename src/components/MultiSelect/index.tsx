import React, {FC} from "react"
import {Cascader} from 'antd'
import "./index.less"
import {CascaderOptionType} from "antd/lib/cascader";
import ContainerRow from "../ContainerRow";

interface IMultiSelect {
    title: string
    titleWidth?: number
    placeholder?: string
    data: Array<CascaderOptionType> | undefined
    value: any
    onChange: (val: any) => void
    style?: React.CSSProperties | undefined
    required?: boolean
}

const MultiSelect: FC<IMultiSelect> = ({title, style, required, titleWidth, data, placeholder, value, onChange}) => {
    if (!titleWidth) titleWidth = 120
    if (!placeholder) placeholder = '请选择'
    return (
        <ContainerRow title={title} titleWidth={titleWidth} required={required}>
            <Cascader placeholder={placeholder} options={data} value={value} onChange={(val) => {
                onChange(val)
            }}/>
        </ContainerRow>
    )
}


export default MultiSelect;