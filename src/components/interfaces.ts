export interface IConfigRow {
    title: string
    required?: boolean
    titleWidth?: number | string
    inputWidth?: string
}
