//阿里数 1 ～ 100
// @ts-ignore
var AliNumber = /** @class */ (function () {
    function AliNumber() {
        var _this = this;
        this.squareSum = function (n) {
            var sum = 0;
            while (n > 0) {
                sum += (n % 10) * (n % 10);
                n /= 10;
            }
            return sum;
        };
        this.isAliNum = function (n) {
            var set = new Set();
            while (n !== 1) {
                n = _this.squareSum(n);
                if (set.has(n))
                    return false;
                set.add(n);
            }
            return true;
        };
    }
    return AliNumber;
}());
var alinumber = new AliNumber();
console.log(alinumber.isAliNum(19));
// for (let i = 0;i <= 100;i++) {
//     setInterval(()=>{
//         if (alinumber.isAliNum(i)) {
//             console.log(i)
//         } else {
//             return
//         }
//     }, 0)
// }
