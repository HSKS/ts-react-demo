import React, { Component } from "react"
import { Link } from "react-router-dom";
import { observer } from "mobx-react"
import { Layout, Menu, Avatar, Dropdown, Badge } from 'antd';
import {
    AlignLeftOutlined,
    AlignRightOutlined,
    CloudTwoTone,
    MailOutlined,
    BellOutlined,
    UserOutlined,
    DownOutlined,
    ContainerOutlined
} from '@ant-design/icons';
import store from "./store"
import "./styles.less"
import { IMenu } from '../config/menus'
import menus from '../config/menus'
import base from "../config/base";

const { Header, Sider, Content } = Layout;
const { SubMenu } = Menu
const menu = (
    <Menu>
        <Menu.Item>
            修改信息
        </Menu.Item>
        <Menu.Item onClick={()=>{

            console.log('object');

        }}>
            退出登录
        </Menu.Item>
    </Menu>
);


@observer
class Index extends Component {

    componentDidMount(): void {
        let { setSelectedKeys } = store
        setSelectedKeys([window.location.pathname])
    }

    toggle = (): void => {
        const { collapsed, setCollapsed } = store
        setCollapsed(!collapsed)
    }

    renderMenus = (menuList: Array<IMenu>): Array<React.ReactNode> => {
        return menuList.map(v => {
            return v.children && v.children.length ? (
                <SubMenu
                    key={v.key}
                    disabled={v.disabled}
                    title={
                        <span>
                            {v.icon}
                            <span>{v.title}</span>
                        </span>
                    }
                >
                    {this.renderMenus(v.children)}
                </SubMenu>
            ) : (
                    <Menu.Item key={v.key}  disabled={v.disabled}>
                        <Link to={v.path}>
                            <span>
                                {v.icon}
                                <span>{v.title}</span>
                            </span>
                        </Link>
                    </Menu.Item>
                )
        })
    }

    render(): React.ReactNode {
        let { collapsed, openKeys, selectedKeys } = store;
        const logo = require('../assets/navlogo.png');
        const logoMin = require('../assets/navlogomin.png');
        return (
            <Layout className='layout'>

                {/* 左侧内容 */}
                <Sider trigger={null} collapsible collapsed={collapsed}>
                    {/* collapsed ? "logo-close" :  */}
                    {/* logo */}
                    <div className='logo'>
                        <img src={!collapsed ? logo : logoMin} className='logo-img' />
                    </div>
                    {/* 菜单 */}
                    <Menu
                        theme="dark"
                        mode="inline"
                        openKeys={[...openKeys]}
                        selectedKeys={[...selectedKeys]}
                        onSelect={({ selectedKeys }) => {
                            const { setSelectedKeys } = store
                            setSelectedKeys(selectedKeys as string[])
                        }}
                        onOpenChange={(openKeys) => {
                            const { setOpenKeys } = store
                            setOpenKeys(openKeys as string[])
                        }}
                    >
                        {this.renderMenus(menus)}
                    </Menu>
                </Sider>


                {/* 右侧内容 */}
                <Layout className="site-layout">

                    {/* 头部开始 */}
                    <Header className="layout-header">
                        {React.createElement(collapsed ? AlignLeftOutlined : AlignRightOutlined, {
                            className: 'trigger',
                            onClick: this.toggle,
                        })}
                        {/* 右侧内容 */}
                        <div className='right'>

                            {/* 邮件 */}
                            <div className='icon-msg'>
                                <Badge count={5}>
                                    <MailOutlined />
                                </Badge>
                            </div>

                            {/* 消息 */}
                            <div className='icon-info' >
                                <Badge count={0}>
                                    <BellOutlined />
                                </Badge>
                            </div>

                            {/* 帮助文档 */}
                            <div className='icon-help'>
                                <ContainerOutlined />
                                <span>帮助文档</span>
                            </div>

                            {/* 头像姓名 */}
                            <div className='user-info'>

                                <Dropdown overlay={menu} placement="bottomRight" arrow>
                                    <span className='nickname'>Katty Rite<DownOutlined /></span>
                                </Dropdown>
                                <Avatar size={55} icon={<UserOutlined />} />
                            </div>
                        </div>
                    </Header>
                    {/* 头部结束 */}


                    {/* 内容开始 */}
                    <Content className="site-layout-background">
                        {this.props.children}
                    </Content>
                    {/* 内容结束 */}
                </Layout>
            </Layout>
        )
    }
}

export default Index