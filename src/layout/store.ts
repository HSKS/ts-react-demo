import {observable, action} from "mobx"

class Store {
    @observable collapsed: boolean = false
    @action setCollapsed = (c: boolean): void => {
        this.collapsed = c
    }
    @observable openKeys: string[] = []
    @action setOpenKeys = (arr: string[]): void => {
        this.openKeys = arr
    }
    @observable selectedKeys: string[] = []
    @action setSelectedKeys = (arr: string[]): void => {
        this.selectedKeys = arr
    }
    @observable keyPaths: string[] = []
    @action setKeyPaths = (arr: string[]): void => {
        this.keyPaths = arr
    }
}

export default new Store()