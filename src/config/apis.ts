const apis = {
    Login: {
        url: '/XHApi/Login',
        method: 'POST'
    },
    GetAreaCluster: {
        url: '/XHApi/GetAreaCluster',
        method: 'POST'
    },
    InitResourceData: {
        url: '/XHApi/InitResourceData',
        method: 'POST',
    },
    GetAppList: {
        url: '/XHApi/GetAppList',
        method: 'POST',
    },
    GetAppDetails: {
        url: '/XHApi/GetAppDetails',
        method: 'GET',
    },
    GetPodList: {
        url: '/XHApi/GetPodList',
        method: 'POST',
    },
    GetPodLogs: {
        url: '/XHApi/GetPodLogs',
        method: 'GET',
    },
    GetHaproxyList: {
        url: '/XHApi/GetHaproxyList',
        method: 'GET',
    },
    DeleteApp: {
        url: '/XHApi/DeleteApp',
        method: 'POST',
    },
    StartApp: {
        url: '/XHApi/StartApp',
        method: 'POST',
    },
    RestartApp: {
        url: '/XHApi/RestartApp',
        method: 'POST',
    },
    StopApp: {
        url: '/XHApi/StopApp',
        method: 'POST',
    },
    CreateApp: {
        url: '/XHApi/CreateApp',
        method: 'POST',
    },
    GetAppInfo: {
        url: '/XHApi/GetAppInfo',
        method: 'POST',
    },
    UpdateAppHpa: {
        url: '/XHApi/hpa/createOrUpdate',
        method: 'POST',
    },
    GetClusterList: {
        url: '/XHApi/GetClusterList',
        method: 'POST'
    },
    GetClusterInfoDetail: {
        url: '/XHApi/GetClusterInfoDetail',
        method: 'POST'
    },
    CreateNamespace: {
        url: '/XHApi/CreateNamespace',
        method: 'POST'
    },
    GetNamespaceList: {
        url: '/XHApi/GetNamespaceList',
        method: 'POST'
    },
    GetNamespace: {
        url: '/XHApi/GetNamespace',
        method: 'POST'
    },
    ListCreateApp: {
        url: '/XHApi/mirror/listCreateApp',
        method: 'POST'
    },
    GetNodeType: {
        url: '/XHApi/GetNodeType',
        method: 'POST'
    },
    GetDepotList: {
        url: '/XHApi/mirror/list',
        method: 'GET'
    },
    GetHpaDetail: {
        url: '/XHApi/hpa/detail/',
        method: 'GET'
    },
    GetAppClusterList: {
        url: '/XHApi/GetAppClusterList',
        method: 'GET'
    },
    GetDomain: {
        url: '/XHApi/GetDomain',
        method: 'POST'
    },
    RestartPod: {
        url: '/XHApi/RestartPod',
        method: 'POST'
    },
    GetmirrorList: {
        url: '/XHApi/mirror/list',
        method: 'GET'
    },
    MirrorCollect: {
        url: '/XHApi/mirror/collect',
        method: 'POST'
    },
    NamespaceMemberList: {
        url: '/XHApi/NamespaceMemberList',//管理成员列表
        method: 'POST'
    },
    NamespaceDeleteMember: {
        url: '/XHApi/NamespaceDeleteMember',//移除
        method: 'POST'
    },
    NamespaceGroupList: {
        url: '/XHApi/NamespaceGroupList',//业务系统获取租户列表
        method: 'GET'
    },
    NamespaceUserList: {
        url: '/XHApi/NamespaceUserList',//业务系统获取租户成员列表
        method: 'POST'
    },
    NamespaceRoleList: {
        url: '/XHApi/NamespaceRoleList',//业务系统获取角色列表
        method: 'POST'
    },
    NamespaceAddMembers: {
        url: '/XHApi/NamespaceAddMembers',//业务系统批量添加成员
        method: 'POST'
    },       

    GetMirrorSelectData: {
        url: '/XHApi/mirror/listCreateApp',
        method: 'POST'
    },
    CreateHaproxy: {
        url: '/XHApi/CreateHaproxy',
        method: 'POST'
    },
    DeleteHaproxy: {
        url: '/XHApi/DeleteHaproxy',
        method: 'POST'
    },
    AlterAppInstance: {
        url: '/XHApi/AlterAppInstance',
        method: 'POST'
    },
    DownloadLogs: {
        url: '/XHApi/DownloadLogs',
        method: 'GET'
    },
    GetNsInfo: {
        url: '/XHApi/GetNsInfo',
        method: 'POST'
    },
    GetOperationLog: {
        url: '/XHApi/GetOperationLog',
        method: 'POST'
    },
    GetNsAppList: {
        url: '/XHApi/GetNsAppList',
        method: 'POST'
    },
    upload: {
        url: '/XHApi/mirror/upload',
        method: 'POST'
    },
    HubList: {
        url: '/XHApi/mirror/hub/list',
        method: 'GET'
    },
    DeleteNs: {
        url: '/XHApi/DeleteNs',
        method: 'POST'
    }
    
}

export default apis