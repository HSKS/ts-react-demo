import * as React from "react";
import {
    ApartmentOutlined,
    UserOutlined,
    AppstoreOutlined,
    ApiOutlined,
    ExceptionOutlined,
    ThunderboltOutlined,
    PieChartOutlined,
    DeploymentUnitOutlined,
    BankOutlined

} from '@ant-design/icons';

export interface IMenu {
    key: string
    title: string
    path: string
    icon: React.ReactNode
    disabled?: boolean
    children?: Array<IMenu>
}

const menus: Array<IMenu> = [
    /*首页*/
    {
        key: '/dcos/',
        title: '首页',
        path: '/dcos/',
        icon: <BankOutlined />,
        children: undefined
    },
    /*应用管理*/
    {
        key: '/dcos/deployment',
        title: '应用管理',
        path: '/dcos/deployment',
        icon: <AppstoreOutlined />,
        children: undefined
    },
    /*有状态服务*/
    // {
    //     key: '/dcos/stateful',
    //     title: '有状态服务',
    //     icon: <ApiOutlined/>,
    //     children: undefined
    // },
    /*集群管理*/
    {
        key: '/dcos/cluster',
        title: '集群管理',
        path: '/dcos/cluster',
        icon: <ApartmentOutlined />,
        children: [{
            key: '/dcos/cluster',
            title: '集群管理',
            path: '/dcos/cluster',
            icon: null,
            children: undefined
        }]
    },
    /*镜像管理*/
    {
        key: '/dcos/image', // xiao
        title: '镜像管理',
        path: '/dcos/image',
        icon: <DeploymentUnitOutlined />,
        children: [{
            key: '/dcos/image',
            title: '自有镜像',
            path: '/dcos/image',
            icon: null,
            children: undefined
        }, {
            key: '/dcos/mirror',
            title: '镜像中心',
            path: '/dcos/mirror',
            icon: null,
            children: undefined
        },
        {
            key: '/dcos/collect',
            title: '我的收藏',
            path: '/dcos/collect',
            icon: null,
            children: undefined
        }
            ,
        {
            key: '/dcos/mirror_create',
            title: '创建镜像',
            path: '/dcos/mirror_create',
            icon: null,
            children: undefined
        }
        ]
    },
    /*日志管理*/
    {
        key: '/dcos/logger',
        title: '日志管理',
        path: '/dcos/logger',
        icon: <ExceptionOutlined />,
        disabled: true,
        children: undefined
    },
    /*告警管理*/
    {
        key: '/dcos/alerter',
        title: '告警管理',
        path: '/dcos/alerter',
        icon: <ThunderboltOutlined />,
        disabled: true,
        children: undefined
    },
    /*业务系统*/
    {
        key: '/dcos/system',
        title: '业务系统',
        path: '/dcos/system',
        icon: <PieChartOutlined />,
        children: [{
            key: '/dcos/system',
            title: '业务系统管理',
            path: '/dcos/system',
            icon: null,
            children: undefined
        }
        ]
    },
    /*用户权限管理*/
    {
        key: '/dcos/permission',
        title: '用户权限管理',
        path: '/dcos/permission',
        icon: <UserOutlined />,
        disabled: true,
        children: undefined
    }
]

export default menus