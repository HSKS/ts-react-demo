const routes: { [porp: string]: string } = {
    home: '/dcos/',
    image: '/dcos/image',
    logger: '/dcos/logger',
    alerter: '/dcos/alerter',
    system: '/dcos/system',
    permission: '/dcos/permission',
    //============================
    // 集群
    //============================
    cluster: '/dcos/cluster',//集群主页
    cluster_details: '/dcos/cluster_details',//集群-详情
    cluster_details_node: '/dcos/cluster_details_node',//集群-详情-节点
    //============================
    // 应用管理
    //============================
    deployment: '/dcos/deployment',
    app_create: '/dcos/app_create',
    deployment_details: '/dcos/deployment_details',
    deployment_create1: '/dcos/deployment_details1',
    deployment_create: '/dcos/deployment_create',//应用管理-创建
    //============================
    // 业务管理
    //============================
    system_create: '/dcos/system_create',
    system_details: '/dcos/system_details',
    member:'/dcos/member',
    //============================
    // 镜像
    //============================
    image_details: '/dcos/image_details',
    mirror: '/dcos/mirror',
    mirror_details: '/dcos/mirror_details',
    collect:'/dcos/collect',
    mirror_create: '/dcos/mirror_create',
}

export default routes;

