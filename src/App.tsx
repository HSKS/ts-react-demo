import React from 'react';
import { BrowserRouter as Router, Route, Redirect,Switch } from "react-router-dom";
import Index from './pages/index'
import Login from './pages/login'
function App() {
    return (
        <Router>
            <Switch>
                <Route exact path="/login" component={Login} />
                <Route path="/dcos/" component={Index} />
                <Redirect to="/login"/>
            </Switch>
        </Router>
    );
}

export default App;